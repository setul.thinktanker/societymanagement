import Realm from 'realm';

export const USER_SCHEMA = 'user_schema';

//define user table schema
export const UserSchema = {
    name: USER_SCHEMA,
    primaryKey: 'id',
    properties: {
        //user registration fields
        id: 'string',    // primary key
        societyId: 'string',
        societyName: 'string',
        name: 'string',
        mobileNo: 'string',
        email: 'string',
        gender: 'string',
        houseType: 'string',
        block: 'string',
        blockNo: 'string',
        isOwnerStayInSociety: 'string',
        currentAddress: 'string',
        isRented: 'string',
        isChairman: 'string',
        image: 'string',
        // token: 'string',
        password: 'string',
    }
};




//create database option for create database
const databaseOptions = {
    path: 'societyManagement.realm',
    schema: [UserSchema],
    schemaVersion: 8 //optional
};

//functions for Insert user
export const insertUser = user => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            realm.create(USER_SCHEMA, user);
            resolve(user);
        });
    }).catch((error) => reject(error));
});

export const updateUser = data => new Promise((resolve, reject) => {
    console.log(data);
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            const updatingData = realm.objectForPrimaryKey(USER_SCHEMA, data.id);
            console.log(updatingData);
            updatingData.name = data.name;
            updatingData.mobileNo = data.mobileNo;
            updatingData.email = data.email;
            updatingData.gender = data.gender;
            updatingData.houseType = data.houseType;
            updatingData.block = data.block;
            updatingData.blockNo = data.blockNo;
            updatingData.isOwnerStayInSociety = data.isOwnerStayInSociety;
            updatingData.isRented = data.isRented;
            // updatingData.image = data.image;
            resolve();
        });
    }).catch((error) => reject(error));
});


//function for get user
export const getUser = () => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        const user = realm.objects(USER_SCHEMA);
        resolve(user);
    }).catch((error) => {
        reject(error);
    });
});



export const deleteUser = () => new Promise((resolve, reject) => {
    Realm.open(databaseOptions).then(realm => {
        realm.write(() => {
            const user = realm.objects(USER_SCHEMA);
            realm.delete(user);
            resolve();
        });
    }).catch((error) => reject(error));
});





export default new Realm(databaseOptions);

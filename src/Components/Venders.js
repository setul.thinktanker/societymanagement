import React, {Component, Fragment} from "react";
import {
    View,
    StatusBar,
    FlatList,
    Animated,
    StyleSheet,
    Image,
    Linking,
    TouchableOpacity,
    SafeAreaView
} from "react-native";
import {DARK_GRADIENT, LIGHT_GRADIENT} from "../Commons/Colors";
import VendorDetailsItemLayout from "./VendorDetailsItemLayout";
import LinearGradient from "react-native-linear-gradient";
import Header from "./Header";
import Swipeable from "react-native-gesture-handler/Swipeable";
import {RectButton} from "react-native-gesture-handler";
import AsyncStorage from "@react-native-community/async-storage";
import {getUser} from "../Database/allSchema";
import {connect} from "react-redux";
import {getVendors, deleteVenderDB} from "../Actions";
import {RENTER_DETAILS_SUCCESS} from "../Actions/type";

let authToken = '';
let userId = '';
let societyId = '';

// User added vendors by passing true in getVendors
class Venders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            venderData: [],
            // venderData: [
            //     {
            //         name: "Mr. Name",
            //         inSociety: false,
            //         profession: "Carpenter",
            //         mobileNo: "7645992344",
            //         address:
            //             "a-101,apartment name,Near hsdsadsx asxaxax bmnlaksl wsxamdjdsdsvsdcfkls",
            //         services:
            //             "Carpenters maintain and repair interior and exterior wood components such as window frames, doors and frames, walls, floors, decorative molding, etc. Carpentry also repairs and replaces drop-ceiling systems."
            //     },
            //     {
            //         name: "Mr. Name",
            //         inSociety: true,
            //         profession: "Plumber",
            //         mobileNo: "8767266233",
            //         address: "a-102,apartment name,Near hsdsadsx asxaxax bmnlaksl wsxamdjdsdsvsdcfkls",
            //         services: "Leak Repair. One of the primary services offered by any plumber is repairing plumbing leaks."
            //     },
            //     {
            //         name: "Mr. Name",
            //         inSociety: true,
            //         profession: "Plumber",
            //         mobileNo: "909090903",
            //         address: "a-102,apartment name,Near hsdsadsx asxaxax bmnlaksl wsxamdjdsdsvsdcfkls",
            //         services: "Leak Repair. One of the primary services offered by any plumber is repairing plumbing leaks."
            //     }
            // ]
        }
        ;
    }

    //call
    renderRightActionCall = (text, color, x, progress, item) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0]
        });
        return (
            <Animated.View
                style={[
                    styles.animateViewStyle,
                    {transform: [{translateX: trans}]}
                ]}
            >
                <RectButton
                    style={[styles.rightAction, {backgroundColor: color}]}
                    onPress={() => {
                        Linking.openURL(`tel:${item.mobileNo}`);
                    }}
                >
                    <Image
                        style={[styles.actionImg, {tintColor: "white"}]}
                        source={require("../img/call.png")}
                    />
                </RectButton>
            </Animated.View>
        );
    };

    //edit vendor
    renderRightActionEdit = (text, color, x, progress, item) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0]
        });

        return (
            <Animated.View style={[styles.animateViewStyle, {transform: [{translateX: trans}]}]}>
                <RectButton style={[styles.rightAction, {backgroundColor: color}]}
                            onPress={() => {
                                this.props.navigation.navigate("AddVender", {item: item});
                            }}>
                    <Image style={[styles.actionImg, {tintColor: "white"}]}
                           source={require("../img/edit.png")}/>
                </RectButton>
            </Animated.View>
        );
    };

    //delete
    renderRightActionDelete = (text, color, x, progress, item, index) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0]
        });

        const onpress = () => {
            // this._swipeableRow.close();
            this.deleteVender(index, item.id);
        };

        return (
            <Animated.View
                style={[
                    styles.animateViewStyle,
                    {transform: [{translateX: trans}]}
                ]}>
                <RectButton
                    style={[styles.rightAction, {backgroundColor: color}]}
                    onPress={onpress()}>
                    <Image
                        style={[styles.actionImg, {tintColor: "white"}]}
                        source={require("../img/delete.png")}
                    />
                </RectButton>
            </Animated.View>
        );
    };

    renderRightActions(progress, item, index) {
        return (
            <View style={{width: 250, flexDirection: "row"}}>
                {this.renderRightActionCall("Call", "#2DC170", 80, progress, item)}
                {this.renderRightActionEdit("Edit", "#2D6CC1", 80, progress, item)}
                {this.renderRightActionDelete("Delete", "#C13D2D", 80, progress, item, index)}
            </View>
        );
    }

    deleteVender(index, vendor_id) {
        // this.close;
        // // console.warn("index", index);
        // let tempArray = this.state.venderData;
        // tempArray.splice(index, 1);
        // // console.warn("tempArray", tempArray);
        // this.setState({venderData: tempArray});
        // this.state.venderData.filter(vender => vender.id === id)
        this.props.deleteVenderDB(vendor_id, "delete");
    }

    updateRef = ref => {
        this._swipeableRow = ref;
    };

    close = () => {
        this._swipeableRow.close();
    };

    onBackPress() {
        this.props.navigation.goBack();
    }

    componentWillMount() {
        console.log("componentWillMount");
        this.getToken();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        console.log("componentWillReceiveProps", nextProps.venderList.data);
        // console.log(nextProps.response.data)
        this.state.venderData = [];
        this.state.venderData = nextProps.venderList.data;

        //first directly delete to check filter is working then put inside success!!
        this.state.venderData.filter(vender => vender.id !== id)

        if (nextProps.response.data.success === true) {
            this.refs.toast.show(nextProps.response.data.msg, 1500);
            setTimeout(() => {

            }, 1000);
        } else {

        }
    }

    getToken() {
        try {
            AsyncStorage.getItem('@authToken_Key')
                .then((token) => {
                    console.log('token', token);
                    authToken = token;
                    getUser().then((user) => {
                        console.log("User data:" + JSON.stringify(user));
                        societyId = user[0].societyId;
                        userId = user[0].id;
                        this.props.getVendors(societyId, "list", userId, authToken, true)
                    }).catch((err) => {
                        console.log('err', err)
                    });
                })
                .catch((err) => {
                    console.log('err', err)
                })
        } catch (e) {
            console.log('Error in store authToken');
        }
    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{flex: 0, backgroundColor: DARK_GRADIENT}}/>
                <SafeAreaView style={{flex: 1}}>
                    <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content"/>
                    <View style={{height: 50}}>
                        <Header
                            headerText={"Venders"}
                            leftImage={require("../img/left-arrow.png")}
                            onBackPress={() => this.onBackPress()}
                        />
                    </View>

                    {this.state.venderData.length > 0 ?
                        <FlatList
                            style={{flex: 1}}
                            contentContainerStyle={{padding: 10, paddingBottom: 80}}
                            data={this.state.venderData}
                            extraData={this.state.venderData}
                            showsVerticalScrollIndicator={false}
                            renderItem={({item, index}) => {
                                return (
                                    <Swipeable
                                        ref={this.updateRef}
                                        friction={1}
                                        leftThreshold={30}
                                        rightThreshold={40}
                                        renderRightActions={progress =>
                                            this.renderRightActions(progress, item, index)
                                        }>
                                        <VendorDetailsItemLayout item={item} index={index}/>
                                    </Swipeable>
                                );
                            }}
                        /> : null
                    }

                    {
                        this.props.isVendorLoading ?
                            <Image
                                style={{
                                    height: 40,
                                    width: 40,
                                    backgroundColor: "transparent"
                                }}
                                source={require("../img/loader2.gif")}
                            /> : null
                    }

                    {/*<LinearGradient*/}
                    {/*    start={{x: 0, y: 0.5}}*/}
                    {/*    end={{x: 1.5, y: 0}}*/}
                    {/*    colors={[DARK_GRADIENT, DARK_GRADIENT]}*/}
                    {/*    style={styles.bottomBtn}*/}
                    {/*>*/}
                    {/*    <TouchableOpacity*/}
                    {/*        onPress={() => this.props.navigation.navigate("Vendor")}*/}
                    {/*        style={styles.btn}*/}
                    {/*    >*/}
                    {/*        <Image*/}
                    {/*            source={require("../img/add.png")}*/}
                    {/*            style={{*/}
                    {/*                height: 30,*/}
                    {/*                width: 30,*/}
                    {/*                tintColor: "white",*/}
                    {/*                alignSelf: "center"*/}
                    {/*            }}*/}
                    {/*        />*/}
                    {/*    </TouchableOpacity>*/}
                    {/*</LinearGradient>*/}
                </SafeAreaView>
            </Fragment>
        );
    }
}

const mapStateToProps = state => {
    console.log("mapStateToProps: " + JSON.stringify(state));

    return {
        isVendorLoading: state.Vendor.vendorListLoading,
        success: state.Vendor.success,
        msg: state.Vendor.msg,
        response: state.Vendor.response,
        venderList: state.Vendor.venderList,
    }
};

const styles = StyleSheet.create({
    animateViewStyle: {
        flex: 1,
        marginBottom: 5,
        marginTop: 10
    },
    actionText: {
        color: "white",
        fontSize: 20,
        backgroundColor: "transparent",
        padding: 10
    },
    rightAction: {
        alignItems: "center",
        flex: 1,
        borderRadius: 8,
        marginLeft: 5,
        justifyContent: "center"
    },
    actionImg: {
        height: 40,
        width: 40
    },
    bottomBtn: {
        flex: 1,
        height: 60,
        width: 60,
        borderRadius: 60 / 2,
        position: "absolute",
        bottom: "2%",
        right: "4%",
        alignItems: "center",
        justifyContent: "center"
    },
    btn: {
        flex: 1,
        width: "100%",
        height: "100%",
        justifyContent: "center"
    }
});

export default connect(mapStateToProps, {getVendors, deleteVenderDB})(Venders);

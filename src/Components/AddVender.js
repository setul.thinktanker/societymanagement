//profession,name,mobno.address,services
import React, { Component, Fragment } from "react";
import {
  View,
  StatusBar,
  ScrollView,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  SafeAreaView,
  Image,
  Platform,
  FlatList,
  Text,
  Dimensions,
  PermissionsAndroid,
  Alert
} from "react-native";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";
import AddVenderItemLayout from "./AddVenderItemLayout";
import CustomTextView from "../Custom/CustomTextView";
import Toast, { DURATION } from "react-native-easy-toast";
import Contacts from "react-native-contacts";
import Header from "./Header";
import Autocomplete from "react-native-autocomplete-input";
let uniqueArray = [];

export default class AddVender extends Component {
  constructor (props) {
    super(props);
    this.state = {
      isAlreadyAdded: false,
      radioBtnsData: ["Yes", "No"],
      venderLeaving: null,
      contacts: null,
      searchText: ""
    };
  }
  item = this.props.navigation.getParam("item");
  componentWillMount() {
    // console.warn("item", this.props.navigation.getParam("item"));
    let item = this.props.navigation.getParam("item");
    if (item) {
      this.setState({
        venderName: item.name,
        venderProfession: item.profession,
        venderMobileNumber: item.mobileNo,
        venderAddress: item.address,
        venderServices: item.services,
        venderLeaving: item.inSociety ? 0 : 1,
        isAlreadyAdded: true
      });
    }
    if (Platform.OS === "ios") {
      Contacts.getAll((err, contacts) => {
        if (err) {
          throw err;
          console.warn(err);
        }
        // contacts returned
        this.setState({ contacts: contacts });
      });
    } else if (Platform.OS === "android") {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: "Contacts",
        message: "This app would like to view your contacts."
      }).then(() => {
        Contacts.getAll((err, contacts) => {
          if (err === "denied") {
            // error
            console.log('err', err)
          } else {
            // contacts returned in Array
            this.setState({ contacts: contacts });
          }
        });
      });
    }
  }
  componentWillReceiveProps(nextProps) {
    let item = nextProps.navigation.getParam("item");
    if (item) {
      this.setState({
        venderName: item.profession,
        venderProfession: item.profession,
        venderMobileNumber: item.mobileNo,
        venderAddress: item.address,
        venderServices: item.services,
        venderLeaving: item.inSociety ? 0 : 1,
        isAlreadyAdded: true
      });
    }
  }
  componentDidMount() {
    if (Platform.OS === "ios") {
      Contacts.getAll((err, contacts) => {
        if (err) {
          throw err;
        }
        // contacts returned
        this.setState({ contacts });
      });
    } else if (Platform.OS === "android") {
      PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.READ_CONTACTS, {
        title: "Contacts",
        message: "Society Management would like to view your contacts."
      }).then(() => {
        Contacts.getAll((err, contacts) => {
          if (err === "denied") {
            // error

          } else {
            // contacts returned in Array
            console.log('contacts', contacts)
            this.setState({ contacts });
          }
        });
      });
    }
  }

  find(searchText) {
    // console.warn("Array get", this.state.contacts);
    // console.warn("searchText", searchText);

    if (searchText === "") {
      return [];
    }
    if (this.state.contacts && searchText.length > 1) {
      let data = this.state.contacts;
      const regex = new RegExp("^" + searchText, "i");
      let filterData = data.filter(function (name) {
        return name.givenName.search(regex) >= 0;
      });
      return filterData;
    }
  }
  onBackPress() {
    this.props.navigation.goBack();
  }
  onPressSaveVender() {
    // console.warn("onpress save vender details");
    const {
      venderName,
      venderProfession,
      venderMobileNumber,
      venderAddress,
      venderServices,
      venderLeaving
    } = this.state;
    let isValid = true;
    const number = /^[0-9\b]{10,10}$/;

    if (!venderName) {
      isValid = false;
      this.refs.toast.show("Please enter name of vender", 2000);
    }
    if (!venderProfession && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter profession of vender", 2000);
    }
    if (!venderMobileNumber && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter mobile number of vender", 2000);
    } else if (number.test(venderMobileNumber) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid mobile number of vender", 2500);
    }
    if (!venderLeaving && isValid) {
      isValid = false;
      this.refs.toast.show(
        "Please select vender leaving in society or not",
        2500
      );
    }

    if (isValid) {
      this.props.navigation.goBack();
    }
  }
  setName(item) {
    console.warn("set", item);
    item.phoneNumbers.map(phone => {
      let number = phone.number.replace(/[^\d]/g, "");

      this.setState({ venderMobileNumber: number });
    });
    this.setState({ searchText: item.givenName, venderName: item.givenName });
  }
  render() {
    const filterSearch = this.find(this.state.searchText);
    uniqueArray = [...new Set(filterSearch)];
    console.log('uniqueArray', uniqueArray)
    return (
      <Fragment>
        <SafeAreaView style={ { flex: 0, backgroundColor: DARK_GRADIENT } } />
        <SafeAreaView style={ { flex: 1 } }>
          <StatusBar backgroundColor={ DARK_GRADIENT } barStyle="light-content" />
          <View style={ { height: 50 } }>
            <Header
              headerText={
                this.state.isAlreadyAdded ? "Update Vender" : "Add Vender"
              }
              leftImage={ require("../img/left-arrow.png") }
              onBackPress={ () => this.onBackPress() }
            />
          </View>
          <ScrollView
            alwaysBounceVertical={ false }
            style={ { flex: 1, height: "100%" } }
          >
            {/* <FlatList
              data={this.state.contacts}
              renderItem={({ item }) => (
                <View>
                  <Text style={styles.contact_details}>
                    Name: {`${item.givenName} `} Surname: {item.familyName}
                  </Text>
                  {item.phoneNumbers.map(phone => (
                    <Text style={styles.phones}>
                      {phone.label} : {phone.number}
                    </Text>
                  ))}
                </View>
              )}
              //Setting the number of column
              numColumns={1}
              keyExtractor={(item, index) => index}
            /> */}
            <View style={ styles.container }>
              <View style={ styles.card }>
                <View style={ [styles.textInputView, { zIndex: 100 }] }>
                  <Autocomplete
                    allowFontScaling={ false }
                    fontSize={ 15 }
                    // color={ "#1A2980" }
                    // autoCapitalize="none"
                    // autoCorrect={ false }
                    underlineColorAndroid="transparent"
                    style={ styles.autocompleteStyle }
                    containerStyle={ {} }
                    value={ this.state.venderName }
                    inputContainerStyle={ styles.autoInputContainerStyle }
                    data={ (uniqueArray.length === 1 && this.state.searchText == uniqueArray[0].givenName) ? [] : uniqueArray }
                    defaultValue={ this.state.searchText }
                    width={ Dimensions.get("window").width - 40 }
                    onChangeText={ item => {
                      this.setState({ searchText: item, venderName: item })
                    } }
                    placeholder="Enter Vender's Name"
                    placeholderTextColor={ DARK_GRADIENT }
                    listContainerStyle={ {} }
                    listStyle={ styles.autoListStyle }
                    renderItem={ item => {
                      return (
                        <TouchableOpacity
                          onPress={ () => {
                            uniqueArray = [];
                            this.setName(item.item);
                          } }
                        >
                          <View
                            style={ {
                              padding: 10,
                              zIndex: 100,
                              backgroundColor: "#E5E7E9",
                              flexDirection: "row"
                            } }
                          >
                            <Text style={ styles.textStyleauto }>
                              { `${item.item.givenName} ` }
                            </Text>
                            { item.item.phoneNumbers.map(phone => (
                              <Text style={ styles.textStyleauto }>
                                ( { phone.number } )
                              </Text>
                            )) }
                          </View>
                          <View
                            style={ {
                              borderBottomWidth: 0.5,
                              borderBottomColor: DARK_GRADIENT
                            } }
                          />
                        </TouchableOpacity>
                      );
                    } }
                  />

                  {/* <TextInput
                    allowFontScaling={false}
                    style={styles.textInput}
                    placeholder="Enter Vender's Name"
                    placeholderTextColor={DARK_GRADIENT}
                    placeholderTextSize="18"
                    keyboardType="default"
                    onChangeText={text => this.setState({ venderName: text })}
                    value={this.state.venderName}
                    // onChangeText={this.props.onChangeText}
                    // returnKeyType="search"
                    // onSubmitEditing={this.props.onSubmitEditing}
                  /> */}
                </View>

                <View style={ styles.textInputView }>
                  <TextInput
                    allowFontScaling={ false }
                    style={ styles.textInput }
                    placeholder="Enter Mobile Number"
                    placeholderTextColor={ DARK_GRADIENT }
                    placeholderTextSize="18"
                    keyboardType="number-pad"
                    maxLength={ 10 }
                    onChangeText={ text =>
                      this.setState({ venderMobileNumber: text })
                    }
                    value={ this.state.venderMobileNumber }
                  // onChangeText={this.props.onChangeText}
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                  />
                </View>
                <View style={ styles.textInputView }>
                  <TextInput
                    allowFontScaling={ false }
                    style={ styles.textInput }
                    placeholder="Enter Proffession/Work"
                    placeholderTextColor={ DARK_GRADIENT }
                    placeholderTextSize="18"
                    keyboardType="default"
                    onChangeText={ text =>
                      this.setState({ venderProfession: text })
                    }
                    value={ this.state.venderProfession }
                  // onChangeText={this.props.onChangeText}
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                  />
                </View>
                <View style={ styles.textInputView }>
                  <TextInput
                    allowFontScaling={ false }
                    style={ styles.textInput }
                    placeholder="Enter Address"
                    placeholderTextColor={ DARK_GRADIENT }
                    placeholderTextSize="18"
                    keyboardType="default"
                    multiline={ true }
                    onChangeText={ text =>
                      this.setState({ venderAddress: text })
                    }
                    value={ this.state.venderAddress }
                  // onChangeText={this.props.onChangeText}
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                  />
                </View>
                <View style={ styles.textInputView }>
                  <TextInput
                    allowFontScaling={ false }
                    style={ styles.textInput }
                    placeholder="Enter Services"
                    placeholderTextColor={ DARK_GRADIENT }
                    placeholderTextSize="18"
                    keyboardType="default"
                    multiline={ true }
                    onChangeText={ text =>
                      this.setState({ venderServices: text })
                    }
                    value={ this.state.venderServices }
                  // onChangeText={this.props.onChangeText}
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                  />
                </View>
                <View
                  style={ [
                    styles.textInputView,
                    {
                      borderBottomWidth: 0,
                      flexDirection: "row",
                      marginBottom: 0,
                      marginTop: Platform.OS === "ios" ? 0 : 10
                    }
                  ] }
                >
                  <CustomTextView
                    text="Leaving in Society ?"
                    style={ [styles.radioText, { marginLeft: 2 }] }
                  />
                  { this.state.radioBtnsData.map((data, key) => {
                    return (
                      <View key={ key } style={ { flex: 1, marginLeft: 20 } }>
                        { this.state.venderLeaving == key ? (
                          <TouchableOpacity
                            onPress={ () => {
                              this.setState({ venderLeaving: key });
                            } }
                            style={ styles.btn }
                          >
                            <Image
                              style={ styles.img }
                              source={ require("../img/selectedRadioBtn.png") }
                            />
                            <CustomTextView
                              text={ data }
                              style={ [styles.radioText] }
                            />
                          </TouchableOpacity>
                        ) : (
                            <TouchableOpacity
                              onPress={ () => {
                                this.setState({ venderLeaving: key });
                              } }
                              style={ styles.btn }
                            >
                              <Image
                                style={ [styles.img, {}] }
                                source={ require("../img/unSelectedRadioBtn.png") }
                              />

                              <CustomTextView
                                text={ data }
                                style={ [styles.radioText] }
                              />
                            </TouchableOpacity>
                          ) }
                      </View>
                    );
                  }) }
                </View>
                <Toast
                  ref="toast"
                  position={ Platform.OS === "ios" ? "bottom" : "center" }
                  style={ {
                    backgroundColor: DARK_GRADIENT,
                    position: "absolute",
                    bottom: 80
                  } }
                  opacity={ 0.9 }
                />
              </View>
            </View>
          </ScrollView>
          <TouchableOpacity
            style={ {
              width: "100%",
              height: 60,
              backgroundColor: DARK_GRADIENT
            } }
          >
            <TouchableOpacity
              onPress={ () => {
                this.onPressSaveVender();
              } }
              style={ {
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              } }
            >
              <CustomTextView
                text="Save"
                style={ { color: "white", fontSize: 18, textAlign: "center" } }
              />
            </TouchableOpacity>
          </TouchableOpacity>
        </SafeAreaView>
      </Fragment>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  card: {
    width: "100%",
    backgroundColor: "white",
    paddingTop: Platform.OS === "ios" ? 15 : 0,
    paddingHorizontal: 10,
    paddingVertical: 15,
    borderRadius: 8,
    shadowColor: DARK_GRADIENT,
    shadowOffset: { height: 5, width: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 8
  },
  sendBtn: {
    height: 40,
    width: 100,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    marginTop: 10,
    alignSelf: "flex-end",
    backgroundColor: DARK_GRADIENT
  },
  textStyle: {
    color: "white",
    fontSize: 15,
    textAlign: "center"
  },
  textInput: {
    fontSize: 15,
    // color: DARK_GRADIENT,
    width: "95%",
    marginLeft: Platform.OS === "ios" ? 0 : -3,
    marginBottom: Platform.OS === "ios" ? 5 : -10
  },
  textInputView: {
    width: "98%",
    alignSelf: "center",
    borderBottomWidth: 1,
    borderColor: DARK_GRADIENT,
    marginBottom: Platform.OS === "ios" ? 20 : 5
  },
  imageBottom: {
    bottom: "20%",
    backgroundColor: "rgba(20,20,20,0.6)",
    width: "100%",
    height: "20%",
    justifyContent: "center"
  },
  addBtnView: {
    width: "100%",
    paddingHorizontal: 10,
    height: 40
  },
  addBtn: {
    flex: 1,
    backgroundColor: "rgba(30, 118, 161, 1)",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  },
  radioText: {
    color: DARK_GRADIENT,
    fontSize: 15
  },
  img: {
    height: 20,
    width: 20,
    marginLeft: 8,
    marginRight: 5,
    tintColor: DARK_GRADIENT
  },
  btn: {
    flexDirection: "row"
  },

  autocompleteStyle: {
    flex: 1,
    color: "#1A2980",
    fontSize: 15,
    paddingBottom: Platform.OS === "ios" ? 5 : 0
  },
  autoInputContainerStyle: {
    borderColor: "transparent",
    marginLeft: Platform.OS === "ios" ? 0 : -3
    // marginTop: 15
  },
  autoListStyle: {
    borderWidth: 0,
    backgroundColor: "white",
    maxHeight: 180
  },
  textStyleauto: {
    color: DARK_GRADIENT,
    fontSize: 15
  }
});

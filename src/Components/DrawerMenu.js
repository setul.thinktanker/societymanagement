import React, { Component, Fragment } from 'react';
import {
    View, StyleSheet, SafeAreaView, TouchableOpacity, UIManager,
    Platform, Animated, Easing, FlatList
} from 'react-native';
import { LIGHT_GRADIENT, DARK_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";
import { Avatar, Icon } from 'react-native-elements'
import CustomTextView from "../Custom/CustomTextView";

export default class DrawerMenu extends Component {
    constructor (props) {
        super(props);
        this.RotateValueHolder = new Animated.Value(0);
        this.state = {
            isExpanded: true,
            to: "0deg",
            from: "180deg",
            drawerMenuData: [
                {
                    icon: 'people',
                    label: 'Society Members',
                    navigateOn: 'SocietyMembers'
                },
                {
                    icon: 'event',
                    label: 'Event Calender',
                    navigateOn: 'Event'
                },
                {
                    icon: 'photo-album',
                    label: 'Photo Gallery',
                    navigateOn: 'PhotoGallery'
                },
                { icon: 'ios-trophy', label: "Achievements", type: 'ionicon' },
                // { icon: 'ios-document', label: "Documents", type: 'ionicon' },
                { icon: 'event', label: "Meeting Tracking" },
                { icon: 'event', label: "Committee" },
                { icon: 'message', label: 'Forum' },
                { icon: 'poll', label: 'Instant Poll' },
                { icon: 'event', label: "Classified" },
                { icon: 'logout', label: "Log out", type: 'material-community' },
            ]
        };
        if (Platform.OS === "android") {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }

    }
    onPressExpand() {
        this.setState({
            to: this.state.toggle ? "0deg" : "180deg",
            from: this.state.toggle ? "0deg" : "180deg",
            isExpanded: !this.state.isExpanded,
        }, () => {
            this.RotateValueHolder.setValue(0);
            Animated.timing(this.RotateValueHolder, {
                toValue: 1,
                duration: 100,
                easing: Easing.linear
            }).start();
        })
    }
    render() {
        const RotateData = this.RotateValueHolder.interpolate({
            inputRange: [0, 1],
            outputRange: [this.state.to, this.state.from]
        });
        return (
            <Fragment>
                <SafeAreaView style={ { flex: 0, backgroundColor: DARK_GRADIENT } } />
                <SafeAreaView style={ { flex: 1 } }>
                    <View style={ [styles.container, { width: (this.state.isExpanded) ? '85%' : '25%' }] }>
                        {/* <TouchableOpacity onPress={() => this.onPressExpand()}>
                            <LinearGradient
                                colors={[DARK_GRADIENT, LIGHT_GRADIENT]}
                                style={styles.mainView}>
                                <Image
                                    style={[styles.iconStyle, { tintColor: 'transparent' }]}
                                    source={require("../img/cityscape.png")}
                                />
                            </LinearGradient>
                        </TouchableOpacity> */}
                        <View style={ styles.containerItems }>
                            <LinearGradient
                                start={ { x: 0, y: 0.3 } }
                                end={ { x: 1.5, y: 0 } }
                                colors={ [DARK_GRADIENT, LIGHT_GRADIENT] } style={ styles.fView }>
                                <View style={ styles.imgView }>
                                    <Avatar
                                        rounded
                                        title="RB"
                                        titleStyle={ { color: 'white' } }
                                        overlayContainerStyle={ { backgroundColor: LIGHT_GRADIENT } }
                                        source={ require("../img/man.png") }
                                        size={ 53 }
                                    />
                                </View>
                                <CustomTextView
                                    style={ [styles.textStyle, { fontSize: 18, marginTop: 5 }] }
                                    text={ (this.state.isExpanded) ? 'Mr. Patel' : null } />
                            </LinearGradient>

                            {/* <View style={styles.bottomLine} /> */ }

                            {/* flat list for all fields */ }

                            <FlatList
                                data={ this.state.drawerMenuData }
                                extraData={ this.state }
                                renderItem={ ({ item }) => {
                                    return (
                                        <TouchableOpacity
                                            onPress={ () => this.props.navigation.navigate(item.navigateOn) }
                                            style={ [styles.mainView, { justifyContent: (this.state.isExpanded) ? null : 'center' }] }>
                                            <Icon
                                                name={ item.icon }
                                                color='#ffffff'
                                                size={ 30 }
                                                type={ item.type }
                                            />
                                            <CustomTextView
                                                style={ [styles.textStyle, { fontWeight: '100', marginLeft: (this.state.isExpanded) ? 10 : null }] }
                                                text={ (this.state.isExpanded) ? item.label : null } />
                                        </TouchableOpacity>
                                    );
                                } }
                            />

                        </View>
                        {/* view for toggle arrow */ }
                        {/* <View style={styles.secondView}>
                            <TouchableOpacity
                                onPress={() => this.onPressExpand()}
                                style={styles.sideArrowView}>
                                <Animated.Image
                                    style={{ transform: [{ rotate: RotateData }], height: 15, width: 15, tintColor: 'white' }}
                                    source={require('../img/right_arrow.png')}
                                />
                            </TouchableOpacity>
                        </View> */}
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1C5C96',
    },
    imgView: {
        height: 55,
        width: 55,
        borderRadius: 55 / 2,
        borderWidth: 2,
        borderColor: LIGHT_GRADIENT,
        alignItems: 'center',
        justifyContent: 'center'
    },
    bottomLine: {
        height: 0.6,
        width: '100%',
        margin: 10,
        elevation: 2,
        backgroundColor: LIGHT_GRADIENT
    },
    mainView: {
        height: 50,
        width: '100%',
        padding: 10,
        // backgroundColor: 'red',
        flexDirection: 'row',
        alignSelf: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    iconStyle: {
        height: 40,
        width: 40,
        tintColor: 'white'
    },
    containerItems: {
        flex: 1,
        // alignItems: 'center',
        // paddingVertical: 10
    },
    textStyle: {
        color: 'white',
        fontSize: 20,
        marginLeft: 10,
    },
    fView: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'flex-start',
        // paddingLeft: 15,
        padding: 10,
        // justifyContent: 'center'
    },
    sideArrowView: {
        flex: 1,
        position: 'absolute',
        height: 25,
        width: 25,
        borderRadius: 25 / 2,
        right: -10,
        backgroundColor: 'rgba(26, 41, 128, 0.7)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    secondView: {
        position: 'absolute',
        flex: 1,
        height: '100%',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
})

/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Image,
  TouchableOpacity
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import CustomTextView from '../Custom/CustomTextView';
import {DARK_GRADIENT,LIGHT_GRADIENT} from '../Commons/Colors';

export default class HeaderHome extends Component {
  render() {
    return (
      <LinearGradient
      start={{x: 0, y: 0.3}}
      end={{x: 1.5, y: 0}}
      colors={[DARK_GRADIENT, LIGHT_GRADIENT]}
      style={styles.container}>
      <StatusBar backgroundColor="#1A2980" barStyle="light-content" />
      <View style = {{ flexDirection:"row" , justifyContent:'center'}}>
        <View style = {styles.startView}>
          <Image
              style={styles.imageStyle}
              source={require('../img/house-outline.png')}
          />
          <CustomTextView style = {styles.textStyle} text={this.props.headerText}/>
        </View>
        <View style = {styles.endView}>
          <TouchableOpacity
          onPress = {this.props.myNavigation}
          style = {[styles.imgView,{}]}
          >
            <Image
                style={styles.imageStyle}
                source={require('../img/notifications-bell-button.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity
          style = {[styles.imgView,{}]}
          >
            <Image
                style={styles.imageStyle}
                source={require('../img/logout.png')}
            />
          </TouchableOpacity>
        </View>
      </View>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width:'100%',
    justifyContent:'center',
    paddingHorizontal:10,
  },
  imageStyle:{
    height:25,
    width:25,
    tintColor:'white',
    alignSelf:'center'
  },
  endView:{
    width:'20%',
    height:'100%',
    flexDirection:'row',
    alignSelf:'flex-end',
    justifyContent:'space-between',
    alignItems:'center'
  },
  imgView:{
    // width:'50%',
    // justifyContent:'center',
    // alignItems:'center'
  },
  startView:{
    flex:1,
    width:'70%',
    alignSelf:'flex-start',
    alignItems:'center',
    flexDirection:'row'
  },
  textStyle:{
    fontSize:20,
    color:'white',
    marginLeft:10
  }
});

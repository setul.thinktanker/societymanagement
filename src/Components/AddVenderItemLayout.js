import React, { Component } from 'react';
import { View, 
    StyleSheet,
    TextInput,

 } from 'react-native';
 import {DARK_GRADIENT,LIGHT_GRADIENT} from '../Commons/Colors';

export default class AddVenderItemLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
      <View style = {styles.container}>
          <View style = {styles.card}>
            <View style = {styles.textInputView}>
                <TextInput
                    allowFontScaling={false}
                    style={styles.textInput}
                    placeholder="Enter Vender's Name"
                    placeholderTextColor = {DARK_GRADIENT}
                    placeholderTextSize="18"
                    keyboardType="default"
                    onChangeText={(text) => this.setState({venderName :text})}
                    // value={this.props.value}
                    // onChangeText={this.props.onChangeText}
                    // returnKeyType="search"
                    // onSubmitEditing={this.props.onSubmitEditing}
                />
            </View>
            <View style = {styles.textInputView}>
                <TextInput
                    allowFontScaling={false}
                    style={styles.textInput}
                    placeholder="Enter Proffession/Work"
                    placeholderTextColor = {DARK_GRADIENT}
                    placeholderTextSize="18"
                    keyboardType="default"
                    onChangeText={(text) => this.setState({venderProfession :text})}
                    // value={this.props.value}
                    // onChangeText={this.props.onChangeText}
                    // returnKeyType="search"
                    // onSubmitEditing={this.props.onSubmitEditing}
                />
            </View>
            <View style = {styles.textInputView}>
                <TextInput
                    allowFontScaling={false}
                    style={styles.textInput}
                    placeholder="Enter Mobile Number"
                    placeholderTextColor = {DARK_GRADIENT}
                    placeholderTextSize="18"
                    keyboardType="number-pad"
                    maxLength={10}
                    onChangeText={(text) => this.setState({venderMobileNumber :text})}
                    // value={this.props.value}
                    // onChangeText={this.props.onChangeText}
                    // returnKeyType="search"
                    // onSubmitEditing={this.props.onSubmitEditing}
                />
            </View>
            <View style = {styles.textInputView}>
                <TextInput
                    allowFontScaling={false}
                    style={styles.textInput}
                    placeholder="Enter Address"
                    placeholderTextColor = {DARK_GRADIENT}
                    placeholderTextSize="18"
                    keyboardType="default"
                    multiline={true}
                    onChangeText={(text) => this.setState({venderAddress :text})}
                    // value={this.props.value}
                    // onChangeText={this.props.onChangeText}
                    // returnKeyType="search"
                    // onSubmitEditing={this.props.onSubmitEditing}
                />
            </View>
            <View style = {styles.textInputView}>
                <TextInput
                    allowFontScaling={false}
                    style={styles.textInput}
                    placeholder="Enter Services"
                    placeholderTextColor = {DARK_GRADIENT}
                    placeholderTextSize="18"
                    keyboardType="default"
                    multiline={true}
                    onChangeText={(text) => this.setState({venderServices:text})}
                    // value={this.props.value}
                    // onChangeText={this.props.onChangeText}
                    // returnKeyType="search"
                    // onSubmitEditing={this.props.onSubmitEditing}
                />
            </View>

          </View>
          
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
      flex:1,
      padding:15,
  },
  card:{
    width:'100%',
    backgroundColor:'white',
    padding:10,
    borderRadius:8,
    marginBottom:10,
    shadowColor:DARK_GRADIENT,
    shadowOffset:{height:5,width:0},
    shadowOpacity:0.5,
    shadowRadius:5
  },
  sendBtn:{
    height:40,
    width:100,
    alignItems:'center',
    justifyContent:'center',
    borderRadius:8,
    marginTop:10,
    alignSelf:'flex-end',
    backgroundColor:DARK_GRADIENT  
  },
  textStyle:{
      color:'white',
      fontSize:18,
      textAlign:'center'
  },
  textInput:{
      fontSize: 15,
      color: DARK_GRADIENT,
      width: '95%',
      marginBottom:5 
  },
  textInputView:{
      width:'98%',
      alignSelf:'center',
      borderBottomWidth:1,
      borderColor:DARK_GRADIENT,
      marginBottom:20
  },
  imageBottom:{
      bottom:'20%',
      backgroundColor:'rgba(20,20,20,0.6)',
      width:'100%',
      height:'20%',
      justifyContent:'center'
  },
  addBtnView:{
    width:'100%',
    paddingHorizontal:10,
    // backgroundColor:'gray',
    height:40,
  },
  addBtn:{
    flex:1,
    backgroundColor:'rgba(30, 118, 161, 1)',
    borderRadius:10,
    justifyContent:'center',
    alignItems:'center'
  }
})
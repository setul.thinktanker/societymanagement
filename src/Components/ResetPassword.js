import React, { Component, Fragment } from 'react';
import {
    View,
    StatusBar,
    SafeAreaView,
    StyleSheet,
    Image,
    TextInput,
    TouchableOpacity,
    Alert
} from 'react-native';
import { connect } from "react-redux";
import { resetPassword } from "../Actions";
import { DARK_GRADIENT, LIGHT_GRADIENT } from '../Commons/Colors';
import CustomTextView from '../Custom/CustomTextView';
import Toast, { DURATION } from "react-native-easy-toast";
import LinearGradient from 'react-native-linear-gradient';
import Header from './Header';
import AsyncStorage from '@react-native-community/async-storage';
import { getUser } from "../Database/allSchema"

let userId = '';

class ResetPassword extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        this.getData();
    }

    getData() {
        console.log('get data')
        getUser()
            .then((user) => {
                userId = user[0].id
                console.log('user[0].id', user[0].id)
            })
            .catch((err) => {
                console.log('user[0].id_err', err);
            })
    }
    onBackPress() {
        this.props.navigation.goBack();
    }

    onPressReset = () => {
        const { oldPassword, newPassword, reEnterPassword } = this.state;
        isValid = true;
        if (!oldPassword) {
            isValid = false;
            this.refs.toast.show("Please enter old password", 2000);
        } else if (oldPassword.length < 6 && isValid) {
            isValid = false;
            this.refs.toast.show("Old password must be at least 6 characters.", 2000);
        }

        if (!newPassword && isValid) {
            isValid = false;
            this.refs.toast.show("Please enter new password", 2000);
        } else if (newPassword && (newPassword.length < 6) && isValid) {
            isValid = false;
            this.refs.toast.show("New password must be at least 6 characters.", 2000);
        }

        if (!reEnterPassword && isValid) {
            isValid = false;
            this.refs.toast.show("Please enter re-enter password", 2000);
        } else if ((newPassword != reEnterPassword) && isValid) {
            isValid = false;
            this.refs.toast.show(
                "Please enter re-enter password same as New password",
                2000
            );
        }

        if (isValid) {
            console.log('isValid')
            try {
                AsyncStorage.getItem('@authToken_Key')
                    .then((token) => {
                        console.log('token_reset', token)
                        this.props.resetPassword({
                            token,
                            userId,
                            oldPassword,
                            newPassword
                        })
                    })
                    .catch((err) => {
                        console.log('err', err)
                    })
            } catch (e) {
                console.log('Error in store authToken');
            }
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.authResult == 'Reset Successfully') {
            console.log('nextProps.msg', nextProps.msg)
            this.refs.toast.show(nextProps.msg, 2000);
        }
        if (nextProps.authResult == 'Reset Failed') {
            console.log('nextProps.msg', nextProps.msg)
            this.refs.toast.show(nextProps.msg, 2000);
        }
        if (nextProps.authResult == 'Reset Error') {
            Alert.alert('Oops', 'Somthing wents wrong please try again later.')
        }
    }
    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: DARK_GRADIENT }} />
                <SafeAreaView style={{ flex: 1 }}>
                    <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content" />
                    <Toast
                        ref="toast"
                        style={{ backgroundColor: '#1F90AC' }}
                        opacity={0.9}
                    />
                    <View style={{ height: 50 }}>
                        <Header
                            headerText={'Reset Password'}
                            leftImage={require('../img/left-arrow.png')}
                            onBackPress={() => this.onBackPress()}
                        />
                    </View>
                    <LinearGradient
                        start={{ x: 0, y: 0.3 }}
                        end={{ x: 1.5, y: 0 }}
                        colors={[DARK_GRADIENT, LIGHT_GRADIENT]}
                        style={styles.container}>
                        <View style={styles.topView}>
                            <Image
                                style={{ height: 100, width: 100, tintColor: 'white' }}
                                source={require('../img/reset_password.png')}
                            />
                        </View>

                        <View style={styles.card}>
                            <View style={styles.textInputView}>
                                <TextInput
                                    allowFontScaling={false}
                                    style={styles.textInput}
                                    placeholder="Enter old password"
                                    placeholderTextColor={DARK_GRADIENT}
                                    placeholderTextSize="18"
                                    keyboardType="default"
                                    secureTextEntry={true}
                                    onChangeText={text =>
                                        this.setState({ oldPassword: text })
                                    }
                                    value={this.state.oldPassword}
                                // onChangeText={this.props.onChangeText}
                                // returnKeyType="search"
                                // onSubmitEditing={this.props.onSubmitEditing}
                                />
                            </View>
                            <View style={styles.textInputView}>
                                <TextInput
                                    allowFontScaling={false}
                                    style={styles.textInput}
                                    placeholder="Enter new password"
                                    placeholderTextColor={DARK_GRADIENT}
                                    placeholderTextSize="18"
                                    keyboardType="default"
                                    secureTextEntry={true}
                                    onChangeText={text =>
                                        this.setState({ newPassword: text })
                                    }
                                    value={this.state.newPassword}
                                // onChangeText={this.props.onChangeText}
                                // returnKeyType="search"
                                // onSubmitEditing={this.props.onSubmitEditing}
                                />
                            </View>
                            <View style={styles.textInputView}>
                                <TextInput
                                    allowFontScaling={false}
                                    style={styles.textInput}
                                    placeholder="Re-enter password"
                                    placeholderTextColor={DARK_GRADIENT}
                                    placeholderTextSize="18"
                                    keyboardType="default"
                                    secureTextEntry={true}
                                    onChangeText={text =>
                                        this.setState({ reEnterPassword: text })
                                    }
                                    value={this.state.reEnterPassword}
                                // onChangeText={this.props.onChangeText}
                                // returnKeyType="search"
                                // onSubmitEditing={this.props.onSubmitEditing}
                                />
                            </View>
                        </View>
                        <TouchableOpacity
                            activeOpacity={0.9}
                            onPress={(this.props.isLoading) ? null : this.onPressReset}
                            style={styles.btn}
                        >
                            {this.props.isLoadingResetPwd ? (
                                <Image
                                    style={{
                                        height: 40,
                                        width: 40,
                                        backgroundColor: "transparent"
                                    }}
                                    source={require("../img/loader2.gif")}
                                />
                            ) : (
                                    <CustomTextView style={styles.textStyle} text="Reset" />
                                )}
                        </TouchableOpacity>

                    </LinearGradient>
                </SafeAreaView>
            </Fragment >
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
    },
    topView: {
        height: '30%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    },
    card: {
        width: '100%',
        backgroundColor: 'white',
        borderRadius: 8,
        shadowOffset: { height: 5, width: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 8,
        marginTop: 20,
        padding: 15,
        paddingTop: Platform.OS === "ios" ? 15 : 5,
        paddingBottom: Platform.OS === "ios" ? 15 : 20,

    },
    textInputView: {
        width: "98%",
        alignSelf: "center",
        borderBottomWidth: 1,
        borderColor: DARK_GRADIENT,
        marginBottom: Platform.OS === "ios" ? 20 : 5
    },
    textInput: {
        fontSize: 15,
        color: DARK_GRADIENT,
        width: "95%",
        marginLeft: Platform.OS === "ios" ? 0 : -3,
        marginBottom: Platform.OS === "ios" ? 5 : -10
    },
    btn: {
        height: 40,
        width: 100,
        borderRadius: 30,
        marginTop: 30,
        alignSelf: "flex-end",
        alignItems: "center",
        backgroundColor: DARK_GRADIENT,
        justifyContent: "center",
        elevation: 10
    },
    textStyle: {
        fontSize: 20,
        alignSelf: "center",
        color: "white"
    },
});
const mapStateToProps = state => {
    return {
        isLoadingResetPwd: state.Profile.isLoadingResetPwd,
        msg: state.Profile.msg,
        authResult: state.Profile.authResult
    };
};
export default connect(
    mapStateToProps,
    {
        resetPassword
    }
)(ResetPassword);
/* @flow */

import React, { Component, Fragment} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  ScrollView,
  Image,
  Picker,
  Modal,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Platform
} from 'react-native';
import { connect } from 'react-redux';
import LinearGradient from 'react-native-linear-gradient';
import CustomTextView from '../Custom/CustomTextView';
import CustomDropDown from '../Custom/CustomDropDown';
import {DARK_GRADIENT,LIGHT_GRADIENT} from '../Commons/Colors';
import Toast, {DURATION} from 'react-native-easy-toast'
import {signUpInitialState,userSignUp} from '../Actions';

class SignUpOtherDetails extends Component {
  constructor(props) {
      super(props);
      this.state = {
        
       showOwnerLivingInfo:false
      }
  }

  

  render() {
    return (
      <Fragment>
      <SafeAreaView style={{ flex:0, backgroundColor: DARK_GRADIENT }} />
      <SafeAreaView style={{ flex:1, backgroundColor: LIGHT_GRADIENT }}>
      <StatusBar backgroundColor = {DARK_GRADIENT} barStyle="light-content" />
      <LinearGradient
        colors={[DARK_GRADIENT,LIGHT_GRADIENT]}
        style={styles.container}>
        
        <ScrollView
          style = {{ flex:1 , height:'100%' , width:'100%' }}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{paddingBottom:90,paddingTop:40,paddingHorizontal:20}}
        >
        <View style = {styles.firstView}>
          <Image
              style={styles.imageStyle}
              source={require('../img/cityscape.png')}
          />
          <CustomTextView
            style={styles.text} 
            text="Society Management"/>
          <View style={styles.line} />
        </View>
        
        <View style={styles.inputView}>
          <Image
              style={styles.imageIconStyle}
              source={require('../img/address.png')}
          />
          <TextInput
            allowFontScaling={false}
            style={[styles.textInput,{bottom:2}]}
            placeholder="Enter Current Address"
            placeholderTextColor = "white"
            placeholderTextSize="15"
            keyboardType="default"
            multiline = {true}
            numberOfLines={5}
            onChangeText={(text) => this.setState({currentAddress :text})}
            // value={this.props.value}
            // onChangeText={this.props.onChangeText}
            // returnKeyType="search"
            // onSubmitEditing={this.props.onSubmitEditing}
           />
        </View>
        
        <Toast
          ref="toast"
          style={{backgroundColor:'#1A2980'}}
          opacity = {0.9}
        />

        </ScrollView>
      </LinearGradient>
     
      </SafeAreaView>
      </Fragment>
      

    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems:'center'
  },
  firstView:{
    justifyContent:'center',
    alignItems:'center'
  },
  imageStyle:{
    tintColor:'white',
    height:90,
    width:90,
    alignSelf:'center'
  },
  text:{
    color:'white',
    fontSize:10
  },
  line:{
    height:3,
    width:90,
    borderRadius:63,
    backgroundColor:'white',
    marginTop:1
  },
  inputView:{
    width:'100%',
    borderRadius:30,
    backgroundColor:'rgba(255,255,255,0.3)',
    marginTop:15,
    paddingHorizontal:5,
    flexDirection:'row',
    paddingVertical:Platform.OS === 'ios'? 10 : 0 
  },
  inputViewDropDown:{
    width:'100%',
    borderRadius:30,
    backgroundColor:'rgba(255,255,255,0.3)',
    marginTop:15,
    paddingHorizontal:5,
    flexDirection:'row',
    paddingVertical:Platform.OS === 'ios'? 10 : 14
  },
  inputTwoDropDown:{
    width:'100%',
    marginTop:15,
    flexDirection:'row',
  },
  imageIconStyle:{
    height:20,
    width:20,
    tintColor:'white',
    alignSelf:'center',
    marginLeft:8,
  },
  textInput:{
    fontSize: 14,
    color: 'white',
    width: '85%',
    marginLeft:8
  },
  textStyle:{
    fontSize:20,
    alignSelf:'center',
    color:'white',
  },
  btn:{
    height:40,
    width:100,
    borderRadius:30,
    marginTop:20,
    alignSelf:'flex-end',
    alignItems:'center',
    backgroundColor:'#1A2980',
    justifyContent:'center'
  },
  textStyle2:{
    fontSize:18,
    color:'#1A2980',
    alignSelf:'center'
  },
  linkStyle:{
    width:'100%',
    flexDirection:'row',
    alignItems:'center',
    justifyContent:'center',
    marginTop:20,
    height:40
  },
  arrowIconStyle:{
    height:18,
    width:18,
    tintColor:'white',
    alignSelf:'center',
    marginLeft:8
  },
  lineView:{
    borderRightWidth:1.5,
    borderColor:'white',
    borderRadius:20,
    height:'80%',
    alignSelf:'center',
    marginLeft:6
  }
});
const mapStateToProps = state =>{
  return{

  };
};

export default connect(
  mapStateToProps,
  {
    
  }
)(SignUpOtherDetails);

import React, { Component } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Animated,
  Easing,
  Image,
  UIManager,
  Platform,
  FlatList,
  Modal,
  Linking,
  WebView,
  LayoutAnimation
} from "react-native";
import CustomTextView from "../Custom/CustomTextView";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";
import Video from "react-native-video";

export default class ComplaintsItemLayout extends Component {
  constructor(props) {
    super(props);
    this.RotateValueHolder = new Animated.Value(0);
    this.state = {
      toggle: false,
      to: "0deg",
      from: "180deg",
      isImagePress: false,
      videoModalVisible: false,
      //for video
      rate: 1,
      volume: 1,
      muted: false,
      resizeMode: "contain",
      duration: 0.0,
      currentTime: 0.0,
      paused: true
    };
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }
  StartImageRotateFunction() {
    //linear,easeInEaseOut
    LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);

    this.setState(
      {
        to: this.state.toggle ? "180deg" : "360deg",
        from: this.state.toggle ? "0deg" : "180deg",
        toggle: !this.state.toggle
      },
      () => {
        this.RotateValueHolder.setValue(0);
        Animated.timing(this.RotateValueHolder, {
          toValue: 1,
          duration: 500,
          easing: Easing.linear
        }).start();
      }
    );
  }
  onPressImage(arr) {
    console.warn("onpress image");
    this.setState({ isImagePress: true, imgUri: arr });
  }
  onLoad = data => {
    this.setState({ duration: data.duration });
  };
  onProgress = data => {
    this.setState({ currentTime: data.currentTime });
  };
  onEnd = () => {
    this.setState({ paused: true });
    // this.video.seek(0);
  };
  getCurrentTimePercentage() {
    if (this.state.currentTime > 0) {
      return (
        parseFloat(this.state.currentTime) / parseFloat(this.state.duration)
      );
    }
    return 0;
  }
  onPressViewMore = () => {
    this.StartImageRotateFunction();
  };
  render() {
    const flexCompleted = this.getCurrentTimePercentage() * 100;
    const flexRemaining = (1 - this.getCurrentTimePercentage()) * 100;

    // const { uri } = this.props.item.video;

    const RotateData = this.RotateValueHolder.interpolate({
      inputRange: [0, 1],
      outputRange: [this.state.to, this.state.from]
    });
    return (
      <View style={{ flex: 1, marginTop: this.props.item.inSociety ? 8 : 4 }}>
        <View style={styles.label}>
          <CustomTextView
            numberOfLines={1}
            text="Pending"
            style={{ fontSize: 12, textAlign: "center", color: "white" }}
          />
        </View>
        <LinearGradient
          start={{ x: 0, y: 0.3 }}
          end={{ x: 1.5, y: 0 }}
          colors={[DARK_GRADIENT, LIGHT_GRADIENT]}
          style={styles.container}
        >
          <View style={{ flex: 1 }}>
            <View
              style={{ flexDirection: "row", width: "100%", marginBottom: 5 }}
            >
              <CustomTextView
                numberOfLines={1}
                text={this.props.item.Subject}
                style={[
                  styles.textStyle,
                  { width: "85%", fontSize: 20, fontWeight: "700", marginTop: 0 }
                ]}
              />
            </View>
            <View style={{ flexDirection: "row", width: "100%" }}>
              <CustomTextView
                numberOfLines={5}
                text={this.props.item.description}
                style={[styles.textStyle, { width: "90%" }]}
              />
            </View>
            <View
              style={{ height: this.state.toggle ? null : 0, overflow: "hidden" }}
            >
              <FlatList
                style={{ marginRight: 30 }}
                contentContainerStyle={{ marginTop: 8 }}
                data={this.props.item.images.uri}
                horizontal
                showsHorizontalScrollIndicator={false}
                renderItem={({ item, index }) => {
                  let arr = item.path;
                  return (
                    <TouchableOpacity
                      onPress={() => {
                        this.onPressImage(arr);
                      }}
                    // onLongPress = {()=>{
                    //   this.onPressImage(arr)
                    // }}
                    // onPressOut = {()=>{
                    //   this.setState({isImagePress:false})
                    // }}
                    >
                      <Image
                        style={{
                          height: 90,
                          width: 90,
                          backgroundColor: "black",
                          margin: 1
                        }}
                        source={require("../img/complaint_img.jpg")}
                      // source={{uri:arr}}
                      />
                    </TouchableOpacity>
                  );
                }}
              />
              <View
                style={{
                  marginTop: 5,
                  height: 90,
                  width: 90,
                  margin: 1,
                  backgroundColor: "black"
                }}
              >
                <TouchableOpacity
                  activeOpacity={0.8}
                  onPress={() => {
                    this.props.onPressVideo();
                  }}
                  style={{ flex: 1 }}
                >
                  <Video
                    ref={ref => {
                      this.player = ref;
                    }}
                    source={{ uri: this.props.item.video }}
                    style={styles.fullScreen}
                    paused={this.state.paused}
                    resizeMode={"contain"}
                    onLoad={this.onLoad}
                    onProgress={this.onProgress}
                    onEnd={this.onEnd}
                    repeat
                  />
                  <View
                    style={{
                      flex: 1,
                      height: "100%",
                      width: "100%",
                      alignItems: "center",
                      justifyContent: "center",
                      position: "absolute"
                      // backgroundColor: "rgba(40,40,40,0.6)"
                    }}
                  >
                    <Image
                      style={{ tintColor: "white", height: 40, width: 40 }}
                      source={require("../img/play.png")}
                    />
                  </View>
                </TouchableOpacity>
              </View>
              {/* <View
              style={{ flex: 1, marginTop: 5, height: 150, marginRight: 34 }}
            >
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {
                  this.props.onPressVideo();
                  // this.setState({ paused: !this.state.paused });
                }}
                style={{ flex: 1 }}
              >
                <Video
                  ref={ref => {
                    this.player = ref;
                  }}
                  source={{ uri: this.props.item.video }}
                  style={styles.fullScreen}
                  paused={this.state.paused}
                  resizeMode={"contain"}
                  onLoad={this.onLoad}
                  onProgress={this.onProgress}
                  onEnd={this.onEnd}
                  repeat
                />
                <View style={styles.controls}>
                  {this.state.paused ? (
                    <Image
                      style={{ height: 20, width: 20, tintColor: "white" }}
                      source={require("../img/pause.png")}
                    />
                  ) : (
                    <Image
                      style={{ height: 20, width: 20, tintColor: "white" }}
                      source={require("../img/play.png")}
                    />
                  )}
                  <View style={styles.progress}>
                    <View
                      style={[
                        styles.innerProgressCompleted,
                        { flex: flexCompleted }
                      ]}
                    />
                    <View
                      style={[
                        styles.innerProgressRemaining,
                        { flex: flexRemaining }
                      ]}
                    />
                  </View>
                </View>
              </TouchableOpacity>
            </View> */}
            </View>
            <View style={styles.viewMore}>
              <TouchableOpacity onPress={this.onPressViewMore} style={styles.btn}>
                <Animated.Image
                  style={{
                    height: 15,
                    width: 15,
                    tintColor: "white",
                    top: 1,
                    transform: [{ rotate: RotateData }]
                  }}
                  source={require("../img/downArrow.png")}
                />
              </TouchableOpacity>
            </View>
          </View>
        </LinearGradient>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    padding: 8,
    borderRadius: 8,
    marginBottom: 10
  },
  textStyle: {
    color: "white",
    fontSize: 15,
    marginTop: 5
  },
  viewMore: {
    height: 30,
    width: 30,
    borderRadius: 30 / 2,
    alignSelf: "flex-end",
    position: "absolute",
    bottom: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "rgba(255,255,255,0.4)"
  },
  btn: {
    flex: 1,
    height: "100%",
    width: "100%",
    alignItems: "center",
    justifyContent: "center"
  },

  fullScreen: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  controls: {
    flexDirection: "row",
    backgroundColor: "transparent",
    borderRadius: 5,
    position: "absolute",
    bottom: 20,
    left: 20,
    right: 20,
    alignItems: "center"
  },
  progress: {
    flex: 1,
    flexDirection: "row",
    borderRadius: 3,
    overflow: "hidden",
    marginLeft: 10
  },
  innerProgressCompleted: {
    height: 10,
    backgroundColor: "#f1a91b"
  },
  innerProgressRemaining: {
    height: 10,
    backgroundColor: "#2C2C2C"
  },
  containerVideo: {
    flex: 1,
    backgroundColor: "green",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(40,40,40,0.6)"
  },
  label: {
    alignSelf: "flex-end",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 2,
    height: 15,
    width: "20%",
    top: 0,
    position: "absolute",
    zIndex: 1111,
    backgroundColor: DARK_GRADIENT
  }
});

import React, { Component, Fragment } from "react";
import {
  View,
  StyleSheet,
  SafeAreaView,
  StatusBar,
  FlatList
} from "react-native";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";
import Header from "./Header";
import NotificationItemLayout from "./NotificationItemLayout";

export default class Notification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notifications: [
        {
          description:
            "notification description for society members to paying maintenancenotification description for society members to paying maintenance.",
          date: "2-8-2019"
        },
        {
          description:
            "notification description for society members to paying maintenancenotification description for society members to paying maintenance.",
          date: "1-8-2019"
        }
      ]
    };
  }
  onBackPress() {
    this.props.navigation.goBack();
  }
  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: DARK_GRADIENT }} />
        <SafeAreaView style={{ flex: 1 }}>
          <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content" />
          <View style={styles.container}>
            <View style={{ height: 50 }}>
              <Header
                headerText={"Notifications"}
                leftImage={require("../img/left-arrow.png")}
                onBackPress={() => this.onBackPress()}
              />
            </View>
            <FlatList
              // style = {{padding:10}}
              contentContainerStyle={{ padding: 10 }}
              data={this.state.notifications}
              renderItem={({ item, index }) => {
                return <NotificationItemLayout item={item} />;
              }}
            />
          </View>
        </SafeAreaView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
    //   padding:10
  }
});

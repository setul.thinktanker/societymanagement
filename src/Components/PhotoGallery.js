import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, FlatList, Image } from 'react-native';
import Header from "./Header";

class PhotoGallery extends Component {
    constructor (props) {
        super(props);
        this.state = {
            listData: [
                {
                    id: 1,
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    id: 2,
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    id: 3,
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    id: 4,
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    id: 5,
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    id: 6,
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    id: 7,
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    id: 8,
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                }
            ]
        };
    }

    onBackPress() {
        this.props.navigation.goBack();
    }

    render() {
        return (
            <View style={ { flex: 1, backgroundColor: '#F5F5F5' } }>
                <View style={ { height: 50 } }>
                    <Header
                        headerText={ "Photo Gallery" }
                        leftImage={ require("../img/left-arrow.png") }
                        onBackPress={ () => this.onBackPress() }
                    />
                </View>
                <View style={ { flex: 1 } }>
                    <FlatList
                        data={ this.state.listData }
                        numColumns={ 2 }
                        renderItem={ ({ item, index }) =>
                            this.displayPhotoData(item, index) }
                    />
                </View>
            </View>
        );
    }

    displayPhotoData(item, index) {
        return (
            <TouchableOpacity activeOpacity={ 2.0 }
                style={ myStyles.topViewStyle }
                onPress={ () => this.props.navigation.navigate('PhotoGalleryDetails', { id: item.id }) }>
                <View style={ myStyles.rowStyle }>
                    <Image style={ myStyles.imgStyle } source={ { uri: item.image } } />
                    <Text allowFontScaling={ false } style={ myStyles.txtStyle }>{ item.title } { index + 1 }</Text>
                </View>
            </TouchableOpacity>
        )
    }
}

const myStyles = StyleSheet.create({
    topViewStyle: {
        elevation: 2,
        margin: 3,
        flex: 1,
        borderRadius: 8,
        backgroundColor: '#FFFFFF'
    },
    rowStyle: {
        flexDirection: 'column',
    },
    txtStyle: {
        fontSize: 24,
        padding: '2%',
        color: '#000000',

    },
    imgStyle: {
        height: 150,
        width: '100%'
    }
});

export default PhotoGallery;

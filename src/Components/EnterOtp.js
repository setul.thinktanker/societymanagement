import React, { Component, Fragment } from 'react';
import {
    View,
    StyleSheet,
    StatusBar,
    SafeAreaView,
    Image,
    TouchableOpacity
} from 'react-native';
import { DARK_GRADIENT, LIGHT_GRADIENT } from '../Commons/Colors';
import LinearGradient from 'react-native-linear-gradient';
import CustomTextView from '../Custom/CustomTextView';
import CustomConfirmationCodeInput from '../Custom/CustomConfirmationCodeInput';
import Header from './Header';
let confirmResultState;
export default class EnterOtp extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    componentWillMount() {
        confirmResultState = this.props.navigation.getParam('confirmResult'),
            this.setState({
                fname: this.props.navigation.getParam('fname'),
                confirmResult: this.props.navigation.getParam('confirmResult'),
                lname: this.props.navigation.getParam('lname'),
                userId: this.props.navigation.getParam('userId'),
                mobileNo: this.props.navigation.getParam('mobileNo'),
                email: this.props.navigation.getParam('email'),
                selectedGender: this.props.navigation.getParam('selectedGender'),
                selectedHouseType: this.props.navigation.getParam('selectedHouseType'),
                selectedBlock: this.props.navigation.getParam('selectedBlock'),
                selectedBlockNo: this.props.navigation.getParam('selectedBlockNo'),
                isOwnerStayInSociety: this.props.navigation.getParam('isOwnerStayInSociety'),
                currentAddress: this.props.navigation.getParam('currentAddress'),
                isRented: this.props.navigation.getParam('isRented'),
                password: this.props.navigation.getParam('password')
            })

        console.warn('confirmResultState', confirmResultState)
    }
    onBackPress() {
        this.props.navigation.goBack();
    }
    confirmCode = () => {
        console.warn('in confirm code')
        const { codeInput, confirmResult } = this.state;

        if (confirmResultState && codeInput.length) {
            confirmResultState.confirm(codeInput)
                .then((user) => {
                    console.warn('user in then', user)
                })
                .catch((error) => {
                    console.warn('user in catch', error)
                });
        }
    };
    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: DARK_GRADIENT }} />
                <SafeAreaView style={{ flex: 1 }}>
                    <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content" />
                    <View style={{ height: 50 }}>
                        <Header
                            headerText={'Verify Number'}
                            leftImage={require('../img/left-arrow.png')}
                            onBackPress={() => this.onBackPress()}
                        />
                    </View>
                    <LinearGradient
                        start={{ x: 0, y: 0.3 }}
                        end={{ x: 1.5, y: 0 }}
                        colors={[DARK_GRADIENT, LIGHT_GRADIENT]}
                        style={styles.container}
                    >
                    </LinearGradient>
                    <View style={styles.mainImgView}>
                        <LinearGradient
                            colors={[LIGHT_GRADIENT, DARK_GRADIENT]}
                            style={styles.mainView}
                        >
                            <View style={styles.imgView}>
                                <Image
                                    style={{ height: 60, width: 60, tintColor: DARK_GRADIENT }}
                                    source={require('../img/phone.png')} />
                            </View>
                        </LinearGradient>
                    </View>
                    <View style={styles.textView}>
                        <CustomTextView style={styles.textStyle} text="We have sent you One Time Password via SMS for Mobile number verifications" />
                        <CustomTextView style={[styles.textStyle, { color: '#1E76A1', marginTop: 20 }]} text="Enter OTP here" />
                        <CustomConfirmationCodeInput
                            ref='codeInputRef2'
                            allowFontScaling={false}
                            space={5}
                            size={40}
                            autoFocus={false}
                            borderType='border-circle'
                            inactiveColor={DARK_GRADIENT}
                            activeColor={DARK_GRADIENT}
                            inputPosition='center'
                            codeLength={4}
                            onFulfill={code => { this.setState({ codeInput: code }) }}
                            codeInputStyle={{ fontSize: 20, borderRadius: 30, borderWidth: 1.5 }}
                        />

                    </View>
                    <View style={{ flex: 1, alignItems: 'center', marginTop: 40 }}>

                        <View style={styles.roundView}>
                            <TouchableOpacity
                                onPress={this.confirmCode}
                                style={styles.roundView}>
                                <Image
                                    style={{ height: 30, width: 30, tintColor: 'white' }}
                                    source={require('../img/right.png')} />
                            </TouchableOpacity>
                        </View>

                        <TouchableOpacity>
                            <CustomTextView style={[styles.textStyle, { color: '#1E76A1', fontWeight: '800', marginTop: 50 }]} text="Resend Code" />
                        </TouchableOpacity>


                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        height: '16%'
    },
    mainImgView: {
        width: '100%',
        top: '16%',
        alignItems: 'center',
        justifyContent: 'center',
        position: 'absolute',
    },
    imgView: {
        backgroundColor: "white",
        height: 96,
        width: 96,
        borderRadius: 96 / 2,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center"
    },
    mainView: {
        height: 100,
        width: 100,
        borderRadius: 100 / 2,
        alignItems: "center",
        justifyContent: "center"
    },
    textView: {
        padding: 10,
        width: '100%',
        marginTop: 60,
        // backgroundColor: 'gray'
    },
    textStyle: {
        fontSize: 18,
        color: DARK_GRADIENT,
        textAlign: 'center'
    },
    roundView: {
        height: 60,
        width: 60,
        borderRadius: 60 / 2,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: DARK_GRADIENT,
        shadowColor: DARK_GRADIENT,
        shadowOffset: { height: 5, width: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 10
    }
})
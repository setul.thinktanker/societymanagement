import React, { Component } from "react";
import { View, TouchableOpacity, StyleSheet, Animated } from "react-native";
import CustomTextView from "../Custom/CustomTextView";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";

export default class MaintenanceItemLayout extends Component {
  constructor(props) {
    super(props);
    this.state = { scaleValue: new Animated.Value(0) };
  }
  componentDidMount() {
    Animated.timing(this.state.scaleValue, {
      toValue: 1,
      duration: 600,
      delay: this.props.index * 350
    }).start();
  }
  //₹
  render() {
    return (
      <Animated.View
        style={[styles.container, { opacity: this.state.scaleValue }]}
      >
        <View style={styles.RupeesViewStyle}>
          <CustomTextView text="Rs." style={{ fontSize: 20, color: "white" }} />
          <CustomTextView
            style={styles.descriptionStyle}
            text={this.props.item.rupees}
          />
        </View>
        <View style={{ width: "70%", paddingRight: 10, paddingBottom: 5 }}>
          <View style={{ flexDirection: "row" }}>
            <View
              style={{
                width: "1%",
                backgroundColor: "white",
                marginHorizontal: 5
              }}
            />
            <CustomTextView
              style={[
                styles.descriptionStyle,
                { color: DARK_GRADIENT, fontSize: 20 }
              ]}
              text={this.props.item.Subject}
            />
          </View>
          <CustomTextView
            style={[styles.dateStyle, { color: "#1C5C96" }]}
            text={this.props.item.startDate}
          />
        </View>
      </Animated.View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    // width: "100%",
    flexDirection: "row",
    backgroundColor: "white",
    padding: 0,
    borderRadius: 8,
    marginBottom: 10,
    shadowColor: DARK_GRADIENT,
    shadowOffset: { height: 5, width: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5
  },
  descriptionStyle: {
    fontSize: 16,
    marginRight: 5,
    color: "white"
  },
  dateStyle: {
    fontSize: 12,
    color: "#1E86A7",
    textAlign: "right"
  },
  RupeesViewStyle: {
    flexDirection: "row",
    width: "30%",
    alignItems: "center",
    justifyContent: "center",
    // padding: 10,
    // height: 80,
    borderTopLeftRadius: 8,
    borderBottomLeftRadius: 8,
    backgroundColor: DARK_GRADIENT
  }
});

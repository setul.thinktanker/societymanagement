import React, { Component } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Animated,
  Image
} from "react-native";
import CustomTextView from "../Custom/CustomTextView";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";

export default class VehiclesDetailsItemLayout extends Component {
  constructor (props) {
    super(props);
    this.state = {};
  }

  componentWillMount() { }

  render() {
    return (
      <LinearGradient
        start={ { x: 0, y: 0.3 } }
        end={ { x: 1.5, y: 0 } }
        colors={ [DARK_GRADIENT, LIGHT_GRADIENT] }
        style={ styles.container }
      >
        <View style={ { flexDirection: "row", width: "100%", marginBottom: 5 } }>
          <CustomTextView
            numberOfLines={ 1 }
            text={ this.props.item.name }
            style={ [
              styles.textStyle,
              { width: "70%", fontSize: 20, fontWeight: "700" }
            ] }
          />
        </View>
        <View style={ { flexDirection: "row", width: "100%" } }>
          <CustomTextView
            numberOfLines={ 1 }
            text={ "Vehicle No :" }
            style={ [styles.textStyle, { width: "30%" }] }
          />
          <CustomTextView
            numberOfLines={ 1 }
            text={ this.props.item.vehicle_number }
            style={ [styles.textStyle, { width: "70%" }] }
          />
        </View>
        <View style={ { flexDirection: "row", width: "100%" } }>
          <CustomTextView
            numberOfLines={ 1 }
            text={ "House No. : " }
            style={ [styles.textStyle, { width: "30%" }] }
          />
          <CustomTextView
            numberOfLines={ 1 }
            text={ this.props.item.block }
            style={ [styles.textStyle, { width: "70%" }] }
          />
        </View>
        <View style={ { flexDirection: "row", width: "100%" } }>
          <CustomTextView
            numberOfLines={ 1 }
            text={ "Mobile No. : " }
            style={ [styles.textStyle, { width: "30%" }] }
          />
          <CustomTextView
            numberOfLines={ 1 }
            text={ this.props.item.mobile_no }
            style={ [styles.textStyle, { width: "70%" }] }
          />
        </View>

        {/* <View style = {{flexDirection:'row',width:'100%'}}>
        <CustomTextView
            numberOfLines = {1}
            text ={'Vehicle : '}
            style={[styles.textStyle,{width:'30%'}]} />
        <CustomTextView
            numberOfLines = {1}
            text ={this.props.item.VType}
            style={[styles.textStyle,{width:'70%'}]} />
      </View> */}
        <View style={ styles.vehicleMainView }>
          <Image source={ this.props.item.VImg } style={ styles.imgStyle } />
        </View>
      </LinearGradient>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    padding: 8,
    borderRadius: 8,
    marginBottom: 10
  },
  nameView: {
    padding: 3
  },
  textStyle: {
    color: "white",
    fontSize: 15,
    marginBottom: 5
  },
  vehicleMainView: {
    // height:'40%',
    // width:'30%',
    // backgroundColor:'gray',
    position: "absolute",
    right: 0,
    bottom: 0
  },
  imgStyle: {
    height: 70,
    width: 70,
    tintColor: "white",
    alignSelf: "flex-end",
    marginRight: 5
  }
});

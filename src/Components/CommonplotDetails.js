import React, { Component , Fragment} from 'react';
import { View,
    StatusBar ,
    SafeAreaView} from 'react-native';
import {DARK_GRADIENT,LIGHT_GRADIENT} from '../Commons/Colors';
import LinearGradient from 'react-native-linear-gradient';
import Header from './Header';

export default class CommonplotDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  onBackPress(){
    this.props.navigation.goBack();
  }
  render() {
    return (
        <Fragment>
        <SafeAreaView style={{ flex:0, backgroundColor: DARK_GRADIENT }} />
        <SafeAreaView style={{ flex:1}}>
        <StatusBar backgroundColor = {DARK_GRADIENT} barStyle="light-content" />
        <View style= {{height:50}}>
            <Header
                headerText = {'Book A Commonplot'}
                leftImage = {require('../img/left-arrow.png')}
                onBackPress = {()=>this.onBackPress()}
            />
        </View>
        </SafeAreaView>
        </Fragment>
    );
  }
}

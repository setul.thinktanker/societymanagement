import React, { Component, Fragment } from "react";
import { View, StatusBar, SafeAreaView, FlatList, Alert } from "react-native";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";
import ProfileDetailsItemLatout from "./ProfileDetailsItemLayout";
import { deleteUser } from "../Database/allSchema";
import Header from "./Header";

const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);
  let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
  while (
    numberOfElementsLastRow != numColumns &&
    numberOfElementsLastRow != 0
  ) {
    data.push({ key: "blank-${numberOfElementsLastRow}", empty: true });
    numberOfElementsLastRow++;
  }
  return data;
};
const numColumns = 2;

export default class ProfileDetails extends Component {
  constructor (props) {
    super(props);
    this.state = {
      list: [
        { title: "Edit Profile", img: require("../img/user.png") },
        { title: "Edit Vender", img: require("../img/engineer.png") },
        { title: "Edit Vehicles", img: require("../img/vehicle.png") },
        { title: "Reset Password", img: require("../img/reset_password.png") },
        { title: "Edit Event", img: require('../img/eventCal.png') }


      ]
    };
  }
  logout() {
    deleteUser().then(() => {
      this.props.navigation.replace('Login');
    }).catch((err) => {
      Alert.alert('Failed', 'Something went wrong please try again later.');
      console.warn('err logout', err);
    })
  }
  onPressLogout() {
    Alert.alert(
      'Logout',
      'Are you sure, you want to logout?',
      [

        {
          text: 'Cancel',
          onPress: () => console.log('OK Pressed'),
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => { this.logout() },

        }
      ],
      { cancelable: false },
    );
  }
  onBackPress() {
    this.props.navigation.goBack();
  }
  onPressItem(item) {
    console.warn("itempressed", item);
    if (item == "Edit Profile") {
      this.props.navigation.navigate("Profile");
    } else if (item == "Edit Vender") {
      this.props.navigation.navigate("Venders");
    } else if (item == "Edit Vehicles") {
      this.props.navigation.navigate("AddVehicle");
    } else if (item == "Reset Password") {
      this.props.navigation.navigate("ResetPassword");
    } else if (item == "Edit Event") {
      this.props.navigation.navigate("EditEvent");
    }
  }
  render() {
    return (
      <Fragment>
        <SafeAreaView style={ { flex: 0, backgroundColor: DARK_GRADIENT } } />
        <SafeAreaView style={ { flex: 1 } }>
          <StatusBar backgroundColor={ DARK_GRADIENT } barStyle="light-content" />
          <View style={ { height: 50 } }>
            <Header
              headerText={ "My Profile" }
              leftImage={ require("../img/left-arrow.png") }
              onBackPress={ () => this.onBackPress() }
              rightImage={ require("../img/exit.png") }
              onRightPress={ () => this.onPressLogout() }
            />
          </View>
          <LinearGradient
            start={ { x: 1.2, y: 0.5 } }
            end={ { x: 0.5, y: 1 } }
            colors={ [DARK_GRADIENT, LIGHT_GRADIENT] }
            style={ { flex: 1 } }
          >
            <FlatList
              alwaysBounceVertical={ false }
              data={ formatData(this.state.list, numColumns) }
              numColumns={ numColumns }
              contentContainerStyle={ {
                padding: 10,
                justifyContent: "space-between"
              } }
              showsVerticalScrollIndicator={ false }
              renderItem={ ({ item, index }) => {
                if (!item.empty) {
                  console.log(item.title);
                  return (
                    <ProfileDetailsItemLatout
                      index={ index }
                      item={ item }
                      onPressItem={ () => {
                        // console.warn(item.title);
                        this.onPressItem(item.title);
                      } }
                    />
                  );
                } else {
                  return <View style={ { flex: 1, margin: 10 } } />;
                }
              } }
            />
          </LinearGradient>
        </SafeAreaView>
      </Fragment>
    );
  }
}

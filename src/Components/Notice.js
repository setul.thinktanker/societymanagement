import React, {Component, Fragment} from "react";
import {View, StyleSheet, SafeAreaView, StatusBar, FlatList, Animated, Linking, Image, Text} from "react-native";
import {DARK_GRADIENT, LIGHT_GRADIENT} from "../Commons/Colors";
import Header from "./Header";
import NoticeItemLayout from "./NoticeItemLayout";
import AsyncStorage from "@react-native-community/async-storage";
import {getUser} from "../Database/allSchema";
import {connect} from "react-redux";
import VendorDetailsItemLayout from "./VendorDetailsItemLayout";
import Swipeable from "react-native-gesture-handler/Swipeable";
import {RectButton} from "react-native-gesture-handler";
import {getNotice, delNotice} from "../Actions";

let authToken = '';
let userId = '';
let societyId = '';

class Notice extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // venderData: [],
            venderData: [
                // {
                //     description: "First",
                //     date: "2-8-2019"
                // },
                // {
                //     description: "Second",
                //     date: "1-8-2019"
                // },
            ]
        };
    }

    componentWillMount() {
        this.getTokenValue();
    }

    getTokenValue() {
        try {
            AsyncStorage.getItem('@authToken_Key')
                .then((token) => {
                    console.log('token', token);
                    authToken = token;
                    getUser().then((user) => {
                        console.log("User data:" + JSON.stringify(user));
                        societyId = user[0].societyId;
                        userId = user[0].id;

                        console.log("societyId", societyId);
                        console.log("userId", userId);
                        console.log("authToken", authToken);
                        this.props.getNotice(societyId, userId, authToken);

                    }).catch((err) => {
                        console.log('err', err)
                    });
                })
                .catch((err) => {
                    console.log('err', err)
                })
        } catch (e) {
            console.log('Error in store authToken');
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        console.log("componentWillReceiveProps");
        console.log(nextProps.response.data);
        // console.log(nextProps.response);
        // this.state.venderData = [];

        // if (nextProps.data.success) {
        //     console.log("DELETE SUCCESS")
        //     //again load list
        // } else {
        //     console.log("DELETE FAILED")
        // }

        //call this on success delete
        this.state.venderData = nextProps.response.data;
    }

    onBackPress() {
        this.props.navigation.goBack();
    }

    //edit vendor
    renderRightActionEdit = (text, color, x, progress, item) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0]
        });

        return (
            <Animated.View style={[styles.animateViewStyle, {transform: [{translateX: trans}]}]}>
                <RectButton style={[styles.rightAction, {backgroundColor: color}]}
                            onPress={() => {
                                // this.props.navigation.navigate("AddVender", {item: item});
                            }}>
                    <Image style={[styles.actionImg, {tintColor: "white"}]}
                           source={require("../img/edit.png")}/>
                </RectButton>
            </Animated.View>
        );
    };

    //delete
    renderRightActionDelete = (text, color, x, progress, item, index) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0]
        });

        // const onpress = () => {
        //     // this._swipeableRow.close();
        //
        // };

        return (
            <Animated.View
                style={[
                    styles.animateViewStyle,
                    {transform: [{translateX: trans}]}
                ]}>
                <RectButton
                    style={[styles.rightAction, {backgroundColor: color}]}
                    onPress={() => {
                        // console.warn("Delete clicked!");
                        console.warn("item.date", item.date);
                        // this.props.delNotice(item.id);
                        this.deleteNotice(index, item.id);
                    }
                    }>
                    <Image
                        style={[styles.actionImg, {tintColor: "white"}]}
                        source={require("../img/delete.png")}
                    />
                </RectButton>
            </Animated.View>
        );
    };

    renderRightActions(progress, item, index) {
        return (
            <View style={{width: 250, flexDirection: "row"}}>
                {/*{this.renderRightActionCall("Call", "#2DC170", 80, progress, item)}*/}
                {/*{this.renderRightActionEdit("Edit", "#2D6CC1", 80, progress, item)}*/}
                {this.renderRightActionDelete("Delete", "#C13D2D", 80, progress, item, index)}
            </View>
        );
    }

    renderRightActionCall = (text, color, x, progress, item) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0]
        });
        return (
            <Animated.View
                style={[
                    styles.animateViewStyle,
                    {transform: [{translateX: trans}]}
                ]}>
                <RectButton
                    style={[styles.rightAction, {backgroundColor: color}]}
                    onPress={() => {
                        Linking.openURL(`tel:${item.mobileNo}`);
                    }}>
                    <Image
                        style={[styles.actionImg, {tintColor: "white"}]}
                        source={require("../img/call.png")}
                    />
                </RectButton>
            </Animated.View>
        );
    };

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{flex: 0, backgroundColor: DARK_GRADIENT}}/>
                <SafeAreaView style={{flex: 1}}>
                    <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content"/>
                    <View style={styles.container}>
                        <View style={{height: 50}}>
                            <Header
                                headerText={"Notice"}
                                leftImage={require("../img/left-arrow.png")}
                                onBackPress={() => this.onBackPress()}
                            />
                        </View>

                        {
                            this.state.venderData ?
                                <FlatList
                                    contentContainerStyle={{padding: 10}}
                                    data={this.state.venderData}
                                    extraData={this.state.venderData}
                                    keyExtractor={item => item.id}
                                    renderItem={({item, index}) => {
                                        return (
                                            <Swipeable
                                                ref={this.updateRef}
                                                friction={1}
                                                leftThreshold={30}
                                                rightThreshold={40}
                                                renderRightActions={progress =>
                                                    this.renderRightActions(progress, item, index)
                                                }>

                                                <NoticeItemLayout item={item}/>
                                            </Swipeable>
                                        )
                                    }}
                                /> : null
                        }
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }

    deleteNotice = (index, id) => {
        console.warn("index:", index);
        console.warn("id:", id);
        let allItems = [...this.state.venderData];
        let filteredItems = allItems.filter(vendor => vendor.id !== id);
        this.setState({venderData: filteredItems});
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
        //   padding:10
    },
    animateViewStyle: {
        flex: 1,
        marginBottom: 5,
        marginTop: 10
    },
    actionText: {
        color: "white",
        fontSize: 20,
        backgroundColor: "transparent",
        padding: 10
    },
    rightAction: {
        alignItems: "center",
        flex: 1,
        borderRadius: 8,
        marginLeft: 5,
        justifyContent: "center"
    },
    actionImg: {
        height: 40,
        width: 20
    },
});

const mapStateToProps = state => {
    return {
        isNoticeLoading: state.Notice.isLoading,
        response: state.Notice.noticeList,
        success: state.Notice.success,
    };
};

export default connect(mapStateToProps, {getNotice, delNotice})(Notice);

/* @flow */

import React, {Component, Fragment} from "react";
import {
    View,
    BackHandler,
    StyleSheet,
    FlatList,
    ScrollView,
    Animated,
    Easing,
    Dimensions,
    SafeAreaView, TouchableHighlight
} from "react-native";
import Header from "./Header";
import HomeItemLayout from "./HomeItemLayout";
import CustomTextView from "../Custom/CustomTextView";
import {LIGHT_GRADIENT, DARK_GRADIENT} from "../Commons/Colors";
import {societyMembersList} from "../Actions";
import {connect} from "react-redux";
import AsyncStorage from '@react-native-community/async-storage';

class Home extends Component {
    constructor(props) {
        super(props);
        this.animatedValue1 = new Animated.Value(0);
        this.state = {
            data: [
                {
                    name: "Profile",
                    img: require("../img/user.png"),
                    color: "#22C4C2"
                },
                {
                    name: "Vehicles",
                    img: require("../img/vehicle.png"),
                    color: "#20AAB7"
                },
                {
                    name: "Vendors",
                    img: require("../img/engineer.png"),
                    color: "#1F90AC"
                },
                {
                    name: "Complaints",
                    img: require("../img/complaint.png"),
                    color: "#1E76A1"
                },
                {
                    name: "Maintenance",
                    img: require("../img/tools.png"),
                    color: "#1C5C96"
                },
                // {
                //   name: "Book A Commonplot",
                //   img: require("../img/booking.png"),
                //   color: "#1B428B"
                // },
                {
                    name: "Rule Book",
                    img: require("../img/documents.png"),
                    color: "#1B428B"
                    // color: "#1A2980"
                },
                {
                    name: "Notice",
                    img: require("../img/notification.png"),
                    color: "#2522C4C2"
                },
            ]
        };
    }

    onPressItem(name) {
        console.warn(name);
        if (name == "Profile") {
            this.props.navigation.navigate("ProfileDetails");
        }
        if (name == "Notice") {
            this.props.navigation.navigate("Notice");
        }
        if (name == "Vehicles") {
            this.props.navigation.navigate("VehiclesDetails");
        }
        if (name == "Vendors") {
            //shows all vendors
            this.props.navigation.navigate("VenderDetails");
        }
        if (name == "Complaints") {
            this.props.navigation.navigate("Complaints");
        }
        if (name == "Maintenance") {
            this.props.navigation.navigate("Maintenance");
        }
        if (name == "Book A Commonplot") {
            this.props.navigation.navigate("CommonplotDetails");
        }
        if (name == "Rule Book") {
            this.props.navigation.navigate("Rulebook");
        }
    }

    storeToken = async () => {
        try {
            let token = await AsyncStorage.getItem('@authToken_Key')
            console.log('token', token)
            this.props.societyMembersList(token)
        } catch (e) {
            console.log('Error in store authToken');
        }
    }

    componentWillMount() {
        // this.storeToken()
    }

    componentDidMount() {
        this.animate();
        this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => {
            BackHandler.exitApp();
            return true;
        });
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }

    componentWillReceiveProps(nextProps) {
        console.log('users', nextProps.users);
    }

    animate() {
        this.animatedValue1.setValue(0);

        const createAnimation = function (value, duration, easing, delay = 0) {
            return Animated.timing(value, {
                toValue: 1,
                duration,
                easing,
                delay
            });
        };
        Animated.parallel([
            createAnimation(this.animatedValue1, 800, Easing.ease)
        ]).start();
    }

    render() {
        const scaleText = this.animatedValue1.interpolate({
            inputRange: [0, 1],
            outputRange: [0.5, 1]
        });
        return (
            <Fragment>
                <SafeAreaView style={{flex: 0, backgroundColor: DARK_GRADIENT}}/>
                <SafeAreaView style={{flex: 1}}>
                    <View showsVerticalScrollIndicator={false} style={styles.container}>
                        <View style={{height: 50}}>
                            <Header
                                onRightPress={() =>
                                    this.props.navigation.navigate("Notification")
                                }
                                onBackPress={() =>
                                    this.props.navigation.toggleDrawer()
                                }
                                headerText={"Society Management"}
                                leftImage={require("../img/menu.png")}
                                rightImage={require("../img/notification-bell.png")}
                            />
                        </View>
                        <ScrollView showsVerticalScrollIndicator={false}>
                            {/* <View style={{ padding: 10, backgroundColor: "white" }}>
                <Animated.View style={{ transform: [{ scale: scaleText }] }}>
                  <View style={styles.noticeView}>
                    <View style={styles.headerNotice}>
                      <CustomTextView
                        style={{ color: "white", fontSize: 25 }}
                        text="Notice"
                      />
                    </View>
                    <CustomTextView
                      style={styles.noticeText}
                      text="A committee member of a Co-operative Housing Society is a public servant as defined under section 2(c)(ix) of the Prevention of Corruption Act, 1988."
                    />
                  </View>
                </Animated.View>
			  </View> */}
                            {/*<View*/}
                            {/*    style={{*/}
                            {/*        flex: 1*/}
                            {/*    }}*/}
                            {/*>*/}
                            {/*    <View*/}
                            {/*        style={{*/}
                            {/*            flex: 1,*/}
                            {/*            backgroundColor: "rgba(40,40,40,0.3)",*/}
                            {/*            padding: 10*/}
                            {/*        }}*/}
                            {/*    >*/}
                            {/*        <Animated.View style={{transform: [{scale: scaleText}]}}>*/}
                            {/*            <View*/}
                            {/*                style={{*/}
                            {/*                    flex: 1,*/}
                            {/*                    backgroundColor: "white",*/}
                            {/*                    shadowColor: DARK_GRADIENT,*/}
                            {/*                    borderTopLeftRadius: 25,*/}
                            {/*                    borderBottomRightRadius: 25,*/}
                            {/*                    shadowOffset: {height: 5, width: 0},*/}
                            {/*                    shadowOpacity: 0.5,*/}
                            {/*                    shadowRadius: 5*/}
                            {/*                }}*/}
                            {/*            >*/}
                            {/*                <View style={styles.noticeView}>*/}
                            {/*                    <View style={styles.headerNotice}>*/}
                            {/*                        <CustomTextView*/}
                            {/*                            style={{color: "white", fontSize: 25}}*/}
                            {/*                            text="Notice"*/}
                            {/*                        />*/}
                            {/*                    </View>*/}
                            {/*                    <CustomTextView*/}
                            {/*                        style={styles.noticeText}*/}
                            {/*                        text="A committee member of a Co-operative Housing Society is a public servant as defined under section 2(c)(ix) of the Prevention of Corruption Act, 1988."*/}
                            {/*                    />*/}
                            {/*                </View>*/}
                            {/*            </View>*/}
                            {/*        </Animated.View>*/}
                            {/*    </View>*/}
                            {/*</View>*/}
                            <FlatList
                                data={this.state.data}
                                showsVerticalScrollIndicator={false}
                                renderItem={({item, index}) => {
                                    console.log(item.title);
                                    return (
                                        <HomeItemLayout
                                            index={index}
                                            name={item.name}
                                            img={item.img}
                                            color={item.color}
                                            onPressItem={() => this.onPressItem(item.name)}
                                        />
                                    );
                                }}
                            />
                        </ScrollView>
                    </View>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    noticeView: {
        width: "100%",
        // padding:20,
        borderTopLeftRadius: 25,
        borderBottomRightRadius: 25,
        justifyContent: "center",
        borderWidth: 5,
        borderColor: "#1E86A7",
        elevation: 1
    },
    noticeText: {
        color: "#1A2980",
        fontSize: 20,
        textAlign: "center",
        padding: 8
    },
    headerNotice: {
        padding: 5,
        width: "100%",
        backgroundColor: "#1E86A7",
        justifyContent: "center",
        borderTopLeftRadius: 20,
        alignItems: "center"
    },
    cardView: {
        borderRadius: 10,
        width: "100%",
        height: 50,
        shadowColor: DARK_GRADIENT,
        shadowOffset: {height: 5, width: 0},
        shadowOpacity: 0.5,
        shadowRadius: 5
    }
});
const mapStateToProps = state => {
    return {
        authResult: state.Listing.authResult,
    };
};

export default connect(
    mapStateToProps,
    {}
)(Home);

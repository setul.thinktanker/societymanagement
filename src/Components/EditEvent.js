import React, { Component, Fragment } from "react";
import {
    View,
    StatusBar,
    FlatList,
    Animated,
    StyleSheet,
    Image,
    Linking,
    TouchableOpacity,
    SafeAreaView,
    ActivityIndicator
} from "react-native";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import ItemEditEvent from "./ItemEditEvent";
import LinearGradient from "react-native-linear-gradient";
import Header from "./Header";
import Swipeable from "react-native-gesture-handler/Swipeable";
import CustomTextView from "../Custom/CustomTextView";
import { RectButton } from "react-native-gesture-handler";
import AsyncStorage from '@react-native-community/async-storage';
import { getUser } from "../Database/allSchema";
import { eventList, deleteEvent } from "../Actions";
import { connect } from "react-redux";
import Toast, { DURATION } from "react-native-easy-toast";

userId = ''
societyId = ''
authToken = ''

class EditEvent extends Component {
    constructor (props) {
        super(props);
        this.state = {
            pageNo: 1,
            eventlist: [
                {
                    "id": 24,
                    "owner_name": "ketan",
                    "title": "Birthday",
                    "date": "10/2/2020,11/2/2020,10/2/2020,11/2/2020",
                    "created_at": "2020-03-11 06:25:10",
                    "updated_at": "2020-03-11 06:25:10",
                    "society_id": 1,
                    "user_id": 5
                },
                {
                    "id": 25,
                    "owner_name": "ketan",
                    "title": "Event",
                    "date": "10/2/2020,11/2/2020,10/2/2020,11/2/2020",
                    "created_at": "2020-03-11 06:25:46",
                    "updated_at": "2020-03-11 06:25:46",
                    "society_id": 1,
                    "user_id": 5
                }
            ]
        };
    }
    onBackPress() {
        this.props.navigation.goBack();
    }
    storeToken = () => {
        try {
            AsyncStorage.getItem('@authToken_Key')
                .then((token) => {
                    console.log('token', token)
                    authToken = token
                    getUser().then((user) => {
                        userId = user[0].id;
                        societyId = user[0].societyId
                        this.props.eventList({ userId, societyId, pageNo: this.state.pageNo, token })
                    }).catch((err) => {
                        console.log('err', err)
                    });
                })
                .catch((err) => {
                    console.log('err', err)
                })
        } catch (e) {
            console.log('Error in store authToken', e);
        }
    }
    componentWillMount() {
        this.storeToken()
    }
    onAddPress() {
        this.props.navigation.navigate('AddEvent')
    }
    UNSAFE_componentWillReceiveProps(nextProps) {
        console.log('nextProps', nextProps)

        this.setState({ events: nextProps.events })

        if (nextProps.authResult == 'Delete Success') {
            this.refs.toast.show(nextProps.msg, 2500);
        }
        if (nextProps.authResult == 'Delete Failed') {
            this.refs.toast.show(nextProps.msg, 2500);
        }
        if (nextProps.authResult == 'Delete Error') {
            this.refs.toast.show(nextProps.msg, 2500);
        }
    }
    deleteEvent(index, id) {
        console.log("index", index);
        console.log("id", id);
        console.log('authToken', authToken);
        // api call for remove vehicle
        if (id) {
            this.props.deleteEvent({ eventId: id, token: authToken, userId, societyId, pageNo: this.state.pageNo, list: this.state.events })
        }
        let tempArray = this.state.events;
        tempArray.splice(index, 1);
        console.warn("tempArray", tempArray);
        this.setState({ events: tempArray });

    }
    renderRightActionEdit = (text, color, x, progress, item) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0]
        });
        return (
            <Animated.View
                style={ [
                    styles.animateViewStyle,
                    { transform: [{ translateX: trans }] }
                ] }
            >
                <RectButton
                    style={ [styles.rightAction, { backgroundColor: color }] }
                    onPress={ () => {
                        this.props.navigation.navigate("AddEvent", { item: item });
                    } }
                >
                    <Image
                        style={ [styles.actionImg, { tintColor: "white" }] }
                        source={ require("../img/edit.png") }
                    />
                </RectButton>
            </Animated.View>
        );
    };
    renderRightActionToDelete = (color, x, progress, index, id) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0]
        });
        return (
            <Animated.View
                style={ [
                    styles.animateViewStyle,
                    { transform: [{ translateX: trans }] }
                ] }
            >
                <RectButton
                    style={ [styles.rightAction, { backgroundColor: color }] }
                    onPress={ () => this.deleteEvent(index, id) }
                >
                    <Image
                        style={ [styles.actionImg, { tintColor: "white" }] }
                        source={ require("../img/delete.png") }
                    />
                </RectButton>
            </Animated.View>
        );
    };

    handleLoadMore = () => {
        console.log('in handle more')
        this.setState(
            {
                pageNo: this.state.pageNo + 1
            },
            () => {
                this.props.eventList({ userId, societyId, pageNo: this.state.pageNo, token: authToken, list: this.props.events })
            }
        );
    };

    renderRightActions(progress, item, index) {
        return (
            <View style={ { width: 150, flexDirection: "row" } }>
                { this.renderRightActionEdit("Edit", "#2D6CC1", 75, progress, item) }
                { this.renderRightActionToDelete("#C13D2D", 75, progress, index, item.id) }
            </View>
        );
    }
    render() {
        return (
            <Fragment>
                <SafeAreaView style={ { flex: 0, backgroundColor: DARK_GRADIENT } } />
                <SafeAreaView style={ { flex: 1 } }>
                    <StatusBar backgroundColor={ DARK_GRADIENT } barStyle="light-content" />
                    <View style={ { height: 50 } }>
                        <Header
                            headerText={ "Events" }
                            leftImage={ require("../img/left-arrow.png") }
                            onBackPress={ () => this.onBackPress() }
                            rightImage={ require("../img/add.png") }
                            onRightPress={ () => this.onAddPress() }
                        />
                    </View>
                    <Toast
                        ref="toast"
                        style={ { backgroundColor: "#1A2980" } }
                        opacity={ 0.9 }
                    />
                    { (this.props.events.length > 0 && !this.props.isLoading) ?
                        <FlatList
                            // style = {{ padding:10 }}
                            contentContainerStyle={ { padding: 10, paddingBottom: 80 } }
                            data={ this.state.events }
                            showsVerticalScrollIndicator={ false }
                            showsVerticalScrollIndicator={ false }
                            onEndReached={ this.handleLoadMore }
                            onEndReachedThreshold={ 1 }
                            renderItem={ ({ item, index }) => {
                                return (
                                    <Swipeable
                                        ref={ this.updateRef }
                                        friction={ 2 }
                                        leftThreshold={ 30 }
                                        rightThreshold={ 40 }
                                        renderRightActions={ progress =>
                                            this.renderRightActions(progress, item, index)
                                        }
                                    >
                                        <ItemEditEvent item={ item } index={ index } />
                                    </Swipeable>
                                );
                            } }
                            ListFooterComponent={
                                (this.props.isData) ?
                                    <ActivityIndicator size="small" color={ 'white' } />
                                    :
                                    null
                            }
                        />
                        :
                        <View style={ { flex: 1, height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center' } }>
                            { (this.props.isLoading) ?
                                <ActivityIndicator
                                    size={ 70 }
                                    color={ LIGHT_GRADIENT }
                                // animating={this.props.isLoading}
                                />
                                :
                                <CustomTextView
                                    numberOfLines={ 1 }
                                    text={ 'No Events found.' }
                                    style={ { color: 'white', fontSize: 15 } }
                                />
                            }

                        </View>
                    }

                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    animateViewStyle: {
        flex: 1,
        marginBottom: 5,
        marginTop: 10
    },
    actionText: {
        color: "white",
        fontSize: 20,
        backgroundColor: "transparent",
        padding: 10
    },
    rightAction: {
        alignItems: "center",
        flex: 1,
        borderRadius: 8,
        marginLeft: 5,
        justifyContent: "center"
    },
    actionImg: {
        height: 30,
        width: 30
    },
    bottomBtn: {
        flex: 1,
        height: 60,
        width: 60,
        borderRadius: 60 / 2,
        position: "absolute",
        bottom: "2%",
        right: "4%",
        alignItems: "center",
        justifyContent: "center"
    },
    btn: {
        flex: 1,
        width: "100%",
        height: "100%",
        justifyContent: "center"
    }
});

const mapStateToProps = state => {
    return {
        events: state.Events.event,
        isLoading: state.Events.isListLoading,
        isData: state.Events.isData,
        msg: state.Events.msg,
        authResult: state.Events.authResult
    };
};

export default connect(
    mapStateToProps,
    {
        eventList,
        deleteEvent
    }
)(EditEvent);
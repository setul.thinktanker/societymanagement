
import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  Dimensions
} from 'react-native';
import CardView from 'react-native-cardview';

const { width } = Dimensions.get('window');

export default class GalleryItemLayout extends Component {

  render() {
    console.log(this.props.listSize);
    console.log(this.props.index);
    console.log((this.props.listSize - 2) === this.props.index);
    return (

      <View style={{ height: 110, width: width / 2 - 5, marginBottom: ((this.props.listSize - 1) === this.props.index || (this.props.listSize - 2) === this.props.index) ? 40 : 0 }}>
        <View
          style={{ margin: 5, elevation: 5 }}
        >
          <TouchableOpacity
            activeOpacity={0.5}
            // onPress={() => this.props.navigation.navigate('SubGallery')}
            onPress={this.props.onClickItem}
          >

            <Image style={styles.imageTile}
              source={this.props.imageName}
            />

            <View style={styles.marker}>
              <Text
                allowFontScaling={false}
                style={styles.TextStyle}>{this.props.title}</Text>
            </View>
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  marker: {
    height: '100%',
    width: '100%',
    borderRadius: 6,
    position: 'absolute',
    top: 0,
    left: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.4)',
  },
  imageTile: {
    height: '100%',
    width: '100%',
    borderRadius: 6
  },
  TextStyle: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    margin: 8,
    textAlign: 'center'
  },
});

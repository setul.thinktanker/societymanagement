import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Image,
  StatusBar,
  TouchableOpacity,
  Modal,
  TextInput,
  Platform,
  Dimensions
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import CustomTextView from "../Custom/CustomTextView";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";

export default class HeaderSearch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isModalVisible: false
    };
  }

  onPressSearch() {
    this.setState({ isModalVisible: true });
  }

  render() {
    return (
      <LinearGradient
        start={{ x: 0, y: 0.3 }}
        end={{ x: 1.5, y: 0 }}
        colors={[DARK_GRADIENT, LIGHT_GRADIENT]}
        style={{ flex: 1 }}
      >
        <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content" />
        {!this.state.isModalVisible ? (
          <View style={styles.container}>
            <TouchableOpacity
              onPress={this.props.onBackPress}
              style={styles.imgView}
            >
              <Image
                style={[styles.imageStyle, { marginLeft: 0, marginRight: 10 }]}
                source={this.props.leftImage}
              />
            </TouchableOpacity>

            <CustomTextView
              style={styles.textStyle}
              text={this.props.headerText}
            />

            <TouchableOpacity
              onPress={() => {
                this.setState({ isModalVisible: true });
              }}
              style={styles.imgView}
            >
              <Image style={styles.imageStyle} source={this.props.rightImage} />
            </TouchableOpacity>
          </View>
        ) : (
          <View style={[styles.container, {}]}>
            <View style={styles.seachView}>
              <View style={{ flexDirection: "row", flex: 1 }}>
                <Image
                  style={[styles.imageIconStyle, { marginRight: 5 }]}
                  source={require("../img/search.png")}
                />

                {/* <View style = {styles.line} /> */}
                <TextInput
                  allowFontScaling={false}
                  style={styles.textInput}
                  placeholder="Search here"
                  placeholderTextColor="white"
                  placeholderTextSize="14"
                  keyboardType="default"
                  // maxLength={30}
                  onChangeText={this.props.onChangeText}
                  // value={this.props.value}
                  // onChangeText={this.props.onChangeText}
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                />
              </View>
              <TouchableOpacity
                onPress={() => {
                  this.setState({ isModalVisible: false });
                }}
                style={styles.imageIconStyle}
              >
                <Image
                  style={styles.imageIconStyle}
                  source={require("../img/close.png")}
                />
              </TouchableOpacity>
              <TouchableOpacity
                activeOpacity={0.9}
                onPress={() => {
                  this.props.onPressClose();
                  this.setState({ isModalVisible: false });
                }}
                style={{
                  position: "absolute",
                  height: "100%",
                  width: "20%",
                  right: 0
                }}
              />
            </View>
          </View>
        )}
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    //   height:50,
    justifyContent: "space-between",
    flexDirection: "row",
    paddingHorizontal: 10,
    paddingVertical: 8,
    alignItems: "center"
  },
  imageStyle: {
    height: 25,
    width: 25,
    tintColor: "white",
    alignSelf: "center",
    marginLeft: 10
  },
  imgView: {
    height: "100%",
    justifyContent: "center",
    alignItems: "center"
  },
  startView: {
    flex: 1,
    width: "70%",
    alignSelf: "flex-start",
    alignItems: "center",
    flexDirection: "row"
  },
  textStyle: {
    fontSize: 20,
    color: "white",
    marginLeft: 10
  },
  imageIconStyle: {
    height: 18,
    width: 18,
    tintColor: "white",
    alignSelf: "center"
  },
  seachView: {
    backgroundColor: "rgba(255,255,255,0.4)",
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1,
    borderRadius: 8,
    padding: 10,
    alignItems: "center",
    alignSelf: "center"
  },
  line: {
    borderLeftWidth: 0.8,
    borderLeftColor: "white",
    marginLeft: 4
  },
  textInput: {
    fontSize: 14,
    color: "white",
    width: "85%",
    marginLeft: Platform.OS === "ios" ? 8 : 20,
    bottom: Platform.OS === "ios" ? null : -15,
    position: Platform.OS === "ios" ? null : "absolute"
  }
});

import React, { Component, Fragment } from "react";
import { View, StatusBar, SafeAreaView, FlatList } from "react-native";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import MaintenanceItemLayout from "./MaintenanceItemLayout";
import LinearGradient from "react-native-linear-gradient";
import Header from "./Header";

export default class Maintenance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      maintenanceData: [
        {
          rupees: "2000",
          Subject: "Monthly Maintanance",
          startDate: "01-07-2019",
          endDate: "01-08-2019"
        },
        {
          rupees: "1000",
          Subject: "Renovate Temple",
          startDate: "01-08-2019",
          endDate: "01-08-2019"
        }
      ]
    };
  }
  onBackPress() {
    this.props.navigation.goBack();
  }
  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: DARK_GRADIENT }} />
        <SafeAreaView style={{ flex: 1 }}>
          <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content" />
          <View style={{ height: 50 }}>
            <Header
              headerText={"Maintenance"}
              leftImage={require("../img/left-arrow.png")}
              onBackPress={() => this.onBackPress()}
            />
          </View>
          <FlatList
            // style = {{ padding:10 }}
            contentContainerStyle={{ padding: 10 }}
            data={this.state.maintenanceData}
            showsVerticalScrollIndicator={false}
            renderItem={({ item, index }) => {
              return <MaintenanceItemLayout item={item} index={index} />;
            }}
          />
        </SafeAreaView>
      </Fragment>
    );
  }
}

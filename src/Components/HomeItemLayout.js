/* @flow */

import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  Animated,
  TouchableOpacity
} from 'react-native';
import CustomTextView from '../Custom/CustomTextView';

export default class HomeItemLayout extends Component {
  constructor(props) {
        super(props);

        this.state = {
            scaleValue: new Animated.Value(0)
        }
    }
    componentDidMount() {
          Animated.timing(this.state.scaleValue, {
              toValue: 1,
              duration : 600,
              delay: this.props.index * 350
          }).start();
      }

  render() {
    return (
      <TouchableOpacity
      onPress = {this.props.onPressItem}
      activeOpacity = {0.7}
      >
      <Animated.View style={[styles.container,{backgroundColor:this.props.color,opacity: this.state.scaleValue}]}>
        <Image
            style={styles.imageStyle}
            source={this.props.img}
        />
        <CustomTextView style = {styles.textStyle} text={this.props.name} />
      </Animated.View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    height:100,
    width:'100%',
    flexDirection:'row',
    alignItems:'center',
    paddingLeft:20,
    // backgroundColor:'red'
  },
  textStyle:{
    fontSize:25,
    color:'white'
  },
  imageStyle:{
    height:40,
    width:40,
    marginRight:14,
    tintColor:'white'
  }
});

import React, { Component } from "react";
import { View, TouchableOpacity, Image, Button, Text } from "react-native";
import Video from "react-native-video";
import VideoPlayer from "react-native-video-player";

const VIMEO_ID = "179859217";
export default class PlayVideo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      //   rate: 1,
      //   volume: 1,
      //   muted: false,
      //   resizeMode: "contain",
      //   duration: 0.0,
      //   currentTime: 0.0,
      //   paused: true
      video: { width: undefined, height: undefined, duration: undefined },
      thumbnailUrl: undefined,
      videoUrl: undefined
    };
  }
  componentWillMount() {
    let uri = this.props.navigation.getParam("uri");
    this.setState({ uri: uri });
    console.warn(uri);
  }
  componentDidMount() {
    // global
    //   .fetch(`https://player.vimeo.com/video/${VIMEO_ID}/config`)
    //   .then(res => res.json())
    //   .then(res =>
    //     this.setState({
    //       thumbnailUrl: res.video.thumbs["640"],
    //       videoUrl:
    //         res.request.files.hls.cdns[res.request.files.hls.default_cdn].url,
    //       video: res.video
    //     })
    //   );
    let uri = this.props.navigation.getParam("uri");
    this.setState({ uri: uri });
    console.warn(uri);
  }
  onLoad = data => {
    this.setState({ duration: data.duration });
  };
  onProgress = data => {
    this.setState({ currentTime: data.currentTime });
  };
  onEnd = () => {
    this.setState({ paused: true });
    // this.video.seek(0);
  };
  getCurrentTimePercentage() {
    if (this.state.currentTime > 0) {
      return (
        parseFloat(this.state.currentTime) / parseFloat(this.state.duration)
      );
    }
    return 0;
  }
  render() {
    return (
      <View style={{ flex: 1, height: "100%" }}>
        <VideoPlayer
          style={{ alignItems: "center", height: "100%", width: "100%" }}
          endWithThumbnail
          disableFullscreen
          autoplay
          thumbnail={{ uri: this.state.thumbnailUrl }}
          video={{ uri: this.state.uri }}
          videoWidth={this.state.video.width}
          videoHeight={this.state.video.height}
          duration={
            this.state.video
              .duration /* I'm using a hls stream here, react-native-video
            can't figure out the length, so I pass it here from the vimeo config */
          }
          ref={r => (this.player = r)}
        />
      </View>
    );
  }
}
const styles = {
  container: {
    flex: 1,
    // justifyContent: "center",
    alignItems: "center"
    // backgroundColor: "#000"
  },
  fullScreen: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },
  controls: {
    flexDirection: "row",
    backgroundColor: "transparent",
    borderRadius: 5,
    position: "absolute",
    bottom: 20,
    left: 20,
    right: 20,
    alignItems: "center"
  },
  progress: {
    flex: 1,
    flexDirection: "row",
    borderRadius: 3,
    overflow: "hidden",
    marginLeft: 10
  },
  innerProgressCompleted: {
    height: 10,
    backgroundColor: "#f1a91b"
  },
  innerProgressRemaining: {
    height: 10,
    backgroundColor: "#2C2C2C"
  }
};

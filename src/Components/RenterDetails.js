import React, { Component, Fragment } from "react";
import {
  View,
  TextInput,
  StyleSheet,
  ScrollView,
  Image,
  Picker,
  Modal,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Platform,
  DatePickerIOS,
  DatePickerAndroid
} from "react-native";
import { connect } from "react-redux";
import LinearGradient from "react-native-linear-gradient";
import CustomTextView from "../Custom/CustomTextView";
import CustomDropDown from "../Custom/CustomDropDown";
import CustomPasswordInputView from "../Custom/CustomPasswordInputView";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import Toast, { DURATION } from "react-native-easy-toast";
import moment from "moment";
import { reanterDetailsInitialState, reanterDetails } from "../Actions";

class RenterDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      gender: [{ name: "Female" }, { name: "Male" }],
      date: new Date(),
      showDatePicker: false
    };
  }
  onBackPress() {
    // console.warn("onback press");
    this.props.navigation.goBack();
  }
  componentWillMount() {
    this.props.reanterDetailsInitialState();
  }
  componentWillReceiveProps(nextProp) {
    console.warn(nextProp.authResult);
    if (nextProp.authResult == "RenterDetails submit Success") {
      this.props.navigation.replace("Home");
    }
  }
  showPicker = async (stateKey, options) => {
    try {
      var newState = {};
      const { action, year, month, day } = await DatePickerAndroid.open(
        options
      );
      if (action === DatePickerAndroid.dismissedAction) {
        // newState[stateKey + "Text"] = "dismissed";
      } else {
        var date = new Date(year, month, day);
        newState[stateKey + "Text"] = date.toLocaleDateString();
        newState[stateKey + "Date"] = date;
      }
      this.setState({ SelectedDate: newState.maxDate });
      // console.warn("newState", newState.maxDate);
      // console.warn('date',newState);

      this.setState(newState);
    } catch ({ code, message }) {
      console.warn(`Error in example '${stateKey}': `, message);
    }
  };

  onPressSubmit = () => {
    const name = /^[a-zA-Z '.]{3,30}$/;
    const number = /^[0-9\b]{10,10}$/;
    const emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let isValid = true;
    const {
      fname,
      lname,
      mobileNo,
      email,
      SelectGender,
      SelectedDate,
      password,
      cPassword
    } = this.state;

    //check validaiton - first name
    if (!fname) {
      isValid = false;
      this.refs.toast.show("Please enter first name", 1500);
    } else if (name.test(fname) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid first name", 1500);
    }
    //check validation - last name

    if (!lname && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter last name", 1500);
    } else if (name.test(lname) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid last name", 1500);
    }
    //check validation - mobile number
    if (!mobileNo && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter mobile number", 2000);
    } else if (number.test(mobileNo) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid mobile number", 2000);
    }

    //email
    if (email) {
      if (emailReg.test(email) === false && isValid) {
        isValid = false;
        this.refs.toast.show("Please enter valid e-mail", 2000);
      }
    }

    //check validation - gender
    if (!SelectGender && isValid) {
      isValid = false;
      this.refs.toast.show("Please select gender", 2000);
    }

    //check validation - date
    if (!SelectedDate && isValid) {
      isValid = false;
      this.refs.toast.show("Please select date", 2000);
    }

    //check validation - password
    if (!password && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter password", 2000);
    }

    //check confirm password
    if (!cPassword && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter confirm password", 2000);
    } else if (cPassword !== password) {
      isValid = false;
      this.refs.toast.show(
        "Please enter confirm password same as password",
        2000
      );
    }

    if (isValid) {
      console.warn("is Valid");
      // this.props.navigation.navigate('Home');
      this.props.reanterDetails({
        fname,
        lname,
        mobileNo,
        email,
        SelectGender,
        SelectedDate,
        password
      });
    } else {
      console.warn("is not valid");
    }
  };

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: DARK_GRADIENT }} />
        <SafeAreaView style={{ flex: 1, backgroundColor: LIGHT_GRADIENT }}>
          <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content" />

          <LinearGradient
            colors={[DARK_GRADIENT, LIGHT_GRADIENT]}
            style={styles.container}
          >
            <TouchableOpacity
              onPress={() => this.onBackPress()}
              style={styles.imgView}
            >
              <Image
                style={[styles.imageStyle2]}
                source={require("../img/left-arrow.png")}
              />
            </TouchableOpacity>
            <ScrollView
              style={{ flex: 1, height: "100%", width: "100%" }}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={{
                paddingBottom: 200,
                paddingTop: 30,
                paddingHorizontal: 20
              }}
            >
              <View style={styles.firstView}>
                <Image
                  style={styles.imageStyle}
                  source={require("../img/cityscape.png")}
                />
                <CustomTextView style={styles.text} text="Society Management" />
                <View style={styles.line} />
              </View>

              <CustomTextView
                style={{
                  width: "100%",
                  textAlign: "center",
                  fontSize: 30,
                  color: "white",
                  marginVertical: 10
                }}
                text="Renter Details"
              />

              <View style={styles.inputView}>
                <Image
                  style={styles.imageIconStyle}
                  source={require("../img/user.png")}
                />
                <TextInput
                  allowFontScaling={false}
                  style={styles.textInput}
                  placeholder="Enter First Name"
                  placeholderTextColor="white"
                  placeholderTextSize="15"
                  keyboardType="default"
                  maxLength={30}
                  onChangeText={text => this.setState({ fname: text })}
                  // value={this.props.value}
                  // onChangeText={this.props.onChangeText}
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                />
              </View>
              <View style={styles.inputView}>
                <Image
                  style={styles.imageIconStyle}
                  source={require("../img/user.png")}
                />
                <TextInput
                  allowFontScaling={false}
                  style={styles.textInput}
                  placeholder="Enter Last Name"
                  placeholderTextColor="white"
                  placeholderTextSize="15"
                  keyboardType="default"
                  maxLength={30}
                  onChangeText={text => this.setState({ lname: text })}
                  // value={this.props.value}
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                />
              </View>
              <View style={styles.inputView}>
                <Image
                  style={styles.imageIconStyle}
                  source={require("../img/smartphone.png")}
                />
                <TextInput
                  allowFontScaling={false}
                  style={styles.textInput}
                  placeholder="Enter Mobile Number"
                  placeholderTextColor="white"
                  placeholderTextSize="15"
                  keyboardType="number-pad"
                  maxLength={10}
                  onChangeText={text => this.setState({ mobileNo: text })}
                  // value={this.props.value}
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                />
              </View>
              <View style={styles.inputView}>
                <Image
                  style={styles.imageIconStyle}
                  source={require("../img/envelope.png")}
                />
                <TextInput
                  allowFontScaling={false}
                  style={styles.textInput}
                  placeholder="Enter E-mail"
                  placeholderTextColor="white"
                  placeholderTextSize="15"
                  keyboardType="email-address"
                  maxLength={30}
                  onChangeText={text => this.setState({ email: text })}
                  // value={this.props.value}
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                />
              </View>

              <CustomDropDown
                image={require("../img/gender.png")}
                item={this.state.gender}
                prompt="Select Gender"
                onValueChange={(itemValue, itemIndex) => {
                  if (itemValue != 0) {
                    this.setState({
                      SelectGender: this.state.gender[itemIndex - 1].name
                    });
                  }
                }}
                selectedValue={this.state.SelectGender || "Select Gender"}
                text={this.state.SelectGender}
              />
              {Platform.OS == "ios" ? (
                <TouchableOpacity
                  onPress={() => this.setState({ showDatePicker: true })}
                  style={styles.inputView}
                >
                  <Image
                    style={styles.imageIconStyle}
                    source={require("../img/calendar.png")}
                  />
                  <CustomTextView
                    style={styles.textInput}
                    text={
                      this.state.SelectedDate
                        ? moment(this.state.SelectedDate).format("DD-MM-YYYY")
                        : "Select Tenancy Date"
                    }
                  />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={this.showPicker.bind(this, "max", {
                    mode: "spinner",
                    maxDate: new Date()
                  })}
                  style={[
                    styles.inputView,
                    {
                      paddingVertical: Platform.OS === "ios" ? 10 : 14
                    }
                  ]}
                >
                  <Image
                    style={styles.imageIconStyle}
                    source={require("../img/calendar.png")}
                  />
                  <CustomTextView
                    style={styles.textInput}
                    text={
                      this.state.SelectedDate
                        ? moment(this.state.SelectedDate).format("DD-MM-YYYY")
                        : "Select Tenancy Date"
                    }
                  />
                </TouchableOpacity>
              )}

              <View style={styles.inputView}>
                <Image
                  style={styles.imageIconStyle}
                  source={require("../img/lock.png")}
                />
                <CustomPasswordInputView
                  placeholder={"Enter Password"}
                  onChangeText={text => this.setState({ password: text })}
                />
              </View>

              <View style={styles.inputView}>
                <Image
                  style={styles.imageIconStyle}
                  source={require("../img/lock.png")}
                />
                <CustomPasswordInputView
                  placeholder={"Enter Confirm Password"}
                  onChangeText={text => this.setState({ cPassword: text })}
                />
              </View>

              <TouchableOpacity
                activeOpacity={0.9}
                onPress={this.onPressSubmit}
                style={styles.btn}
              >
                {this.props.isLoading ? (
                  <Image
                    style={{
                      height: 40,
                      width: 40,
                      backgroundColor: "transparent"
                    }}
                    source={require("../img/loader2.gif")}
                  />
                ) : (
                  <CustomTextView style={styles.textStyle} text="Submit" />
                )}
              </TouchableOpacity>

              <Toast
                ref="toast"
                style={{ backgroundColor: "#1A2980" }}
                opacity={0.9}
              />
              {this.state.showDatePicker ? (
                <Modal
                  visible={this.state.showPicker}
                  transparent
                  animationType={"slide"}
                  supportedOrientations={["portrait", "landscape"]}
                >
                  <TouchableOpacity
                    style={{ flex: 1 }}
                    onPress={() => {
                      this.setState({ showDatePicker: false });
                    }}
                  />
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({
                        showDatePicker: false
                      });
                    }}
                    hitSlop={styles.hitSlopStyle}
                  >
                    <View>
                      <CustomTextView text="Done" style={styles.pickerText} />
                    </View>
                  </TouchableOpacity>
                  <View style={{ backgroundColor: "white" }}>
                    <DatePickerIOS
                      date={this.state.date}
                      mode="date"
                      onDateChange={newDate => {
                        this.setState({ SelectedDate: newDate, date: newDate });
                      }}
                    />
                  </View>
                </Modal>
              ) : null}
            </ScrollView>
          </LinearGradient>
        </SafeAreaView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  firstView: {
    justifyContent: "center",
    alignItems: "center"
  },
  imageStyle: {
    tintColor: "white",
    height: 90,
    width: 90,
    alignSelf: "center"
  },
  text: {
    color: "white",
    fontSize: 10
  },
  line: {
    height: 3,
    width: 90,
    borderRadius: 63,
    backgroundColor: "white",
    marginTop: 1
  },
  inputView: {
    width: "100%",
    borderRadius: 30,
    backgroundColor: "rgba(255,255,255,0.3)",
    marginTop: 15,
    paddingHorizontal: 5,
    flexDirection: "row",
    paddingVertical: Platform.OS === "ios" ? 10 : 0
  },
  inputViewDropDown: {
    width: "100%",
    borderRadius: 30,
    backgroundColor: "rgba(255,255,255,0.3)",
    marginTop: 15,
    paddingHorizontal: 5,
    flexDirection: "row",
    paddingVertical: Platform.OS === "ios" ? 10 : 14
  },
  inputTwoDropDown: {
    width: "100%",
    marginTop: 15,
    flexDirection: "row"
  },
  imageIconStyle: {
    height: 20,
    width: 20,
    tintColor: "white",
    alignSelf: "center",
    marginLeft: 8
  },
  textInput: {
    fontSize: 14,
    color: "white",
    width: "100%",
    marginLeft: 8
  },
  textStyle: {
    fontSize: 20,
    alignSelf: "center",
    color: "white"
  },
  btn: {
    height: 40,
    width: 100,
    borderRadius: 30,
    marginTop: 20,
    alignSelf: "flex-end",
    alignItems: "center",
    backgroundColor: "#1A2980",
    justifyContent: "center"
  },
  textStyle2: {
    fontSize: 18,
    color: "#1A2980",
    alignSelf: "center"
  },
  linkStyle: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    height: 40
  },
  arrowIconStyle: {
    height: 18,
    width: 18,
    tintColor: "white",
    alignSelf: "center",
    marginLeft: 8
  },
  lineView: {
    borderRightWidth: 1.5,
    borderColor: "white",
    borderRadius: 20,
    height: "80%",
    alignSelf: "center",
    marginLeft: 6
  },
  modalStyle: {
    flex: 1,
    backgroundColor: "rgba(40,40,40,0.8)",
    alignItems: "center",
    justifyContent: "center"
  },
  modalView: {
    height: "20%",
    width: "90%",
    borderRadius: 8,
    backgroundColor: "white",
    justifyContent: "center",
    overflow: "scroll"
  },
  modalText: {
    color: "white",
    fontSize: 20,
    // textAlign:'left',
    marginLeft: 20
  },
  modalBtn: {
    // backgroundColor:'rgba(255,255,255,0.2)',
    borderRadius: 30,
    width: "49%",
    alignItems: "center",
    justifyContent: "center"
  },
  modalRound: {
    height: 75,
    width: 75,
    borderRadius: 75 / 2,
    backgroundColor: DARK_GRADIENT,
    elevation: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  pickerText: {
    color: "#007AFE",
    fontWeight: "bold",
    fontSize: 16,
    padding: 5,
    textAlign: "right",
    backgroundColor: "white"
  },
  hitSlopStyle: {
    top: 4,
    right: 4,
    bottom: 4,
    left: 4
  },
  pickerViewStyle: {
    height: 215,
    justifyContent: "center",
    backgroundColor: "#D0D4DB"
  },
  pickerStyle: {
    backgroundColor: "white"
  },
  imageStyle2: {
    height: 25,
    width: 25,
    tintColor: "white",
    alignSelf: "center",
    marginLeft: 10
  },
  imgView: {
    marginTop: 10,
    marginLeft: 5,
    alignSelf: "flex-start",
    justifyContent: "center",
    alignItems: "center"
  }
});

const mapStateToProps = state => {
  return {
    fname: state.RenterDetails.fname,
    lname: state.RenterDetails.lname,
    mobileNo: state.RenterDetails.mobileNo,
    password: state.RenterDetails.password,
    gender: state.RenterDetails.gender,
    date: state.RenterDetails.date,
    isLoading: state.RenterDetails.isLoading,
    authResult: state.RenterDetails.authResult
  };
};

export default connect(
  mapStateToProps,
  {
    reanterDetailsInitialState,
    reanterDetails
  }
)(RenterDetails);

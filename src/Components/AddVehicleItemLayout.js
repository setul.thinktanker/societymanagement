import React, { Component } from "react";
import { View, StyleSheet, Image, TextInput } from "react-native";
import CustomTextView from "../Custom/CustomTextView";
import CustomDropDown from "../Custom/CustomDropDown";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";
import { TouchableOpacity } from "react-native-gesture-handler";
import Toast, { DURATION } from "react-native-easy-toast";

export default class AddVehicleItemLayout extends Component {
  constructor (props) {
    super(props);
    this.state = {
      vehicleType: [{ name: "Car" }, { name: "Bike" }, { name: "Cycle" }]
    };
  }

  componentWillReceiveProps(nextProp) {
    this.setState({
      vehicleNo: nextProp.item.vehicle_number,
      userName: nextProp.item.name,
      SelectVehicleType: nextProp.item.vehicle_type,
      MobileNo: nextProp.item.mobile_no
    });
    // console.warn('item.this.props.item', SelectVehicleType)

  }
  componentWillMount() {
    this.setState({
      vehicleNo: this.props.item.vehicle_number,
      userName: this.props.item.name,
      SelectVehicleType: this.props.item.vehicle_type,
      MobileNo: this.props.item.mobile_no
    });
  }
  // onPressSave(){

  //   let vehicleDetails = {};

  //   vehicleDetails.vehicleNo = this.state.vehicleNo;
  //   vehicleDetails.vehicleType = this.state.SelectVehicleType;
  //   console.warn(vehicleDetails)
  //   this.props.onPressSave(vehicleDetails);
  // }

  render() {
    return (
      <View style={ styles.container }>
        {/* <View style={styles.inputView}>
          <Image
              style={styles.imageIconStyle}
              source={require('../img/user.png')}
          />
          <TextInput
            allowFontScaling={false}
            style={styles.textInput}
            placeholder="Enter User Name"
            placeholderTextColor = "white"
            placeholderTextSize="15"
            keyboardType="default"
            maxLength={30}
            onChangeText={this.props.onChangeUserName}
            value={this.state.userName}
            // onChangeText={this.props.onChangeText}
            // returnKeyType="search"
            // onSubmitEditing={this.props.onSubmitEditing}
           />
        </View> */}
        <View style={ [styles.inputView] }>
          <Image
            style={ styles.imageIconStyle }
            source={ require("../img/license-plate.png") }
          />
          <TextInput
            allowFontScaling={ false }
            style={ styles.textInput }
            placeholder="Enter Vehicle Number"
            placeholderTextColor="white"
            placeholderTextSize="15"
            keyboardType="default"
            maxLength={ 30 }
            onChangeText={ this.props.onChangeVehicleData }
            value={ this.state.vehicleNo }
          // onChangeText={this.props.onChangeText}
          // returnKeyType="search"
          // onSubmitEditing={this.props.onSubmitEditing}
          />
        </View>
        {/* <View style={[styles.inputView, { marginTop: 10 }]}>
          <Image
            style={styles.imageIconStyle}
            source={require("../img/smartphone.png")}
          />
          <TextInput
            allowFontScaling={false}
            style={styles.textInput}
            placeholder="Enter Mobile Number"
            placeholderTextColor="white"
            placeholderTextSize="15"
            keyboardType="number-pad"
            maxLength={10}
            // onChangeText={(text) => this.setState({MobileNo :text})}
            onChangeText={this.props.onChangeMobileNo}
            value={this.state.MobileNo}
            // onChangeText={this.props.onChangeText}
            // returnKeyType="search"
            // onSubmitEditing={this.props.onSubmitEditing}
          />
        </View> */}
        <CustomDropDown
          style={ { backgroundColor: "rgba(27, 66, 139, 1)", marginTop: 10 } }
          image={ require("../img/vehicle.png") }
          item={ this.state.vehicleType }
          prompt="Select Vehicle Type"
          onValueChange={ (itemValue, itemIndex) =>
            this.props.onChangeVehicleType(
              itemValue,
              itemIndex,
              this.state.vehicleType
            )
          }
          selectedValue={ this.props.onSelectVehicleType }
          text={ this.props.text }
        />
        {/* <View style={styles.bottomBtnView}>
            <LinearGradient
            start={{x: 1.5, y: 0}}
            end={{x: 0, y: 0.3}}
            colors={[DARK_GRADIENT,LIGHT_GRADIENT]}
            style = {styles.saveBtn}>
              <TouchableOpacity style = {styles.btnStyle}>
                <CustomTextView style={[styles.textInput,{marginLeft:0}]} text='Edit' />
              </TouchableOpacity>
            </LinearGradient>
       
            <LinearGradient
            start={{x: 1.5, y: 0}}
            end={{x: 0, y: 0.3}}
            colors={[DARK_GRADIENT,LIGHT_GRADIENT]}
            style = {styles.saveBtn}>
              <TouchableOpacity 
              onPress = {()=>{this.onPressSave()}}
              style = {styles.btnStyle}> 
                <CustomTextView style={[styles.textInput,{marginLeft:0}]} text='Save' />
              </TouchableOpacity>
            </LinearGradient>
        </View> */}
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "98%",
    alignSelf: "center",
    backgroundColor: "rgba(30, 118, 161, 0.4)",
    padding: 8,
    marginBottom: 10,
    borderRadius: 8
    // shadowRadius:8,
    // shadowColor:DARK_GRADIENT,
    // shadowOffset:{height:-3,width:0},
    // shadowOpacity:0.2,
  },
  inputView: {
    width: "100%",
    borderRadius: 30,
    backgroundColor: "rgba(27, 66, 139,1)",
    paddingHorizontal: 5,
    flexDirection: "row",
    paddingVertical: Platform.OS === "ios" ? 10 : 0
  },
  imageIconStyle: {
    height: 20,
    width: 20,
    tintColor: "white",
    alignSelf: "center",
    marginLeft: 8
  },
  textInput: {
    fontSize: 14,
    color: "white",
    width: "100%",
    marginLeft: 8
  },
  saveBtn: {
    width: "45%",
    borderRadius: 30,
    alignSelf: "flex-end",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: LIGHT_GRADIENT
  },
  bottomBtnView: {
    flex: 1,
    // justifyContent:'space-between',
    // flexDirection:'row',
    marginTop: 10
  },
  btnStyle: {
    flex: 1,
    padding: 10,
    borderRadius: 30
  }
});

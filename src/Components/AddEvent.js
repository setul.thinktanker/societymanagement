import React, { Component, Fragment } from "react";
import {
    View,
    StatusBar,
    StyleSheet,
    TextInput,
    ScrollView,
    TouchableOpacity,
    SafeAreaView,
    TimePickerAndroid
} from "react-native";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import CustomTextView from "../Custom/CustomTextView";
import Toast, { DURATION } from "react-native-easy-toast";
import { Calendar } from 'react-native-calendars';
import Header from "./Header";
import moment from 'moment';
import { initialEventData, addEvent, editEvent } from "../Actions";
import { connect } from "react-redux";
import { getUser } from "../Database/allSchema";
import AsyncStorage from '@react-native-community/async-storage';

let authToken = '';
let userId = '';
let societyId = '';



class AddEvent extends Component {
    constructor (props) {
        super(props);
        this.state = {
            selectedDate: moment(new Date()).format("YYYY-MM-DD"),
            markDates: {},
            isAlreadyAdded: false,

        };
    }
    onBackPress() {
        this.props.navigation.goBack();
    }
    UNSAFE_componentWillReceiveProps(nextProps) {
        if (nextProps.authResult == 'Add Success') {
            this.refs.toast.show(nextProps.msg, 2000);
            setTimeout(() => {
                // this.props.navigation.navigate('Event')
                // this.props.navigation.navigate.goBack()
            }, 2000);
        }
        if (nextProps.authResult == 'Add Failed') {
            this.refs.toast.show(nextProps.msg, 2000);
        }
        if (nextProps.authResult == 'Add Error') {
            this.refs.toast.show('Somthing went wrong please try again later.', 2000);
            this.setState({ markDates: {}, event: '', description: '' })
        }
        if (nextProps.authResult == 'edit Success') {
            this.refs.toast.show(nextProps.msg, 2000);
            setTimeout(() => {
                // this.props.navigation.navigate.goBack()
            }, 2000);
        }
        if (nextProps.authResult == 'edit Failed') {
            this.refs.toast.show(nextProps.msg, 2000);
        }
        if (nextProps.authResult == 'edit Error') {
            this.refs.toast.show('Somthing went wrong please try again later.', 2000);
            this.setState({ markDates: {}, event: '', description: '' })
        }
    }
    UNSAFE_componentWillMount() {
        // console.warn("item", this.props.navigation.getParam("item"));
        let item = this.props.navigation.getParam("item");

        if (item) {
            //define object to set previous object value (first bank then ,set of previous values)
            let newObj = {}
            //selected dates
            let dates = item.date
            for (let index = 0; index < dates.length; index++) {
                let key = dates[index];
                let obj = {}
                obj[key] = { selected: true, dateSelected: true, selectedColor: "#1A2980" }
                let tempObj = Object.assign(obj, newObj);
                newObj = tempObj
                this.setState({ markDates: tempObj }, console.log('markDates', this.state.markDates))
            }


            this.setState({
                event_Id: item.id,
                event: item.title,
                description: item.description,
                isAlreadyAdded: true,
            });
        }
    }
    storeToken = () => {
        try {
            AsyncStorage.getItem('@authToken_Key')
                .then((token) => {
                    console.log('token', token)
                    authToken = token
                    getUser().then((user) => {
                        userId = user[0].id;
                        societyId = user[0].societyId
                    }).catch((err) => {
                        console.log('err', err)
                    });
                })
                .catch((err) => {
                    console.log('err', err)
                })
        } catch (e) {
            console.log('Error in store authToken');
        }
    }
    componentWillMount() {
        this.storeToken()
    }
    onDaySelected(day) {
        let isMatched = true
        let dateVar = { ...this.state.markDates }
        let x = "";
        // traverses json object key-wise.
        for (x in dateVar) {
            if (day.dateString === x) {
                if (this.state.selectedDate === x) {

                    if (dateVar[x].dateSelected === true) {
                        dateVar[day.dateString] = {
                            ...dateVar[day.dateString],
                            selected: true,
                            dateSelected: false,
                            selectedColor: DARK_GRADIENT,
                            eventAddTime: day.timestamp
                        }

                        this.setState({ markDates: dateVar }, () => {
                            console.log('this.state.markDates', this.state.markDates)
                        })
                        isMatched = false
                        break;
                    } else {
                        dateVar[day.dateString] = {
                            ...dateVar[day.dateString],
                            selected: true,
                            dateSelected: true,
                            selectedColor: DARK_GRADIENT,
                            eventAddTime: day.timestamp
                        }
                        this.setState({ markDates: dateVar }, () => {
                            console.log(JSON.stringify(this.state.markDates))
                        })
                        isMatched = false
                        break;
                    }
                } else {
                    delete dateVar[x]
                    this.setState({ markDates: dateVar }, () => {
                        console.log('this.state.markDates', this.state.markDates)
                    })
                    isMatched = false
                    break;
                }
            }
        }

        //If date is not present 
        if (isMatched) {
            console.log("isMatched");
            dateVar[day.dateString] = {
                ...dateVar[day.dateString],
                selected: true,
                dateSelected: true,
                selectedColor: DARK_GRADIENT,
                eventAddTime: day.timestamp
            }
            this.setState({ markDates: dateVar }, () => {
                console.log('markDates', this.state.markDates)
            })
        }
    }


    onpressSend = () => {
        let dates = this.state.markDates
        let eventDates = []
        for (let [key, value] of Object.entries(dates)) {
            console.warn(`${key}: ${value}`);
            eventDates.push(key)
        }
        console.warn('eventDates', eventDates)
        const { event, description, markDates, event_Id } = this.state;
        isValid = true;
        if (!event) {
            isValid = false;
            this.refs.toast.show("Please enter event name", 2000);
        } else if (!description) {
            isValid = false;
            this.refs.toast.show("Please enter event description", 2000);
        } else if (!eventDates.length > 0) {
            isValid = false;
            this.refs.toast.show("Please select event date", 2000);
        }

        if (isValid) {
            //api call

            {
                (!this.state.isAlreadyAdded) ?
                    this.props.addEvent({
                        eventData: eventDates,
                        eventName: event,
                        description,
                        societyId,
                        userId,
                        token: authToken
                    })
                    :
                    this.props.editEvent({
                        eventId: event_Id,
                        eventData: eventDates,
                        eventName: event,
                        description,
                        societyId,
                        userId,
                        token: authToken
                    })
            }
        }
    };
    render() {
        return (
            <Fragment>
                <SafeAreaView style={ { flex: 0, backgroundColor: DARK_GRADIENT } } />
                <SafeAreaView style={ { flex: 1 } }>
                    <StatusBar backgroundColor={ DARK_GRADIENT } barStyle="light-content" />
                    <View style={ { height: 50 } }>
                        <Header
                            headerText={ (this.state.isAlreadyAdded) ? "Edit Event" : "Add Event" }
                            leftImage={ require("../img/left-arrow.png") }
                            onBackPress={ () => this.onBackPress() }
                        />
                    </View>
                    <ScrollView showsVerticalScrollIndicator={ false } style={ { flex: 1 } }>
                        <View style={ styles.container }>
                            <View style={ styles.card }>
                                <View style={ styles.textInputView }>
                                    <TextInput
                                        allowFontScaling={ false }
                                        style={ [styles.textInput, styles.textInput2] }
                                        placeholder="Enter Event"
                                        placeholderTextColor={ DARK_GRADIENT }
                                        placeholderTextSize="18"
                                        keyboardType="default"
                                        onChangeText={ text => this.setState({ event: text }) }
                                        value={ this.state.event }
                                    // onChangeText={this.props.onChangeText}
                                    // returnKeyType="search"
                                    // onSubmitEditing={this.props.onSubmitEditing}
                                    />
                                </View>
                                <View style={ styles.textInputView }>
                                    <TextInput
                                        allowFontScaling={ false }
                                        style={ [
                                            styles.textInput,
                                            styles.textInput2,
                                            {
                                                marginTop: 0
                                            }
                                        ] }
                                        placeholder="Enter Description"
                                        placeholderTextColor={ DARK_GRADIENT }
                                        placeholderTextSize="18"
                                        keyboardType="default"
                                        multiline={ true }
                                        onChangeText={ text => this.setState({ description: text }) }
                                        value={ this.state.description }
                                    // onChangeText={this.props.onChangeText}
                                    // returnKeyType="search"
                                    // onSubmitEditing={this.props.onSubmitEditing}
                                    />
                                </View>
                                <View style={ styles.textInputView }>
                                    <TextInput
                                        allowFontScaling={ false }
                                        style={ [
                                            styles.textInput,
                                            styles.textInput2,
                                            {
                                                marginTop: 0
                                            }
                                        ] }
                                        editable={ false }
                                        placeholder="Select Date"
                                        placeholderTextColor={ DARK_GRADIENT }
                                        placeholderTextSize="18"
                                        keyboardType="default"
                                        multiline={ true }
                                    // onChangeText={ text => this.setState({ description: text }) }
                                    // value={this.props.value}
                                    // onChangeText={this.props.onChangeText}
                                    // returnKeyType="search"
                                    // onSubmitEditing={this.props.onSubmitEditing}
                                    />
                                </View>
                                <View style={ styles.calView }>
                                    <Calendar
                                        style={ { elevation: 1 } }
                                        // Initially visible month. Default = Date()
                                        current={ this.state.selectedDate }
                                        // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                                        minDate={ '2020-01-01' }
                                        // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                                        // maxDate={'2020-10-30'}
                                        // Handler which gets executed on day press. Default = undefined
                                        onDayPress={ (day) => {
                                            this.onDaySelected(day)
                                        } }
                                        // Handler which gets executed on day long press. Default = undefined
                                        onDayLongPress={ (day) => {
                                            // this.onDayLongPressed(day)
                                        } }
                                        // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                                        monthFormat={ 'yyyy-MM-dd' }
                                        // Handler which gets executed when visible month changes in calendar. Default = undefined
                                        onMonthChange={ (month) => { console.log('month changed', month) } }
                                        // Hide month navigation arrows. Default = false
                                        hideArrows={ false }
                                        // Replace default arrows with custom ones (direction can be 'left' or 'right')
                                        // renderArrow={(direction) => (<Arrow />)}
                                        // Do not show days of other months in month page. Default = false
                                        hideExtraDays={ false }
                                        // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                                        // day from another month that is visible in calendar page. Default = false
                                        disableMonthChange={ false }
                                        // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                                        firstDay={ 1 }
                                        // Hide day names. Default = false
                                        hideDayNames={ false }
                                        // Show week numbers to the left. Default = false
                                        showWeekNumbers={ false }
                                        // Handler which gets executed when press arrow icon left. It receive a callback can go back month
                                        onPressArrowLeft={ substractMonth => substractMonth() }
                                        // Handler which gets executed when press arrow icon right. It receive a callback can go next month
                                        onPressArrowRight={ addMonth => addMonth() }
                                        // Disable left arrow. Default = false
                                        disableArrowLeft={ false }
                                        // Disable right arrow. Default = false
                                        disableArrowRight={ false }
                                        markedDates={ this.state.markDates }
                                        // markedDates={todayDate}
                                        // markingType={'period'}
                                        theme={ {
                                            calendarBackground: 'white',
                                            textSectionTitleColor: DARK_GRADIENT,    //week's title color
                                            dayTextColor: 'black',
                                            todayTextColor: 'black',   //today's date color
                                            selectedDayTextColor: 'white',   //selected text color
                                            monthTextColor: DARK_GRADIENT,       //month's color
                                            indicatorColor: DARK_GRADIENT,
                                            selectedDayBackgroundColor: DARK_GRADIENT,  //selected day background color
                                            arrowColor: 'black',
                                            textDayFontFamily: 'monospace',
                                            textMonthFontFamily: 'monospace',
                                            textDayHeaderFontFamily: 'monospace',
                                            textDayFontWeight: '300',
                                            textMonthFontWeight: 'bold',
                                            textDayHeaderFontWeight: '300',
                                            textDayFontSize: 14,
                                            textMonthFontSize: 14,
                                            textDayHeaderFontSize: 14,
                                            'stylesheet.calendar.header': {
                                                week: {
                                                    // color: Colors.white,
                                                    padding: 4,
                                                    flexDirection: 'row',
                                                    justifyContent: 'space-between'
                                                }
                                            }
                                        } }
                                    />
                                </View>
                                <TouchableOpacity
                                    onPress={ this.onpressSend }
                                    style={ styles.sendBtn }
                                >
                                    <CustomTextView text={ (this.state.isAlreadyAdded) ? "Edit" : "Add" } style={ styles.textStyle } />
                                </TouchableOpacity>
                            </View>
                        </View>

                    </ScrollView>
                    <Toast
                        ref="toast"
                        style={ { backgroundColor: DARK_GRADIENT } }
                        opacity={ 0.9 }
                    />
                </SafeAreaView>
            </Fragment>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 15
    },
    card: {
        width: "100%",
        backgroundColor: "white",
        padding: 10,
        borderRadius: 8,
        marginBottom: 10,
        shadowColor: DARK_GRADIENT,
        shadowOffset: { height: 5, width: 0 },
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 5
    },
    sendBtn: {
        height: 40,
        width: 100,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 8,
        marginTop: 10,
        alignSelf: "flex-end",
        backgroundColor: DARK_GRADIENT
    },
    textStyle: {
        color: "white",
        fontSize: 18,
        textAlign: "center"
    },
    textInput: {
        fontSize: 15,
        color: DARK_GRADIENT,
        width: "90%",
        marginBottom: 5
    },
    textInputView: {
        width: "98%",
        alignSelf: "center",
        borderBottomWidth: 1,
        borderColor: DARK_GRADIENT,
        marginBottom: Platform.OS === "ios" ? 20 : 5
    },
    imageBottom: {
        bottom: "20%",
        backgroundColor: "rgba(20,20,20,0.6)",
        width: "100%",
        height: "20%",
        justifyContent: "center"
    },
    textInput2: {
        marginLeft: Platform.OS === "ios" ? 0 : -3,
        marginBottom: Platform.OS === "ios" ? 5 : -10
    },
    calView: {
        marginTop: 10
    }
});

const mapStateToProps = state => {
    return {
        isLoading: state.Events.isLoading,
        authResult: state.Events.authResult,
        msg: state.Events.msg,
    };
};

export default connect(
    mapStateToProps,
    {
        initialEventData, addEvent, editEvent
    }
)(AddEvent);
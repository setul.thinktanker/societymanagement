import React, {Component} from "react";
import {View, TouchableOpacity, StyleSheet, Animated} from "react-native";
import CustomTextView from "../Custom/CustomTextView";
import {DARK_GRADIENT, LIGHT_GRADIENT} from "../Commons/Colors";

export default class NotificationItemLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            scaleValue: new Animated.Value(0)
        };
    }

    componentDidMount() {
        Animated.timing(this.state.scaleValue, {
            toValue: 1,
            duration: 600,
            delay: this.props.index * 350
        }).start();
    }

    render() {
        return (
            <Animated.View style={[styles.container, {opacity: this.state.scaleValue}]}>
                <CustomTextView style={styles.descriptionStyle} text={this.props.item.title}/>
                <CustomTextView style={styles.dateStyle} text={this.props.item.created_at}/>
            </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        backgroundColor: "white",
        padding: 8,
        borderRadius: 8,
        marginBottom: 10,
        shadowColor: DARK_GRADIENT,
        shadowOffset: {height: 5, width: 0},
        shadowOpacity: 0.5,
        shadowRadius: 5,
        elevation: 5
    },
    descriptionStyle: {
        fontSize: 16,
        color: DARK_GRADIENT
    },
    dateStyle: {
        fontSize: 12,
        color: "#1E86A7",
        textAlign: "right"
    }
});

/* @flow */

import React, { Component } from "react";
import {
  View,
  StyleSheet,
  Image,
  Animated,
  Easing,
  StatusBar,
  Alert,
  BackHandler
} from "react-native";
import { connect } from 'react-redux';
import LinearGradient from "react-native-linear-gradient";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import CustomTextView from "../Custom/CustomTextView";
import { getUser } from "../Database/allSchema";
import DeviceInfo from 'react-native-device-info';
import { getToken } from '../Actions';

let version = DeviceInfo.getVersion();
let timing = 4000;

class Splash extends Component {
  constructor () {
    super();
    this.spinValue = new Animated.Value(0);
  }
  componentWillMount() {
    this.spin();
    console.warn('Device Vesion', version);
    // this.props.getToken()
  }
  navigate() {
    getUser().then((user) => {
      if (user && user.length > 0) {
        this.props.navigation.replace("Home");
      }
      else {
        this.props.navigation.replace("Login");
      }
    })
  }
  componentDidMount() {
    this.spin();
    setTimeout(() => {
      this.navigate();
    }, 2000);
  }
  // componentWillReceiveProps(nextProps) {
  //   console.log('authResult', nextProps.authResult);
  //   if (nextProps.authResult == 'Get_Token_Success') {
  //     console.log('splashAuthToken', nextProps.splashAuthToken)
  //     // setTimeout(() => {
  //     this.navigate();
  //     // }, 500);
  //   }
  //   if (nextProps.authResult == 'Get_Token_Err') {
  //     Alert.alert('Oops', 'Something wents wrong please try again later');
  //     Alert.alert(
  //       'Oops',
  //       'Something wents wrong please try again later.',
  //       [
  //         { text: 'OK', onPress: () => BackHandler.exitApp() },
  //       ],
  //       { cancelable: false },
  //     );
  //   }
  // }

  spin() {
    this.spinValue.setValue(0);
    Animated.timing(this.spinValue, {
      toValue: 1,
      duration: timing,
      easing: Easing.linear
    }).start(() => this.spin());
  }

  render() {
    const spin = this.spinValue.interpolate({
      inputRange: [0, 1],
      outputRange: ["0deg", "360deg"]
    });

    return (
      <LinearGradient
        colors={ [DARK_GRADIENT, LIGHT_GRADIENT] }
        style={ styles.container }
      >
        <StatusBar backgroundColor="transparent" barStyle="light-content" />
        <Animated.Image
          style={ { width: 250, height: 250, transform: [{ rotate: spin }] } }
          source={ require("../img/circle.png") }
        />
        <View style={ styles.logoView }>
          <Image
            style={ styles.imageStyle }
            source={ require("../img/cityscape.png") }
          />
          <CustomTextView style={ styles.text } text="Society Management" />
          <View style={ styles.line } />
        </View>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  imageStyle: {
    tintColor: "white",
    height: 100,
    width: 100,
    alignSelf: "center"
  },
  text: {
    color: "white",
    fontSize: 10
  },
  line: {
    height: 3,
    width: 100,
    borderRadius: 63,
    marginTop: 1,
    backgroundColor: "white"
  },
  logoView: {
    position: "absolute",
    justifyContent: "center",
    alignItems: "center"
  }
});

const mapStateToProps = state => {
  return {
    isLoading: state.Splash.isLoading,
    authResult: state.Splash.authResult,
    splashAuthToken: state.Splash.splashAuthToken
  };
};
export default connect(
  mapStateToProps,
  {
    getToken
  }
)(Splash);
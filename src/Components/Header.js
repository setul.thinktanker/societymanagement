import React, { Component } from 'react';
import { View, 
    StyleSheet,
    Image,
    TouchableOpacity,
    StatusBar
 } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import CustomTextView from '../Custom/CustomTextView';
import {DARK_GRADIENT,LIGHT_GRADIENT} from '../Commons/Colors';

export default class  extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <LinearGradient
        start={{x: 0, y: 0.3}}
        end={{x: 1.5, y: 0}}
        colors={[DARK_GRADIENT, LIGHT_GRADIENT]}
        style={styles.container}>
        <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content" />
            {/* {(this.props.leftImage) ? */}
            <TouchableOpacity
              onPress = {this.props.onBackPress}
              style = {styles.imgView}
            >
            <Image
                style={[styles.imageStyle,{marginLeft:0,marginRight:10}]}
                source={this.props.leftImage}
            />
            </TouchableOpacity>
            {/* : null } */}
            <CustomTextView style = {styles.textStyle} text={this.props.headerText}/>
            {/* {(this.props.rightImage) ?     */}
            <TouchableOpacity
              onPress = {this.props.onRightPress}
              style = {styles.imgView}
            >
              <Image
                  style={styles.imageStyle}
                  source={this.props.rightImage}
              />
            </TouchableOpacity>
            {/* : null } */}
        
        </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      width:'100%',
      justifyContent:'space-between',
      flexDirection:'row',
      paddingHorizontal:10,
      alignItems:'center'
    },
    imageStyle:{
      height:25,
      width:25,
      tintColor:'white',
      alignSelf:'center',
      marginLeft:10
    },
    imgView:{
      height:'100%',
      justifyContent:'center',
      alignItems:'center'
    },
    startView:{
      flex:1,
      width:'70%',
      alignSelf:'flex-start',
      alignItems:'center',
      flexDirection:'row'
    },
    textStyle:{
      fontSize:20,
      color:'white',
      marginLeft:10
    }
  });

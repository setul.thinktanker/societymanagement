import React, { Component, Fragment } from "react";
import LinearGradient from "react-native-linear-gradient";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import { View, StatusBar, SafeAreaView, StyleSheet, TouchableOpacity, Alert, Text } from 'react-native'
import Header from "./Header";
import { Agenda } from 'react-native-calendars';
import CustomTextView from "../Custom/CustomTextView";
import { getUser } from "../Database/allSchema";
import { eventList } from "../Actions";
import { connect } from "react-redux";
import AsyncStorage from '@react-native-community/async-storage';
import moment from 'moment';

userId = ''
societyId = ''
authToken = ''

class Event extends Component {
    constructor (props) {
        super(props);
        this.state = {
            items: {},
            pageNo: 1,
        }
    }
    onBackPress() {
        this.props.navigation.goBack();
    }
    componentWillMount() {
        this.storeToken()
    }
    storeToken = () => {
        try {
            AsyncStorage.getItem('@authToken_Key')
                .then((token) => {
                    console.log('token', token)
                    authToken = token
                    getUser().then((user) => {
                        userId = user[0].id;
                        societyId = user[0].societyId
                        this.props.eventList({ societyId, pageNo: this.state.pageNo, token })
                    }).catch((err) => {
                        console.log('err', err)
                    });
                })
                .catch((err) => {
                    console.log('err', err)
                })
        } catch (e) {
            console.log('Error in store authToken', e);
        }
    }


    loadItems(day) {
        setTimeout(() => {
            for (let i = -15; i < 85; i++) {
                const time = day.timestamp + i * 24 * 60 * 60 * 1000;
                const strTime = this.timeToString(time);
                if (!this.state.items[strTime]) {
                    this.state.items[strTime] = [];
                    const numItems = Math.floor(Math.random() * 5);
                    for (let j = 0; j < numItems; j++) {
                        this.state.items[strTime].push({
                            name: 'Item for ' + strTime + ' #' + j,
                            // height: Math.max(50, Math.floor(Math.random() * 10))
                        });
                    }
                }
            }
            const newItems = {};
            Object.keys(this.state.items).forEach(key => { newItems[key] = this.state.items[key]; });
            console.log('newItems', newItems)
            this.setState({
                items: newItems
            }, () => {
                console.log('this.state.items', this.state.items)
            });
        }, 500);
    }

    renderItem(item) {
        return (
            <TouchableOpacity
                style={ [styles.item, { marginTop: 20 }] }
                onPress={ () => Alert.alert(item.name) }
            >
                <CustomTextView
                    style={ [styles.textStyle] }
                    text={ 'Ramoliya Bharvi' } />
                <View style={ { flexDirection: 'row' } }>
                    <CustomTextView
                        style={ [styles.textStyle] }
                        text={ 'Event :' } />
                    <CustomTextView
                        style={ [styles.textStyle] }
                        text={ 'Birthday Party' } />
                </View>
            </TouchableOpacity>
        );
    }

    renderEmptyDate() {
        return (
            <TouchableOpacity
                style={ [styles.item] }
            >
                <CustomTextView
                    style={ [styles.textStyle] }
                    text={ 'No Event Available.' } />
            </TouchableOpacity>
        );
    }

    rowHasChanged(r1, r2) {
        return r1.name !== r2.name;
    }

    timeToString(time) {
        const date = new Date(time);
        return date.toISOString().split('T')[0];
    }
    onAddPress() {
        this.props.navigation.navigate('AddEvent')
    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={ { flex: 0, backgroundColor: DARK_GRADIENT } } />
                <SafeAreaView style={ { flex: 1 } }>
                    <StatusBar backgroundColor={ DARK_GRADIENT } barStyle="light-content" />
                    <View style={ { height: 50 } }>
                        <Header
                            headerText={ "Events" }
                            leftImage={ require("../img/left-arrow.png") }
                            onBackPress={ () => this.onBackPress() }
                            rightImage={ require("../img/add.png") }
                            onRightPress={ () => this.onAddPress() }
                        />
                    </View>

                    <Agenda
                        items={ this.state.items }
                        loadItemsForMonth={ this.loadItems.bind(this) }
                        selected={ new Date() }
                        renderItem={ this.renderItem.bind(this) }
                        renderEmptyDate={ this.renderEmptyDate.bind(this) }
                        rowHasChanged={ this.rowHasChanged.bind(this) }
                        // renderDay={ (day, item) => {
                        //     // console.log('day', day)
                        //     // console.log('item', item)
                        //     return (
                        //         <View style={ { backgroundColor: 'red', height: 30, width: '100%', flex: 1 } } >
                        //             {/* <Text>{ day }</Text> */ }
                        //             {/* <Text>{ item }</Text> */ }
                        //         </View>
                        //     );
                        // } }
                        // Max amount of months allowed to scroll to the past. Default = 50
                        pastScrollRange={ 50 }
                        // Max amount of months allowed to scroll to the future. Default = 50
                        futureScrollRange={ 50 }
                        // markingType={ 'period' }
                        // markedDates={{
                        //    '2017-05-08': {textColor: '#43515c'},
                        //    '2017-05-09': {textColor: '#43515c'},
                        //    '2017-05-14': {startingDay: true, endingDay: true, color: 'blue'},
                        //    '2017-05-21': {startingDay: true, color: 'blue'},
                        //    '2017-05-22': {endingDay: true, color: 'gray'},
                        //    '2017-05-24': {startingDay: true, color: 'gray'},
                        //    '2017-05-25': {color: 'gray'},
                        //    '2017-05-26': {endingDay: true, color: 'gray'}}}
                        // monthFormat={'yyyy'}
                        theme={ {
                            calendarBackground: '#1F90AC',
                            textSectionTitleColor: DARK_GRADIENT,
                            selectedDayBackgroundColor: DARK_GRADIENT,
                            textDayHeaderFontWeight: '700',
                            selectedDayTextColor: '#ffffff',
                            todayTextColor: DARK_GRADIENT,
                            dayTextColor: 'white',
                            textDisabledColor: '#d9e1e8',
                            // dotColor: '#00adf5',
                            selectedDotColor: '#ffffff',
                            arrowColor: 'orange',
                            disabledArrowColor: '#d9e1e8',
                            monthTextColor: 'white',
                            agendaKnobColor: DARK_GRADIENT,
                            indicatorColor: DARK_GRADIENT,
                            'stylesheet.calendar.header': {
                                week: {
                                    marginTop: 5,
                                    flexDirection: 'row',
                                    justifyContent: 'space-between'
                                }
                            }
                        } }
                        style={ {
                            // borderRadius: 5,
                        } }
                    />

                </SafeAreaView>
            </Fragment>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10
    },
    clenderView: {
        // paddingTop: 20,
    },
    item: {
        backgroundColor: 'white',
        borderRadius: 5,
        padding: 10,
        marginRight: 10,
        marginVertical: 5
    },
    emptyDate: {
        height: 15,
        flex: 1,
        paddingTop: 30
    },
    textStyle: {
        color: DARK_GRADIENT,
        fontSize: 14
    }
})

const mapStateToProps = state => {
    return {
        events: state.Events.event,
        isLoading: state.Events.isListLoading,
        isData: state.Events.isData,
        authResult: state.Events.authResult
    };
};

export default connect(
    mapStateToProps,
    {
        eventList
    }
)(Event);
import React, { Component, Fragment } from "react";
import {
  View,
  StatusBar,
  StyleSheet,
  TextInput,
  Image,
  FlatList,
  ScrollView,
  TouchableOpacity,
  SafeAreaView
} from "react-native";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";
import CustomTextView from "../Custom/CustomTextView";
import ImagePicker from "react-native-image-crop-picker";
import Video from "react-native-video";
import Toast, { DURATION } from "react-native-easy-toast";
import Header from "./Header";

export default class AddComplaint extends Component {
  constructor(props) {
    super(props);
    this.state = {
      imageName: "",
      videoName: "",
      attachImage: null,
      attachImages: [],
      AttachVideo: null,
      isMultipleImages: true
    };
  }
  onBackPress() {
    this.props.navigation.goBack();
  }
  onpressSend = () => {
    //   console.warn('onPress send')
    const { title, description, attachImage, attachImages } = this.state;
    isValid = true;
    if (!title) {
      isValid = false;
      this.refs.toast.show("Please enter complaint subject", 2000);
    } else if (!description) {
      isValid = false;
      this.refs.toast.show("Please enter complaint description", 2000);
    }

    if (isValid) {
      //api call
    }
  };
  pickedImage() {
    console.warn("atteach image");
    ImagePicker.openPicker({
      maxFiles: 10,
      multiple: true
    })
      .then(image => {
        console.warn("img", image.length);
        if (image.length > 1) {
          try {
            this.setState({
              attachImage: null,
              imageName:
                image[0].filename + " , " + image[1].filename + " , ...",
              attachImages: { uri: image }
            });
            console.warn("attachImages", this.state.attachImages);
          } catch (c) {
            console.warn(c);
          }
        } else {
          try {
            image.map(i => {
              this.setState({
                imageName: i.filename,
                attachImage: { uri: i.path }
              });
            });
          } catch (c) {
            console.warn(c);
          }
        }
      })
      .catch(err => {
        console.warn("err", err);
      });
  }

  pickedVideo() {
    // console.warn('atteach video');
    ImagePicker.openPicker({
      mediaType: "video"
    })
      .then(video => {
        console.warn(video);
        try {
          this.setState({
            videoName: video.filename,
            AttachVideo: { uri: video.path }
          });
          console.warn("attachvideo", this.state.AttachVideo);
        } catch (error) {
          console.warn(error);
        }
      })
      .catch(err => {
        console.warn(err);
      });
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: DARK_GRADIENT }} />
        <SafeAreaView style={{ flex: 1 }}>
          <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content" />
          <View style={{ height: 50 }}>
            <Header
              headerText={"New Complaint"}
              leftImage={require("../img/left-arrow.png")}
              onBackPress={() => this.onBackPress()}
            />
          </View>
          <ScrollView style={{ flex: 1 }}>
            <View style={styles.container}>
              <View style={styles.card}>
                <View style={styles.textInputView}>
                  <TextInput
                    allowFontScaling={false}
                    style={[styles.textInput, styles.textInput2]}
                    placeholder="Enter Subject"
                    placeholderTextColor={DARK_GRADIENT}
                    placeholderTextSize="18"
                    keyboardType="default"
                    onChangeText={text => this.setState({ title: text })}
                    // value={this.props.value}
                    // onChangeText={this.props.onChangeText}
                    // returnKeyType="search"
                    // onSubmitEditing={this.props.onSubmitEditing}
                  />
                </View>
                <View style={styles.textInputView}>
                  <TextInput
                    allowFontScaling={false}
                    style={[
                      styles.textInput,
                      styles.textInput2,
                      {
                        marginTop: 0
                      }
                    ]}
                    placeholder="Enter Description"
                    placeholderTextColor={DARK_GRADIENT}
                    placeholderTextSize="18"
                    keyboardType="default"
                    multiline={true}
                    onChangeText={text => this.setState({ description: text })}
                    // value={this.props.value}
                    // onChangeText={this.props.onChangeText}
                    // returnKeyType="search"
                    // onSubmitEditing={this.props.onSubmitEditing}
                  />
                </View>
                <TouchableOpacity
                  onPress={() => this.pickedImage()}
                  style={[
                    styles.textInputView,
                    { marginBottom: 5, marginTop: 15 }
                  ]}
                >
                  <CustomTextView
                    style={styles.textInput}
                    text={
                      this.state.imageName
                        ? this.state.imageName
                        : "Attach Image / Images"
                    }
                  />
                </TouchableOpacity>
                {this.state.attachImage ? (
                  <View style={{ marginVertical: 10, height: 200 }}>
                    <Image
                      style={{ width: "100%", height: 200 }}
                      source={this.state.attachImage}
                    />
                    <View style={styles.imageBottom}>
                      <CustomTextView
                        text={this.state.imageName}
                        style={styles.textStyle}
                      />
                    </View>
                  </View>
                ) : this.state.attachImages ? (
                  <FlatList
                    contentContainerStyle={{ marginVertical: 8 }}
                    data={this.state.attachImages.uri}
                    horizontal
                    showsHorizontalScrollIndicator={false}
                    renderItem={({ item, index }) => {
                      let arr = item.path;
                      return (
                        <Image
                          style={{
                            height: 92,
                            width: 90,
                            backgroundColor: "black",
                            margin: 1
                          }}
                          source={{ uri: arr }}
                        />
                      );
                    }}
                  />
                ) : null}
                <TouchableOpacity
                  onPress={() => this.pickedVideo()}
                  style={[styles.textInputView, { marginBottom: 0 }]}
                >
                  <CustomTextView
                    style={styles.textInput}
                    text={
                      this.state.videoName
                        ? this.state.videoName
                        : "Attach video"
                    }
                  />
                </TouchableOpacity>
                {this.state.AttachVideo ? (
                  <View style={{ marginTop: 10, height: 200 }}>
                    <Video
                      style={{ width: "100%", height: 200 }}
                      source={this.state.AttachVideo}
                      ref={ref => {
                        this.player = ref;
                      }}
                    />
                    <View style={styles.imageBottom}>
                      <CustomTextView
                        text={this.state.videoName}
                        style={styles.textStyle}
                      />
                    </View>
                  </View>
                ) : null}
                <TouchableOpacity
                  onPress={this.onpressSend}
                  style={styles.sendBtn}
                >
                  <CustomTextView text="Send" style={styles.textStyle} />
                </TouchableOpacity>
              </View>
            </View>
            <Toast
              ref="toast"
              style={{ backgroundColor: DARK_GRADIENT }}
              opacity={0.9}
            />
          </ScrollView>
        </SafeAreaView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 15
  },
  card: {
    width: "100%",
    backgroundColor: "white",
    padding: 10,
    borderRadius: 8,
    marginBottom: 10,
    shadowColor: DARK_GRADIENT,
    shadowOffset: { height: 5, width: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5
  },
  sendBtn: {
    height: 40,
    width: 100,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 8,
    marginTop: 10,
    alignSelf: "flex-end",
    backgroundColor: DARK_GRADIENT
  },
  textStyle: {
    color: "white",
    fontSize: 18,
    textAlign: "center"
  },
  textInput: {
    fontSize: 15,
    color: DARK_GRADIENT,
    width: "90%",
    marginBottom: 5
  },
  textInputView: {
    width: "98%",
    alignSelf: "center",
    borderBottomWidth: 1,
    borderColor: DARK_GRADIENT,
    marginBottom: Platform.OS === "ios" ? 20 : 5
  },
  imageBottom: {
    bottom: "20%",
    backgroundColor: "rgba(20,20,20,0.6)",
    width: "100%",
    height: "20%",
    justifyContent: "center"
  },
  textInput2: {
    marginLeft: Platform.OS === "ios" ? 0 : -3,
    marginBottom: Platform.OS === "ios" ? 5 : -10
  }
});

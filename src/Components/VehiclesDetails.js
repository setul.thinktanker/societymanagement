import React, { Component, Fragment } from "react";
import {
  View,
  StatusBar,
  FlatList,
  StyleSheet,
  Animated,
  Image,
  Linking,
  ActivityIndicator,
  TouchableOpacity,
  SafeAreaView
} from "react-native";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import CustomTextView from "../Custom/CustomTextView";
import HeaderSearch from "./HeaderSearch";
import VehiclesDetailsItemLayout from "./VehiclesDetailsItemLayout";
import Swipeable from "react-native-gesture-handler/Swipeable";
import { RectButton } from "react-native-gesture-handler";
import { vehicleDetailsList } from "../Actions";
import { connect } from "react-redux";
import { getUser } from "../Database/allSchema"
import AsyncStorage from '@react-native-community/async-storage';

let authToken = '';
let societyId = '';

class VehiclesDetails extends Component {
  constructor (props) {
    super(props);
    this.state = {
      vehiclesData: [],
      pageCount: 1,
    };
  }
  storeToken = () => {
    try {
      AsyncStorage.getItem('@authToken_Key')
        .then((token) => {
          console.log('token', token)
          authToken = token
          getUser().then((user) => {
            userId = user[0].id;
            societyId = user[0].societyId
            console.log('user[0].id', user[0].id);
            console.log('user[0].societyId', user[0].societyId);
            console.log('this.state.pageCount', this.state.pageCount)
            this.props.vehicleDetailsList({
              userId,
              societyId,
              token: authToken,
              pageCount: this.state.pageCount,
            })
          }).catch((err) => {
            console.log('err', err)
          });
        })
        .catch((err) => {
          console.log('err', err)
        })
    } catch (e) {
      console.log('Error in store authToken');
    }
  }

  componentWillMount() {
    this.storeToken()
    // console.log('vehicles', this.props.vehiclesData)
    this.setState({ searchResult: this.props.vehiclesData });
  }
  componentWillReceiveProps(nextProps) {
    // console.log('vehicles', nextProps.vehiclesData)
    this.setState({ searchResult: nextProps.vehiclesData });
  }
  handleLoadMore = () => {
    console.log('in handle more')
    this.setState(
      {
        pageCount: this.state.pageCount + 1
      },
      () => {
        this.props.vehicleDetailsList({
          userId,
          societyId,
          token: authToken,
          pageCount: this.state.pageCount,
          list: this.props.vehiclesData
        })
        // this.props.societyMembersList({ token: authToken, pageCount: this.state.pageCount, list: this.props.users })
      }
    );
  };
  onSearchText(text) {
    let temp = this.props.vehiclesData.filter(
      item =>
        item.name.toLowerCase().includes(text.toLowerCase()) ||
        item.mobileNo.toLowerCase().includes(text.toLowerCase()) ||
        item.vehicle_type.toLowerCase().includes(text.toLowerCase()) ||
        item.block.toLowerCase().includes(text.toLowerCase()) ||
        item.vehicle_no.toLowerCase().includes(text.toLowerCase())
    );

    this.setState({
      searchResult: temp,
      searchText: text
    });
  }
  onBackPress() {
    this.props.navigation.goBack();
  }
  renderRightActionCall = (text, color, x, progress, item) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0]
    });
    return (
      <Animated.View
        style={ [
          styles.animateViewStyle,
          { transform: [{ translateX: trans }] }
        ] }
      >
        <RectButton
          style={ [styles.rightAction, { backgroundColor: color }] }
          onPress={ () => {
            Linking.openURL(`tel:${item.mobile_no}`);
          } }
        >
          <Image
            style={ [styles.actionImg, { tintColor: "white" }] }
            source={ require("../img/call.png") }
          />
        </RectButton>
      </Animated.View>
    );
  };

  renderRightActionSms = (text, color, x, progress, item) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0]
    });
    return (
      <Animated.View
        style={ [
          styles.animateViewStyle,
          { transform: [{ translateX: trans }] }
        ] }
      >
        <RectButton
          style={ [styles.rightAction, { backgroundColor: color }] }
          onPress={ () => {
            Linking.openURL(`sms:${item.mobile_no}`);
          } }
        >
          <Image
            style={ [styles.actionImg, { tintColor: "white" }] }
            source={ require("../img/sms.png") }
          />
        </RectButton>
      </Animated.View>
    );
  };

  renderRightActions(progress, item) {
    return (
      <View style={ { width: 192, flexDirection: "row" } }>
        { this.renderRightActionCall("Call", "#2DC170", 192, progress, item) }
        { this.renderRightActionSms("Message", "#ffab00", 128, progress, item) }
      </View>
    );
  }
  updateRef = ref => {
    this._swipeableRow = ref;
  };
  close = () => {
    this._swipeableRow.close();
  };
  onBackPress() {
    this.props.navigation.goBack();
  }
  render() {
    console.log('vehicles', this.props.vehiclesData)
    return (
      <Fragment>
        <SafeAreaView style={ { flex: 0, backgroundColor: DARK_GRADIENT } } />
        <SafeAreaView style={ { flex: 1 } }>
          <StatusBar backgroundColor={ DARK_GRADIENT } barStyle="light-content" />
          <View style={ { height: 50 } }>
            <HeaderSearch
              headerText={ "Vehicles Details" }
              leftImage={ require("../img/left-arrow.png") }
              rightImage={ require("../img/search.png") }
              onBackPress={ () => this.onBackPress() }
              onChangeText={ text => this.onSearchText(text) }
              onPressClose={ () => {
                console.warn("onpress close");
                this.setState({
                  searchResult: this.props.vehiclesData,
                  searchText: ""
                });
              } }
            />
          </View>
          { (this.props.vehiclesData.length > 0 && !this.props.isLoading) ?
            <FlatList
              contentContainerStyle={ { padding: 10 } }
              data={ this.state.searchResult }
              onEndReached={ this.handleLoadMore }
              onEndReachedThreshold={ 1 }
              showsVerticalScrollIndicator={ false }
              renderItem={ ({ item, index }) => {
                return (
                  <Swipeable
                    ref={ this.updateRef }
                    friction={ 2 }
                    leftThreshold={ 30 }
                    rightThreshold={ 40 }
                    renderRightActions={ progress =>
                      this.renderRightActions(progress, item)
                    }
                  >
                    <VehiclesDetailsItemLayout item={ item } index={ index } />
                  </Swipeable>
                );
              } }
              ListFooterComponent={
                (this.props.isData) ?
                  <ActivityIndicator size="small" color={ '#20AAB7' } />
                  :
                  null
              }
            />
            :
            <View style={ { flex: 1, height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center' } }>
              { (this.props.isLoading) ?
                <ActivityIndicator
                  size={ 70 }
                  color={ DARK_GRADIENT }
                />
                :
                <CustomTextView
                  numberOfLines={ 1 }
                  text={ 'No Vehicles found.' }
                  style={ { color: DARK_GRADIENT, fontSize: 15 } }
                />
              }

            </View>
          }

        </SafeAreaView>
      </Fragment>
    );
  }
}
const styles = StyleSheet.create({
  animateViewStyle: {
    flex: 1,
    marginBottom: 10
  },
  actionText: {
    color: "white",
    fontSize: 20,
    backgroundColor: "transparent",
    padding: 10
  },
  rightAction: {
    alignItems: "center",
    flex: 1,
    borderRadius: 8,
    marginHorizontal: 2,
    justifyContent: "center"
  },
  actionImg: {
    height: 40,
    width: 40
  }
});

const mapStateToProps = state => {
  return {
    vehiclesData: state.Listing.vehicles,
    isLoading: state.Listing.isLoadingVehicles,
    isData: state.Listing.isDataVehicles
  };
};

export default connect(
  mapStateToProps,
  {
    vehicleDetailsList,
  }
)(VehiclesDetails);

import React, { Component } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import CustomTextView from "../Custom/CustomTextView";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";

export default class SocietyMemberItemLayout extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={{ width: "22%", justifyContent: 'center' }}>
                    {(this.props.item.profileImg) ?
                        <Image
                            source={{ uri: 'http://192.168.0.139:8000/app_user/JCQS6Q1rlq.png' }}
                            // source={{ uri: 'https://bootdey.com/img/Content/avatar/avatar6.png' }}
                            style={[
                                styles.imgStyle,
                            ]}
                        />
                        : <Image
                            source={require("../img/man.png")}
                            style={[
                                styles.imgStyle,
                            ]}
                        />}
                </View>
                <View style={{ flex: 1, marginLeft: 5, }}>
                    <View style={{ flex: 1, flexDirection: "row", width: "100%", marginBottom: 5 }}>
                        <CustomTextView
                            numberOfLines={1}
                            text={this.props.item.name}
                            style={[
                                styles.textStyle,
                                { width: "80%", fontSize: 20, fontWeight: "700" }
                            ]}
                        />
                    </View>
                    <View style={{ flexDirection: "row", width: "100%" }}>
                        <CustomTextView
                            numberOfLines={1}
                            text={"Mobile No. : "}
                            style={[styles.textStyle, { width: "30%" }]}
                        />
                        <CustomTextView
                            numberOfLines={1}
                            text={this.props.item.mobile_no}
                            style={[styles.textStyle, { width: "70%" }]}
                        />
                    </View>
                    <View style={{ flexDirection: "row", width: "100%" }}>
                        <CustomTextView
                            numberOfLines={1}
                            text={"House No. : "}
                            style={[styles.textStyle, { width: "30%" }]}
                        />
                        <CustomTextView
                            numberOfLines={1}
                            text={this.props.item.block}
                            style={[styles.textStyle, { width: "70%" }]}
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'white',
        width: "100%",
        padding: 8,
        borderRadius: 8,
        marginBottom: 10,
        elevation: 10
    },
    textStyle: {
        color: DARK_GRADIENT,
        fontSize: 15,
        marginBottom: 5
    },
    imgStyle: {
        height: 75,
        width: 75,
        borderRadius: 75 / 2,
        borderWidth: 2,
        borderColor: DARK_GRADIENT,
        alignSelf: "flex-start",
    }
});
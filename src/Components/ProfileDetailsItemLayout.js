import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import CustomTextView from "../Custom/CustomTextView";
import LinearGradient from "react-native-linear-gradient";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";

export default class ProfileDetailsItemLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <TouchableOpacity
        onPress={this.props.onPressItem}
        style={styles.container}
      >
        <LinearGradient
          colors={[LIGHT_GRADIENT, DARK_GRADIENT]}
          style={styles.mainView}
        >
          <View style={styles.imgView}>
            <Image source={this.props.item.img} style={styles.img} />
          </View>
        </LinearGradient>
        <View style={styles.editView}>
          <Image source={require("../img/edit.png")} style={styles.editImg} />
        </View>
        <CustomTextView text={this.props.item.title} style={styles.textStyle} />
      </TouchableOpacity>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
    height: 140,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10,
    backgroundColor: "white",
    shadowColor: "black",
    shadowOffset: { height: 5, width: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5
  },
  subContainer: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 10
  },
  textStyle: {
    color: DARK_GRADIENT,
    marginTop: 8,
    fontSize: 15
  },
  img: {
    height: 40,
    width: 40,
    tintColor: DARK_GRADIENT
  },
  editImg: {
    height: 14,
    width: 14,
    tintColor: "white"
  },
  editView: {
    height: 25,
    width: 25,
    borderRadius: 25,
    right: "28%",
    top: "48%",
    position: "absolute",
    alignSelf: "flex-end",
    backgroundColor: DARK_GRADIENT,
    alignItems: "center",
    justifyContent: "center"
  },
  imgView: {
    backgroundColor: "white",
    height: 62,
    width: 62,
    borderRadius: 62 / 2,
    alignSelf: "center",
    alignItems: "center",
    justifyContent: "center"
  },
  mainView: {
    height: 65,
    width: 65,
    borderRadius: 65 / 2,
    alignItems: "center",
    justifyContent: "center"
  }
});

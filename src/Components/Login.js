/* @flow */

import React, { Component, Fragment } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Image,
  Platform,
  StatusBar,
  SafeAreaView,
  Modal,
  ActivityIndicator
} from "react-native";
import { userLogin, getSocietyDetails } from "../Actions";
import { connect } from "react-redux";
import LinearGradient from "react-native-linear-gradient";
import Toast, { DURATION } from "react-native-easy-toast";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import CustomTextView from "../Custom/CustomTextView";
import CustomPasswordInputView from "../Custom/CustomPasswordInputView";
import AsyncStorage from '@react-native-community/async-storage';

class Login extends Component {
  constructor (props) {
    super(props);
    this.state = {
      mobileNo: '',
      password: '',
      showPicker: false,
      showPickerForgotPsw: false,
      isIndicatorOn: false
    };
  }

  componentWillMount() {
    // console.log('in componentwillmount', this.props.splashAuthToken);
  }

  storeToken = async (authToken) => {
    // console.log('storeToken', authToken);
    try {
      await AsyncStorage.setItem('@authToken_Key', authToken)
      let token = await AsyncStorage.getItem('@authToken_Key')
      console.log('token', token)
    } catch (e) {
      // saving error
      console.log('Error in store authToken');
    }
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.authResult == "Login Success") {
      // console.log('authToken', nextProps.authToken);
      this.storeToken(nextProps.authToken);
      this.props.navigation.replace("Home");
    }
    if (nextProps.authResult == "Login Failed") {
      console.warn(nextProps.authResult)
      console.warn(nextProps.error)
      this.setState({ password: '' })
      this.refs.toast.show(nextProps.error, 2500);
    }
    if (nextProps.authResult == "Login Error") {
      this.refs.toast.show("Somthing wents wrong please try again later", 2500);
    }
    //if society code match
    if (nextProps.authResult.text == "Societycode Success") {
      this.setState({ isIndicatorOn: false, showPicker: false }, () => {
        this.props.navigation.navigate("SignUp");
      })
    }
    //if society code not match
    if (nextProps.authResult.text == "Societycode Failed") {
      this.setState({ isIndicatorOn: false }, () => {
        this.refs.toast.show(
          "Please enter valid society code.",
          1500
        );
      })
    }
    //if society code error in match
    if (nextProps.authResult.text == "Societycode Error") {
      this.setState({ isIndicatorOn: false }, () => {
        this.refs.modalToast.show(
          "Something wents wrong please try again later.",
          1500
        );
      })
    }
  }

  getScietyCode(societyCode) {
    if (!this.state.societyCode) {
      this.refs.modalToast.show(
        "Please enter society code for sign up",
        1500
      );
    } else {
      this.setState({ isIndicatorOn: true })
      this.props.getSocietyDetails(societyCode, this.props.splashAuthToken)
    }
    // if (!this.state.societyCode) {
    //   this.refs.modalToast.show(
    //     "Please enter society code for sign up",
    //     1500
    //   );
    // } else if (this.state.societyCode == 'pushp@thinktanker') {
    //   this.setState({ showPicker: false });
    //   this.props.navigation.navigate("SignUp");
    // } else {
    //   this.refs.modalToast.show(
    //     "Please enter valid society code ",
    //     1500
    //   );
    // }
  }

  onPressLogin = () => {
    let isValid = true;
    const { mobileNo, password } = this.state;
    const number = /^[0-9\b]{10,10}$/;

    //check validation - mobile number
    if (!mobileNo) {
      isValid = false;
      this.refs.toast.show("Please enter mobile number", 1500);
    } else if (number.test(mobileNo) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid mobile number", 1500);
    }
    //check validation - password
    if (!password && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter password", 1500);
    }

    if (isValid) {
      console.warn("isValid");
      this.props.userLogin({ mobileNo, password, token: this.props.splashAuthToken });
    } else {
      console.warn("isNotValid");
    }
  };

  render() {
    return (
      <Fragment>
        <SafeAreaView style={ { flex: 0, backgroundColor: DARK_GRADIENT } } />
        <SafeAreaView style={ { flex: 1, backgroundColor: LIGHT_GRADIENT } }>
          <StatusBar backgroundColor={ DARK_GRADIENT } barStyle="light-content" />
          <LinearGradient
            colors={ [DARK_GRADIENT, LIGHT_GRADIENT] }
            style={ styles.container }
          >
            <View style={ styles.firstView }>
              <Image
                style={ styles.imageStyle }
                source={ require("../img/cityscape.png") }
              />
              <CustomTextView style={ styles.text } text="Society Management" />
              <View style={ styles.line } />
            </View>

            <View style={ styles.inputView }>
              <Image
                style={ styles.imageIconStyle }
                source={ require("../img/smartphone.png") }
              />
              <TextInput
                allowFontScaling={ false }
                style={ styles.textInput }
                placeholder="Enter Mobile Number"
                placeholderTextColor="white"
                placeholderTextSize="15"
                keyboardType="number-pad"
                maxLength={ 10 }
                value={ this.state.mobileNo }
                editable={ !this.props.isLoading }
                onChangeText={ text => this.setState({ mobileNo: text }) }
              // returnKeyType="search"
              // onSubmitEditing={this.props.onSubmitEditing}
              />
            </View>

            <View style={ styles.inputView }>
              <Image
                style={ styles.imageIconStyle }
                source={ require("../img/lock.png") }
              />
              <CustomPasswordInputView
                placeholder={ "Enter Password" }
                editable={ !this.props.isLoading }
                value={ this.state.password }
                onChangeText={ text => this.setState({ password: text }) }
              />
            </View>

            <TouchableOpacity
              activeOpacity={ 1 }
              onPress={ this.onPressLogin }
              style={ styles.btn }
            >
              { this.props.isLoading ? (
                <Image
                  style={ {
                    height: 40,
                    width: 40,
                    backgroundColor: "transparent"
                  } }
                  source={ require("../img/loader2.gif") }
                />
              ) : (
                  <CustomTextView style={ styles.textStyle } text="Login" />
                ) }
            </TouchableOpacity>



            <TouchableOpacity
              activeOpacity={ 0.9 }
              style={ styles.linkStyle }
              onPress={ () => {
                this.setState({ showPickerForgotPsw: true })
              } }>
              <CustomTextView
                style={ styles.textStyle2 }
                text="Forgot Password ?"
              />
            </TouchableOpacity>

            <TouchableOpacity
              onPress={ () => {
                this.setState({ showPicker: true });
              } }
              activeOpacity={ 0.9 }
              style={ styles.linkStyle }
            >
              <CustomTextView
                style={ styles.textStyle2 }
                text="Don't have an account Yet ? "
              />
              <View style={ { borderBottomWidth: 1, borderColor: "#1A2980" } }>
                <CustomTextView style={ styles.textStyle2 } text="Sign Up" />
              </View>
            </TouchableOpacity>

            <Modal
              visible={ this.state.showPicker }
              transparent
              animationType={ "none" }
              supportedOrientations={ ["portrait", "landscape"] }
            >
              <TouchableOpacity
                activeOpacity={ 1 }
                style={ styles.modalStyle }
                onPress={ () => {
                  // this.setState({ showOwnerCurrentDetails: false })
                } }
              >
                <LinearGradient
                  end={ { x: 0, y: 0.5 } }
                  start={ { x: 1.5, y: 0.3 } }
                  colors={ [DARK_GRADIENT, LIGHT_GRADIENT] }
                  style={ [styles.modalView, { height: 140 }] }
                >
                  <View style={ { flex: 1, padding: 10 } }>
                    <View style={ styles.inputView }>
                      <Image
                        style={ styles.imageIconStyle }
                        source={ require("../img/cityscape.png") }
                      />
                      <TextInput
                        allowFontScaling={ false }
                        style={ [styles.textInput, { bottom: 2, width: "85%" }] }
                        placeholder="Enter Society Code"
                        placeholderTextColor="white"
                        placeholderTextSize="15"
                        keyboardType="default"
                        // maxHeight={60}
                        // multiline = {true}
                        // numberOfLines={4}
                        // blurOnSubmit={true}
                        onChangeText={ text =>
                          this.setState({ societyCode: text })
                        }
                      // value={this.props.value}
                      // onChangeText={this.props.onChangeText}
                      // returnKeyType="search"
                      // onSubmitEditing={this.props.onSubmitEditing}
                      />
                    </View>
                  </View>
                  <View
                    style={ {
                      flexDirection: "row",
                      bottom: 0,
                      position: "absolute",
                      height: "30%",
                      backgroundColor: "rgba(255,255,255,0.3)",
                      width: "100%",
                      justifyContent: "space-between"
                    } }
                  >
                    <TouchableOpacity
                      onPress={ () => {
                        this.setState({ showPicker: false });
                      } }
                      activeOpacity={ 0.4 }
                      style={ styles.modalBtn }
                    >
                      <CustomTextView
                        style={ { color: "white", fontSize: 15 } }
                        text="Cancel"
                      />
                    </TouchableOpacity>
                    <View
                      style={ {
                        borderColor: DARK_GRADIENT,
                        borderWidth: 0.5,
                        width: 0
                      } }
                    />
                    <TouchableOpacity
                      onPress={ () => this.getScietyCode(this.state.societyCode) }
                      activeOpacity={ 0.4 }
                      style={ styles.modalBtn }
                    >

                      { (this.state.isIndicatorOn) ?
                        <ActivityIndicator size="small" color="white" />
                        :
                        <CustomTextView
                          style={ { color: "white", fontSize: 15 } }
                          text="Ok"
                        />
                      }
                    </TouchableOpacity>
                  </View>
                </LinearGradient>
              </TouchableOpacity>
              <Toast
                ref="modalToast"
                style={ { backgroundColor: "#1A2980" } }
                opacity={ 0.9 }
              />
            </Modal>

            <Toast
              ref="toast"
              style={ { backgroundColor: "#1A2980" } }
              opacity={ 0.9 }
            />

            <Modal
              visible={ this.state.showPickerForgotPsw }
              transparent
              animationType={ "none" }
              supportedOrientations={ ["portrait", "landscape"] }
            >
              <TouchableOpacity
                activeOpacity={ 1 }
                style={ styles.modalStyle }
                onPress={ () => {
                  // this.setState({ showOwnerCurrentDetails: false })
                } }
              >
                <LinearGradient
                  end={ { x: 0, y: 0.5 } }
                  start={ { x: 1.5, y: 0.3 } }
                  colors={ [DARK_GRADIENT, LIGHT_GRADIENT] }
                  style={ [styles.modalView, { height: 140 }] }
                >

                  <View style={ { flex: 1, padding: 10 } }>
                    <View style={ styles.inputView }>
                      <Image
                        style={ styles.imageIconStyle }
                        source={ require("../img/smartphone.png") }
                      />
                      <TextInput
                        allowFontScaling={ false }
                        style={ [styles.textInput, { bottom: 2, width: "85%" }] }
                        placeholder="Enter Mobile Number"
                        placeholderTextColor="white"
                        placeholderTextSize="15"
                        keyboardType="number-pad"
                        maxLength={ 10 }
                        // maxHeight={60}
                        // multiline = {true}
                        // numberOfLines={4}
                        // blurOnSubmit={true}
                        onChangeText={ text =>
                          this.setState({ mobileNoForgotPassword: text })
                        }
                      // value={this.props.value}
                      // onChangeText={this.props.onChangeText}
                      // returnKeyType="search"
                      // onSubmitEditing={this.props.onSubmitEditing}
                      />
                    </View>
                  </View>
                  <View
                    style={ {
                      flexDirection: "row",
                      bottom: 0,
                      position: "absolute",
                      height: "30%",
                      backgroundColor: "rgba(255,255,255,0.3)",
                      width: "100%",
                      justifyContent: "space-between"
                    } }
                  >
                    <TouchableOpacity
                      onPress={ () => {
                        this.setState({ showPickerForgotPsw: false });
                      } }
                      activeOpacity={ 0.4 }
                      style={ styles.modalBtn }
                    >
                      <CustomTextView
                        style={ { color: "white", fontSize: 15 } }
                        text="Cancel"
                      />
                    </TouchableOpacity>
                    <View
                      style={ {
                        borderColor: DARK_GRADIENT,
                        borderWidth: 0.5,
                        width: 0
                      } }
                    />
                    <TouchableOpacity
                      onPress={ () => {
                        if (!this.state.mobileNo) {
                          this.refs.modalForgotToast.show(
                            "Please enter mobile number for reset password.",
                            1500
                          );
                        } else {
                          this.setState({ showPickerForgotPsw: false });
                        }
                        //registration api call
                      } }
                      activeOpacity={ 0.4 }
                      style={ styles.modalBtn }
                    >
                      <CustomTextView
                        style={ { color: "white", fontSize: 15 } }
                        text="Ok"
                      />
                    </TouchableOpacity>
                  </View>
                </LinearGradient>
              </TouchableOpacity>
              <Toast
                ref="modalForgotToast"
                style={ { backgroundColor: "#1A2980" } }
                opacity={ 0.9 }
              />
            </Modal>
          </LinearGradient>
        </SafeAreaView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 60,
    paddingHorizontal: 20,
    alignItems: "center"
  },
  firstView: {
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 30
  },
  imageStyle: {
    tintColor: "white",
    height: 90,
    width: 90,
    alignSelf: "center"
  },
  text: {
    color: "white",
    fontSize: 10
  },
  line: {
    height: 3,
    width: 90,
    borderRadius: 63,
    backgroundColor: "white",
    marginTop: 1
  },
  inputView: {
    width: "100%",
    borderRadius: 30,
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,0.3)",
    marginTop: 15,
    paddingHorizontal: 5,
    flexDirection: "row",
    paddingVertical: Platform.OS === "ios" ? 10 : 0
  },
  imageIconStyle: {
    height: 25,
    width: 25,
    tintColor: "white",
    alignSelf: "center",
    marginLeft: 8
  },
  textInput: {
    fontSize: 15,
    color: "white",
    width: "100%",
    marginLeft: 8
  },
  textStyle: {
    fontSize: 20,
    alignSelf: "center",
    color: "white"
    // marginHorizontal:10,
  },
  textStyle2: {
    fontSize: 18,
    color: "#1A2980"
  },
  textView: {
    width: "100%",
    marginTop: 15
  },
  btn: {
    height: 40,
    width: 100,
    borderRadius: 30,
    marginTop: 20,
    alignSelf: "flex-end",
    alignItems: "center",
    backgroundColor: "#1A2980",
    justifyContent: "center"
  },
  linkStyle: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  },
  modalStyle: {
    flex: 1,
    backgroundColor: "rgba(40,40,40,0.8)",
    alignItems: "center",
    justifyContent: "center"
  },
  modalView: {
    height: "15%",
    width: "90%",
    borderRadius: 8,
    backgroundColor: "white",
    justifyContent: "center",
    overflow: "scroll"
  },
  modalText: {
    color: "white",
    fontSize: 20,
    // textAlign:'left',
    marginLeft: 20
  },
  modalBtn: {
    // backgroundColor:'rgba(255,255,255,0.2)',
    borderRadius: 30,
    width: "49%",
    alignItems: "center",
    justifyContent: "center"
  },
  modalRound: {
    height: 75,
    width: 75,
    borderRadius: 75 / 2,
    backgroundColor: DARK_GRADIENT,
    elevation: 5,
    alignItems: "center",
    justifyContent: "center"
  }
});

const mapStateToProps = state => {
  return {

    isLoading: state.Login.isLoading,
    authResult: state.Login.authResult,
    error: state.Login.error,
    authToken: state.Login.authToken,
    splashAuthToken: state.Splash.splashAuthToken
  };
};

export default connect(
  mapStateToProps,
  {
    userLogin,
    getSocietyDetails
  }
)(Login);

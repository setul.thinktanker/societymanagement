import React, {Component} from 'react';
import {View, Text, FlatList, TouchableOpacity, Image, StyleSheet, Modal, Button} from 'react-native';
import Header from "./Header";

class PhotoGalleryDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listData: [
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                }, {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                },
                {
                    title: "Data",
                    image: "https://res.cloudinary.com/demo/image/upload/q_60/sample.jpg"
                }
            ],
            isVisible: false,
            imgURL: null,
        };
    }

    onBackPress() {
        if (this.state.isVisible === true) this.setState({isVisible: false});
        else this.props.navigation.goBack();
    }

    render() {
        // const id = this.props.navigation.getParam('GalleryDetails', 0);
        return (
            <View style={{flex: 1, backgroundColor: '#F5F5F5'}}>
                <Modal
                    animationType={"slide"}
                    transparent={true}
                    visible={this.state.isVisible}
                    onRequestClose={() => {
                        console.log("Modal has been closed.")
                    }}>

                    <View style={myStyles.modal}>
                        {/*<View style={{*/}
                        {/*    flex:1,*/}
                        {/*    flexDirection: 'row',*/}
                        {/*    justifyContent: 'flex-end',*/}
                        {/*}}>*/}
                        {/*    <Image style={{*/}
                        {/*        height: 50, width: 50,*/}
                        {/*    }}*/}
                        {/*           onPress={() => this.hideModal()}*/}
                        {/*           source={require('../img/close.png')}/>*/}
                        {/*</View>*/}

                        {
                            this.state.imgURL !== null ?
                                <Image style={{height: '88%', width: '100%'}}
                                       source={{uri: this.state.imgURL}}/>
                                :
                                null
                        }

                        <View style={{margin: "2%"}}>
                            <Button title="Close" onPress={() => this.hideModal()}/>
                        </View>
                    </View>
                </Modal>

                <View style={{height: 50}}>
                    <Header
                        headerText={"Photos Listing"}
                        leftImage={require("../img/left-arrow.png")}
                        onBackPress={() => this.onBackPress()}
                    />
                </View>

                <View style={{flex: 1, padding: 4}}>
                    <FlatList
                        data={this.state.listData}
                        numColumns={3}
                        renderItem={({item, index}) =>
                            this.displayPhotoData(item, index)}
                    />
                </View>
            </View>
        );
    }

    hideModal() {
        this.setState({isVisible: false});
    }

    componentWillUnmount() {
        this.hideModal()
    }

    componentDidMount() {
        this.hideModal()
    }

    displayPhotoData(item, index) {
        return (
            <TouchableOpacity style={{flex: 1}} activeOpacity={0.8}
                              onPress={() => this.setState(
                                  {
                                      isVisible: true,
                                      imgURL: item.image
                                  })
                              }>
                <View style={{margin: 1, elevation: 1}}>
                    <Image style={{height: 100, width: '100%'}}
                           source={{uri: item.image}}/>
                </View>
            </TouchableOpacity>
        )
    }
}

const myStyles = StyleSheet.create({
    modal: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: "#F5F5F5",
        height: 400,
        width: '80%',
        borderRadius: 16,
        borderWidth: 4,
        borderColor: '#22C4C2',
        marginTop: 80,
        marginLeft: 40,
    }
});

export default PhotoGalleryDetails;

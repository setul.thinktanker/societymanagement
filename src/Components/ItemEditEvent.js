import React, { Component } from "react";
import {
    View,
    StyleSheet,
    TouchableOpacity,
    Animated,
    Easing,
    Image,
    UIManager,
    Platform,
    FlatList,
    LayoutAnimation
} from "react-native";
import CustomTextView from "../Custom/CustomTextView";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";

export default class ItemEditEvent extends Component {
    constructor (props) {
        super(props);
        this.RotateValueHolder = new Animated.Value(0);
        this.state = {
            toggle: false,
            to: "0deg",
            from: "180deg"
        };
        if (Platform.OS === "android") {
            UIManager.setLayoutAnimationEnabledExperimental(true);
        }
    }
    StartImageRotateFunction() {
        //linear,easeInEaseOut
        LayoutAnimation.configureNext(LayoutAnimation.Presets.linear);

        this.setState(
            {
                to: this.state.toggle ? "180deg" : "360deg",
                from: this.state.toggle ? "0deg" : "180deg",
                toggle: !this.state.toggle
            },
            () => {
                this.RotateValueHolder.setValue(0);
                Animated.timing(this.RotateValueHolder, {
                    toValue: 1,
                    // duration: 100,
                    easing: Easing.linear
                }).start();
            }
        );
    }

    onPressViewMore = () => {
        this.StartImageRotateFunction();
    };

    setDate(date) {
        let tempData = date;
        let temp = [];
        for (let index = 0; index < date.length; index++) {
            const element = date[index];
            let obj = {};
            obj.name = tempData[index].toString();
            temp.push(obj);
        }
        console.log('temp', temp);
        this.setState({ eventDate: temp })
    }

    // setDate(date){
    //     let tempData = date;
    //             let temp = [];
    //             for (let index = 0; index < tempData.length; index++) {
    //               let obj = {};
    //               obj.name = tempData[index].toString();
    //               temp.push(obj);
    //             }
    // };

    render() {
        const RotateData = this.RotateValueHolder.interpolate({
            inputRange: [0, 1],
            outputRange: [this.state.to, this.state.from]
        });
        let dates = []
        dateArr = this.props.item.date;
        for (let index = 0; index < dateArr.length; index++) {
            // const element = dateArr[index];
            let obj = {};
            obj.name = dateArr[index].toString();
            obj.index = index
            dates.push(obj);
        }
        console.log('dates', dates);
        return (
            <View style={ { flex: 1, marginTop: this.props.item.inSociety ? 8 : 4 } }>
                <LinearGradient
                    start={ { x: 0, y: 0.3 } }
                    end={ { x: 1.5, y: 0 } }
                    colors={ [DARK_GRADIENT, LIGHT_GRADIENT] }
                    style={ [styles.container, {}] }
                >
                    <View style={ { flex: 1 } }>
                        <View
                            style={ { flexDirection: "row", width: "100%", marginBottom: 5 } }
                        >
                            <CustomTextView
                                numberOfLines={ 1 }
                                text={ this.props.item.title }
                                style={ [
                                    styles.textStyle,
                                    {
                                        width: "70%",
                                        fontSize: 20,
                                        fontWeight: "700",
                                        marginTop: 0
                                    }
                                ] }
                            />
                        </View>



                        <View
                            style={ {
                                height: this.state.toggle ? null : 0,
                                overflow: "hidden"
                            } }
                        >
                            <View style={ { flexDirection: "row", width: "100%" } }>
                                <CustomTextView
                                    numberOfLines={ 1 }
                                    text={ "Date" }
                                    style={ [styles.textStyle, { width: "16%" }] }
                                />
                                <CustomTextView text=" :  " style={ [styles.textStyle] } />
                                <FlatList
                                    data={ dates }
                                    numColumns={ 2 }
                                    onEndReached={ this.handleLoadMore }
                                    onEndReachedThreshold={ 1 }
                                    renderItem={ ({ item, index }) => {
                                        return (
                                            <View style={ { flexDirection: 'row' } }>
                                                <CustomTextView
                                                    numberOfLines={ 5 }
                                                    text={ item.name }
                                                    style={ [styles.textStyle, {}] }
                                                />
                                                { (dates.length > 1 && (dates.length != item.index + 1)) ?
                                                    <CustomTextView text=" , " style={ [styles.textStyle] } />
                                                    :
                                                    null }
                                            </View>
                                        );
                                    } }
                                />
                            </View>

                            { this.props.item.description ? (
                                <View style={ { flexDirection: "row", width: "100%" } }>
                                    <CustomTextView
                                        numberOfLines={ 1 }
                                        text={ "Services" }
                                        style={ [styles.textStyle, { width: "16%" }] }
                                    />
                                    <CustomTextView text=" :  " style={ [styles.textStyle] } />
                                    <CustomTextView
                                        numberOfLines={ 3 }
                                        text={ this.props.item.description }
                                        style={ [styles.textStyle, { width: "65%" }] }
                                    />
                                </View>
                            ) : null }
                        </View>

                        <View style={ styles.viewMore }>
                            <TouchableOpacity
                                onPress={ this.onPressViewMore }
                                style={ styles.btn }
                            >
                                <Animated.Image
                                    style={ {
                                        height: 15,
                                        width: 15,
                                        tintColor: "white",
                                        top: 1,
                                        transform: [{ rotate: RotateData }]
                                    } }
                                    source={ require("../img/downArrow.png") }
                                />
                            </TouchableOpacity>
                        </View>
                    </View>
                </LinearGradient>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: "100%",
        padding: 8,
        borderRadius: 8,
        marginBottom: 5,
        marginTop: 5
    },
    textStyle: {
        color: "white",
        fontSize: 15,
        marginTop: 5
    },
    viewMore: {
        height: 30,
        width: 30,
        borderRadius: 30 / 2,
        alignSelf: "flex-end",
        position: "absolute",
        bottom: 0,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "rgba(255,255,255,0.4)"
    },
    btn: {
        flex: 1,
        height: "100%",
        width: "100%",
        alignItems: "center",
        justifyContent: "center"
    },
    label: {
        alignSelf: "flex-end",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 2,
        height: 15,
        width: "20%",
        top: 0,
        position: "absolute",
        zIndex: 1111,
        backgroundColor: DARK_GRADIENT
    }
});

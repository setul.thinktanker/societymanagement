/* @flow */

import React, { Component, Fragment } from "react";
import {
  View,
  TextInput,
  StyleSheet,
  ScrollView,
  Image,
  Modal,
  StatusBar,
  SafeAreaView,
  TouchableOpacity,
  Platform,
} from "react-native";
import { connect } from "react-redux";
import LinearGradient from "react-native-linear-gradient";
import CustomTextView from "../Custom/CustomTextView";
import CustomDropDown from "../Custom/CustomDropDown";
import CustomPasswordInputView from "../Custom/CustomPasswordInputView";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import Toast, { DURATION } from "react-native-easy-toast";
import { signUpInitialState, userSignUp } from "../Actions";
import firebase from 'react-native-firebase';
import AsyncStorage from '@react-native-community/async-storage';


class SignUp extends Component {
  constructor (props) {
    super(props);
    this.state = {
      gender: [{ name: "Female" }, { name: "Male" }],
      houseType: [],
      blockType: [],
      blockNoType: [],
      radioBtnsData: ["Yes", "No"],
      showOwnerLivingInfo: false,
      showOwnerCurrentDetails: false,
      isHidePwd: true,
      isRented: 0,
    };
  }

  componentWillMount() {
    console.log('this.props.houseType', this.props.houseType);
  }
  componentDidMount() {
    this.props.signUpInitialState();
  }
  storeToken = async (authToken) => {
    console.log('storeToken', authToken);
    try {
      await AsyncStorage.setItem('@authToken_Key', authToken)
      let token = await AsyncStorage.getItem('@authToken_Key')
      console.log('token', token)
    } catch (e) {
      // saving error
      console.log('Error in store authToken');
    }
  }
  componentWillReceiveProps(nextProp) {
    // console.log('houseType', nextProp.houseType);
    // console.warn('authResult', nextProp.authResult);
    // console.log('signUpErr', nextProp.signUpErr);
    if (nextProp.authResult == "SignUp Success") {
      console.log('authToken', nextProp.authToken);
      this.storeToken(nextProp.authToken);
      this.props.navigation.replace("Home");
    }
    if (nextProp.authResult == "SignUp Failed") {
      this.refs.toast.show(nextProp.signUpErr, 1500);
    }
    if (nextProp.authResult == "SignUp Error") {
      this.refs.toast.show("Something wents wrong please try again later", 1500);
    }
  }

  onPressSignUp = () => {
    const name = /^[a-zA-Z '.]{3,30}$/;
    const number = /^[0-9\b]{10,10}$/;
    const emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let isValid = true;
    const {
      fname,
      lname,
      userId,
      mobileNo,
      email,
      selectedGender,
      selectedHouseType,
      selectedBlock,
      selectedBlockNo,
      password,
      cPassword,
      isOwnerStayInSociety,
      currentAddress,
      isRented
    } = this.state;

    //check validaiton - first name
    if (!fname) {
      isValid = false;
      this.refs.toast.show("Please enter first name", 1500);
    } else if (name.test(fname) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid first name", 1500);
    }
    //check validation - last name
    if (!lname && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter last name", 1500);
    } else if (name.test(lname) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid last name", 1500);
    }
    //check validation - mobile number
    if (!mobileNo && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter mobile number", 2000);
    } else if (number.test(mobileNo) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid mobile number", 2000);
    }

    //email
    if (!email && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter e-mail", 2000);
    } else if (emailReg.test(email) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid e-mail", 2000);
    }

    //check validation - gender
    if (!selectedGender && isValid) {
      isValid = false;
      this.refs.toast.show("Please select gender", 2000);
    }
    //check validation - house type
    if (!selectedHouseType && isValid) {
      isValid = false;
      this.refs.toast.show("Please select house type", 2000);
    }
    //check validation - block (abcd)
    // if (!selectedBlock && isValid) {
    //   isValid = false;
    //   this.refs.toast.show("Please select block", 2000);
    // }

    //check validation - house no
    if (!selectedBlockNo && isValid) {
      isValid = false;
      this.refs.toast.show("Please select house no", 2000);
    }
    //check validation - password
    if (!password && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter password", 2000);
    } else if (password.length < 6 && isValid) {
      isValid = false;
      this.refs.toast.show("Password must be at least 6 characters.", 2000);
    }

    //check validation - confirm password
    if (!cPassword && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter password", 2000);
    } else if ((password != cPassword) && isValid) {
      isValid = false;
      this.refs.toast.show(
        "Please enter confirm password same as password",
        2000
      );
    }

    if (isValid) {
      console.warn("is Valid");
      this.setState({ showOwnerLivingInfo: true });
      // this.props.userSignUp({
      //   fname, lname, userId, mobileNo, email, selectedGender, selectedHouseType, selectedBlock,
      //   selectedBlockNo, isOwnerStayInSociety, currentAddress, isRented, password
      // });
    } else {
      console.warn("is not valid");
    }
  };

  render() {
    return (
      <Fragment>
        <SafeAreaView style={ { flex: 0, backgroundColor: DARK_GRADIENT } } />
        <SafeAreaView style={ { flex: 1, backgroundColor: LIGHT_GRADIENT } }>
          <StatusBar backgroundColor={ DARK_GRADIENT } barStyle="light-content" />
          <LinearGradient
            colors={ [DARK_GRADIENT, LIGHT_GRADIENT] }
            style={ styles.container }
          >
            <ScrollView
              style={ { flex: 1, height: "100%", width: "100%" } }
              showsVerticalScrollIndicator={ false }
              contentContainerStyle={ {
                paddingBottom: 150,
                paddingTop: 40,
                paddingHorizontal: 20
              } }
            >
              <View style={ styles.firstView }>
                <Image
                  style={ styles.imageStyle }
                  source={ require("../img/cityscape.png") }
                />
                <CustomTextView style={ styles.text } text="Society Management" />
                <View style={ styles.line } />
              </View>

              <View style={ styles.inputView }>
                <Image
                  style={ styles.imageIconStyle }
                  source={ require("../img/user.png") }
                />
                <TextInput
                  allowFontScaling={ false }
                  style={ styles.textInput }
                  placeholder="Enter First Name"
                  placeholderTextColor="white"
                  placeholderTextSize="15"
                  keyboardType="default"
                  maxLength={ 30 }
                  editable={ !this.props.isLoading }
                  onChangeText={ text => this.setState({ fname: text }) }
                // value={this.props.value}
                // onChangeText={this.props.onChangeText}
                // returnKeyType="search"
                // onSubmitEditing={this.props.onSubmitEditing}
                />
              </View>
              <View style={ styles.inputView }>
                <Image
                  style={ styles.imageIconStyle }
                  source={ require("../img/user.png") }
                />
                <TextInput
                  allowFontScaling={ false }
                  style={ styles.textInput }
                  placeholder="Enter Last Name"
                  placeholderTextColor="white"
                  placeholderTextSize="15"
                  keyboardType="default"
                  maxLength={ 30 }
                  editable={ !this.props.isLoading }
                  onChangeText={ text => this.setState({ lname: text }) }
                // value={this.props.value}
                // returnKeyType="search"
                // onSubmitEditing={this.props.onSubmitEditing}
                />
              </View>
              <View style={ styles.inputView }>
                <Image
                  style={ styles.imageIconStyle }
                  source={ require("../img/smartphone.png") }
                />
                <TextInput
                  allowFontScaling={ false }
                  style={ styles.textInput }
                  placeholder="Enter Mobile Number"
                  placeholderTextColor="white"
                  placeholderTextSize="15"
                  keyboardType="number-pad"
                  maxLength={ 10 }
                  editable={ !this.props.isLoading }
                  onChangeText={ text => this.setState({ mobileNo: text }) }
                // value={this.props.value}
                // returnKeyType="search"
                // onSubmitEditing={this.props.onSubmitEditing}
                />
              </View>
              <View style={ styles.inputView }>
                <Image
                  style={ styles.imageIconStyle }
                  source={ require("../img/envelope.png") }
                />
                <TextInput
                  allowFontScaling={ false }
                  style={ styles.textInput }
                  placeholder="Enter E-mail"
                  placeholderTextColor="white"
                  placeholderTextSize="15"
                  keyboardType="email-address"
                  maxLength={ 30 }
                  editable={ !this.props.isLoading }
                  onChangeText={ text => this.setState({ email: text }) }
                // value={this.props.value}
                // returnKeyType="search"
                // onSubmitEditing={this.props.onSubmitEditing}
                />
              </View>

              <CustomDropDown
                enabled={ !this.props.isLoading }
                image={ require("../img/gender.png") }
                item={ this.state.gender }
                prompt="Select Gender"
                onValueChange={ (itemValue, itemIndex) => {
                  if (itemValue != 0) {
                    this.setState({
                      selectedGender: this.state.gender[itemIndex - 1].name
                    });
                  }
                } }
                selectedValue={ this.state.selectedGender || "Select Gender" }
                text={ this.state.selectedGender }
              />

              <CustomDropDown
                enabled={ !this.props.isLoading }
                image={ require("../img/house-outline.png") }
                item={ this.props.houseType }
                prompt="Select House Type"
                onValueChange={ (itemValue, itemIndex) => {
                  console.log('itemValue itemIndex', itemValue + itemIndex)
                  if (itemValue != 0) {
                    let arr = [];
                    //blank block type and house no
                    this.setState({ blockType: arr, blockNoType: arr })
                    //check block type exist
                    let data = this.props.houseType[itemIndex - 1].blocklist;
                    console.log('data', data);

                    data.forEach(element => {
                      if (Object.keys(element)[0] == 'name') {
                        //set house type & block no
                        this.setState({
                          selectedHouseType: this.props.houseType[itemIndex - 1].name,
                          blockType: this.props.houseType[itemIndex - 1].blocklist
                        });
                      } else {
                        //set house no & house type
                        let tempData = data[0].house_no;
                        let temp = [];
                        for (let index = 0; index < tempData.length; index++) {
                          let obj = {};
                          obj.name = tempData[index].toString();
                          temp.push(obj);
                        }
                        this.setState({
                          blockNoType: temp,
                          selectedHouseType: this.props.houseType[itemIndex - 1].name
                        }, () => {
                          console.log('temp', this.state.blockNoType);
                        })
                      }
                    })

                  }
                } }
                selectedValue={
                  this.state.selectedHouseType || "Select House Type"
                }
                text={ this.state.selectedHouseType }
              />

              <View
                style={ {
                  flexDirection: "row",
                  width: "100%",
                  justifyContent: "space-between"
                } }
              >
                { (this.state.blockType.length > 0) ?
                  <View style={ { width: "42%" } }>
                    <CustomDropDown
                      enabled={ !this.props.isLoading }
                      item={ this.state.blockType }
                      prompt="Select Block"
                      onValueChange={ (itemValue, itemIndex) => {
                        if (itemValue != 0) {
                          this.setState({
                            selectedBlock: this.state.blockType[itemIndex - 1].name
                          }, () => {
                            let data = this.state.blockType[itemIndex - 1].house_no;
                            let temp = [];
                            for (let index = 0; index < data.length; index++) {
                              let obj = {};
                              obj.name = data[index].toString();
                              temp.push(obj);
                            }
                            this.setState({ blockNoType: temp }, () => {
                              console.log('temp', this.state.blockNoType);
                            })
                          });
                        }
                      } }
                      selectedValue={
                        this.state.selectedBlock || "Select Block"
                      }
                      text={ this.state.selectedBlock }
                    />
                  </View>
                  : null
                }
                { (this.state.blockNoType.length > 0) ?
                  <View style={ { width: (this.state.blockType.length > 0) ? "52%" : "100%" } }>
                    <CustomDropDown
                      enabled={ !this.props.isLoading }
                      item={ this.state.blockNoType }
                      prompt="Select House No"
                      onValueChange={ (itemValue, itemIndex) => {
                        if (itemValue != 0) {
                          this.setState({
                            selectedBlockNo: this.state.blockNoType[itemIndex - 1].name
                          });
                        }
                      } }
                      selectedValue={ this.state.selectedBlockNo }
                      text={ this.state.selectedBlockNo }
                    />
                  </View>
                  : null
                }

              </View>

              <View style={ styles.inputView }>
                <Image
                  style={ styles.imageIconStyle }
                  source={ require("../img/lock.png") }
                />
                <CustomPasswordInputView
                  // style={[styles.textInput,{width:'70%'}]}
                  placeholder={ "Enter Password" }
                  editable={ !this.props.isLoading }
                  onChangeText={ text => this.setState({ password: text }) }
                />
              </View>

              <View style={ styles.inputView }>
                <Image
                  style={ styles.imageIconStyle }
                  source={ require("../img/lock.png") }
                />
                <CustomPasswordInputView
                  editable={ !this.props.isLoading }
                  placeholder={ "Enter Confirm Password" }
                  onChangeText={ text => this.setState({ cPassword: text }) }
                />
              </View>

              <TouchableOpacity
                activeOpacity={ 0.9 }
                onPress={ (this.props.isLoading) ? null : this.onPressSignUp }
                style={ styles.btn }
              >
                { this.props.isLoading ? (
                  <Image
                    style={ {
                      height: 40,
                      width: 40,
                      backgroundColor: "transparent"
                    } }
                    source={ require("../img/loader2.gif") }
                  />
                ) : (
                    <CustomTextView style={ styles.textStyle } text="Sign up" />
                  ) }
              </TouchableOpacity>

              <TouchableOpacity
                onPress={ () => this.props.navigation.navigate("Login") }
                activeOpacity={ 0.9 }
                style={ styles.linkStyle }
              >
                <CustomTextView
                  style={ styles.textStyle2 }
                  text="Already have an account? "
                />
                <View style={ { borderBottomWidth: 1, borderColor: "#1A2980" } }>
                  <CustomTextView style={ styles.textStyle2 } text="Sign in" />
                </View>
              </TouchableOpacity>

              <Toast
                ref="toast"
                style={ { backgroundColor: "#1A2980" } }
                opacity={ 0.9 }
              />
            </ScrollView>
          </LinearGradient>
          {/* modal for asking owner living there */ }
          <Modal
            visible={ this.state.showOwnerLivingInfo }
            transparent
            animationType={ "none" }
            supportedOrientations={ ["portrait", "landscape"] }
          >
            <TouchableOpacity
              activeOpacity={ 1 }
              style={ styles.modalStyle }
              onPress={ () => {
                this.setState({ showOwnerLivingInfo: false });
              } }
            >
              <LinearGradient
                end={ { x: 0, y: 0.3 } }
                start={ { x: 1.5, y: 0 } }
                colors={ [DARK_GRADIENT, LIGHT_GRADIENT] }
                style={ styles.modalView }
              >
                <View style={ { flex: 1 } }>
                  <View
                    style={ {
                      flexDirection: "row",
                      height: "60%",
                      alignItems: "center"
                    } }
                  >
                    <Image
                      style={ {
                        height: 40,
                        width: 40,
                        tintColor: "white",
                        marginLeft: 20
                      } }
                      source={ require("../img/attention.png") }
                    />
                    <CustomTextView
                      style={ styles.modalText }
                      text="Owner is living here ?"
                    />
                  </View>

                  <View
                    style={ {
                      flexDirection: "row",
                      bottom: 0,
                      position: "absolute",
                      height: "40%",
                      backgroundColor: "rgba(255,255,255,0.3)",
                      width: "100%",
                      justifyContent: "space-between"
                    } }
                  >
                    <TouchableOpacity
                      onPress={ () => {
                        this.setState({ showOwnerLivingInfo: false, isOwnerStayInSociety: true });
                        // call Signup api
                        this.props.userSignUp({
                          fname: this.state.fname,
                          lname: this.state.lname,
                          userId: this.state.userId,
                          mobileNo: this.state.mobileNo,
                          email: this.state.email,
                          selectedGender: this.state.selectedGender,
                          selectedHouseType: this.state.selectedHouseType,
                          selectedBlock: this.state.selectedBlock,
                          selectedBlockNo: this.state.selectedBlockNo,
                          isOwnerStayInSociety: true,
                          currentAddress: null,
                          isRented: 1,
                          password: this.state.password,
                          token: this.props.splashAuthToken
                        });
                      } }
                      activeOpacity={ 0.4 }
                      style={ styles.modalBtn }
                    >
                      <CustomTextView
                        style={ { color: "white", fontSize: 15 } }
                        text="Yes"
                      />
                    </TouchableOpacity>
                    <View
                      style={ {
                        borderColor: DARK_GRADIENT,
                        borderWidth: 0.5,
                        width: 0
                      } }
                    />
                    <TouchableOpacity
                      onPress={ () => {
                        this.setState({
                          showOwnerLivingInfo: false,
                          showOwnerCurrentDetails: true,
                          isOwnerStayInSociety: false
                        });

                      } }
                      activeOpacity={ 0.4 }
                      style={ styles.modalBtn }
                    >
                      <CustomTextView
                        style={ { color: "white", fontSize: 15 } }
                        text="No"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
              </LinearGradient>
            </TouchableOpacity>
          </Modal>
          {/* modal for asking owner current address there */ }
          <Modal
            visible={ this.state.showOwnerCurrentDetails }
            transparent
            animationType={ "none" }
          // supportedOrientations={["portrait", "landscape"]}
          >

            <TouchableOpacity
              activeOpacity={ 1 }
              style={ styles.modalStyle }
              onPress={ () => {
                // this.setState({ showOwnerCurrentDetails: false })
              } }
            >
              <LinearGradient
                end={ { x: 0, y: 0.5 } }
                start={ { x: 1.5, y: 0.3 } }
                colors={ [DARK_GRADIENT, LIGHT_GRADIENT] }
                style={ [styles.modalView, { height: 200, marginBottom: 20 }] }
              >
                <View style={ { flex: 1, padding: 10 } }>
                  <View style={ styles.inputView }>
                    <Image
                      style={ styles.imageIconStyle }
                      source={ require("../img/address.png") }
                    />
                    <TextInput
                      allowFontScaling={ false }
                      style={ [styles.textInput, { bottom: 2, width: "85%" }] }
                      placeholder="Enter Current Address"
                      placeholderTextColor="white"
                      placeholderTextSize="15"
                      keyboardType="default"
                      maxHeight={ 60 }
                      multiline={ true }
                      // numberOfLines={4}
                      // blurOnSubmit={true}
                      onChangeText={ text =>
                        this.setState({ currentAddress: text })
                      }
                    // value={this.props.value}
                    // onChangeText={this.props.onChangeText}
                    // returnKeyType="search"
                    // onSubmitEditing={this.props.onSubmitEditing}
                    />
                  </View>
                  <View
                    style={ {
                      flexDirection: "row",
                      marginTop: 10,
                      alignItems: "center"
                    } }
                  >
                    <CustomTextView
                      style={ [styles.modalText, { marginLeft: 0 }] }
                      text="It is rented ?"
                    />
                    { this.state.radioBtnsData.map((data, key) => {
                      return (
                        <View key={ key } style={ { flex: 1, marginLeft: 20 } }>
                          { this.state.isRented == key ? (
                            <TouchableOpacity
                              onPress={ () => {
                                console.warn('key', key)
                                this.setState({ isRented: key });
                              } }
                              style={ styles.btnRadio }
                            >
                              <Image
                                style={ styles.img }
                                source={ require("../img/selectedRadioBtn.png") }
                              />
                              <CustomTextView
                                text={ data }
                                style={ [styles.radioText] }
                              />
                            </TouchableOpacity>
                          ) : (
                              <TouchableOpacity
                                onPress={ () => {
                                  console.warn('key', key)
                                  this.setState({ isRented: key });
                                } }
                                style={ styles.btnRadio }
                              >
                                <Image
                                  style={ [styles.img, {}] }
                                  source={ require("../img/unSelectedRadioBtn.png") }
                                />

                                <CustomTextView
                                  text={ data }
                                  style={ [styles.radioText] }
                                />
                              </TouchableOpacity>
                            ) }
                        </View>
                      );
                    }) }
                  </View>
                </View>

                <View
                  style={ {
                    flexDirection: "row",
                    bottom: 0,
                    // position: "absolute",
                    height: "25%",
                    backgroundColor: "rgba(255,255,255,0.3)",
                    width: "100%",
                    justifyContent: "space-between"
                  } }
                >
                  <TouchableOpacity
                    onPress={ () => {
                      // if (!this.state.currentAddress) {
                      //   this.refs.modalToast.show(
                      //     "Please enter current address",
                      //     1500
                      //   );
                      // } else {
                      this.setState({ showOwnerCurrentDetails: false });
                      // }
                      //registration api call
                    } }
                    activeOpacity={ 0.4 }
                    style={ styles.modalBtn }
                  >
                    <CustomTextView
                      style={ { color: "white", fontSize: 15 } }
                      text="cancel"
                    />
                  </TouchableOpacity>

                  <View
                    style={ {
                      borderColor: DARK_GRADIENT,
                      borderWidth: 0.5,
                      width: 0
                    } }
                  />
                  <TouchableOpacity
                    onPress={ () => {
                      if (!this.state.currentAddress) {
                        this.refs.modalToast.show(
                          "Please enter current address",
                          1500
                        );
                      } else {
                        this.setState({ showOwnerCurrentDetails: false });
                        this.props.userSignUp({
                          fname: this.state.fname,
                          lname: this.state.lname,
                          userId: this.state.userId,
                          mobileNo: this.state.mobileNo,
                          email: this.state.email,
                          selectedGender: this.state.selectedGender,
                          selectedHouseType: this.state.selectedHouseType,
                          selectedBlock: this.state.selectedBlock,
                          selectedBlockNo: this.state.selectedBlockNo,
                          isOwnerStayInSociety: false,
                          currentAddress: this.state.currentAddress,
                          isRented: this.state.isRented,
                          password: this.state.password,
                          token: this.props.splashAuthToken
                        });
                        // this.props.navigation.navigate("RenterDetails");
                      }
                    } }
                    activeOpacity={ 0.4 }
                    style={ styles.modalBtn }
                  >
                    <CustomTextView
                      style={ { color: "white", fontSize: 15 } }
                      text="Ok"
                    />
                  </TouchableOpacity>
                </View>
              </LinearGradient>

            </TouchableOpacity>

            <Toast
              ref="modalToast"
              style={ { backgroundColor: "#1A2980" } }
              opacity={ 0.9 }
            />
          </Modal>
        </SafeAreaView>
      </Fragment>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center"
  },
  firstView: {
    justifyContent: "center",
    alignItems: "center"
  },
  imageStyle: {
    tintColor: "white",
    height: 90,
    width: 90,
    alignSelf: "center"
  },
  text: {
    color: "white",
    fontSize: 10
  },
  line: {
    height: 3,
    width: 90,
    borderRadius: 63,
    backgroundColor: "white",
    marginTop: 1
  },
  inputView: {
    width: "100%",
    borderRadius: 30,
    backgroundColor: "rgba(255,255,255,0.3)",
    marginTop: 15,
    paddingHorizontal: 5,
    flexDirection: "row",
    paddingVertical: Platform.OS === "ios" ? 10 : 0
  },
  inputViewDropDown: {
    width: '100%',
    borderRadius: 30,
    backgroundColor: 'rgba(255,255,255,0.3)',
    marginTop: 15,
    paddingHorizontal: 5,
    flexDirection: 'row',
    paddingVertical: Platform.OS === 'ios' ? 10 : 14,
    alignItems: 'center'
  },
  inputTwoDropDown: {
    width: "100%",
    marginTop: 15,
    flexDirection: "row"
  },
  imageIconStyle: {
    height: 20,
    width: 20,
    tintColor: "white",
    alignSelf: "center",
    marginLeft: 8
  },
  textInput: {
    fontSize: 14,
    color: "white",
    width: "100%",
    marginLeft: 8
  },
  textStyle: {
    fontSize: 20,
    alignSelf: "center",
    color: "white"
  },
  btn: {
    height: 40,
    width: 100,
    borderRadius: 30,
    marginTop: 20,
    alignSelf: "flex-end",
    alignItems: "center",
    backgroundColor: "#1A2980",
    justifyContent: "center"
  },
  textStyle2: {
    fontSize: 18,
    color: "#1A2980",
    alignSelf: "center"
  },
  linkStyle: {
    width: "100%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20,
    height: 40
  },
  arrowIconStyle: {
    height: 18,
    width: 18,
    tintColor: "white",
    alignSelf: "center",
    marginLeft: 8
  },
  lineView: {
    borderRightWidth: 1.5,
    borderColor: "white",
    borderRadius: 20,
    height: "80%",
    alignSelf: "center",
    marginLeft: 6
  },
  modalStyle: {
    flex: 1,
    backgroundColor: "rgba(40,40,40,0.8)",
    alignItems: "center",
    justifyContent: "center"
  },
  modalView: {
    height: "20%",
    width: "90%",
    borderRadius: 8,
    backgroundColor: "white",
    justifyContent: "center",
    overflow: "scroll"
  },
  modalText: {
    color: "white",
    fontSize: 20,
    // textAlign:'left',
    marginLeft: 20
  },
  modalBtn: {
    // backgroundColor:'rgba(255,255,255,0.2)',
    borderRadius: 30,
    width: "49%",
    alignItems: "center",
    justifyContent: "center"
  },
  modalRound: {
    height: 75,
    width: 75,
    borderRadius: 75 / 2,
    backgroundColor: DARK_GRADIENT,
    elevation: 5,
    alignItems: "center",
    justifyContent: "center"
  },
  radioText: {
    color: "white",
    fontSize: 15
  },
  img: {
    height: 20,
    width: 20,
    marginLeft: 8,
    marginRight: 5,
    tintColor: "white"
  },
  btnRadio: {
    flexDirection: "row"
  },
  pickerAndroidStyle: {
    flex: 1,
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: 14,
    marginLeft: 10,
    justifyContent: 'center'
  },
});
const mapStateToProps = state => {
  return {
    houseType: state.Login.houseType,
    isLoading: state.SignUp.isLoading,
    signUpErr: state.SignUp.signUpErr,
    authResult: state.SignUp.authResult,
    authToken: state.SignUp.authToken,
    splashAuthToken: state.Splash.splashAuthToken
  };
};

export default connect(
  mapStateToProps,
  {
    signUpInitialState,
    userSignUp
  }
)(SignUp);
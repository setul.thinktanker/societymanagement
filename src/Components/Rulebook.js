import React, { Component, Fragment } from "react";
import { View, StatusBar, FlatList, SafeAreaView } from "react-native";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";
import RulebookItemLayout from "./RulebookItemLayout";
import Header from "./Header";

export default class Rulebook extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rules: [
        {
          rule:
            "Do not throw Wastes/Articles from Windows/ Balconies, always throw it in Garbage duct or designated Garbage bins."
        },
        { rule: "Do not overload lift beyond its capacity. " },
        {
          rule:
            "Do not use lift for Shifting! Vacating the flat, if you observe such practice inform committee members immediately."
        },
        {
          rule:
            "Inform Society before Sub Letting flat and submit rent agreement along with Police Verification of Tenant to the society."
        }
      ]
    };
  }
  onBackPress() {
    this.props.navigation.goBack();
  }
  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: DARK_GRADIENT }} />
        <SafeAreaView style={{ flex: 1 }}>
          <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content" />
          <View style={{ height: 50 }}>
            <Header
              headerText={"Rulebook"}
              leftImage={require("../img/left-arrow.png")}
              onBackPress={() => this.onBackPress()}
            />
          </View>
          <FlatList
            // style = {{padding:10}}
            contentContainerStyle={{ padding: 10 }}
            data={this.state.rules}
            renderItem={({ item, index }) => {
              return <RulebookItemLayout item={item} />;
            }}
          />
        </SafeAreaView>
      </Fragment>
    );
  }
}

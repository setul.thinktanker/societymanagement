import React, { Component, Fragment } from "react";
import { View, StyleSheet, SafeAreaView, StatusBar, FlatList, ActivityIndicator } from 'react-native';
import HeaderSearch from "./HeaderSearch";
import LinearGradient from 'react-native-linear-gradient';
import Swipeable from "react-native-gesture-handler/Swipeable";
import CustomTextView from "../Custom/CustomTextView";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import SocietyMemberItemLayout from './SocietyMemberItemLayout';
import { societyMembersList } from "../Actions";
import { connect } from "react-redux";
import AsyncStorage from '@react-native-community/async-storage';

let authToken = '';
class SocietyMembers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pageCount: 1
        };
    }
    storeToken = () => {
        try {
            AsyncStorage.getItem('@authToken_Key')
                .then((token) => {
                    console.log('token', token)
                    authToken = token;
                    this.props.societyMembersList({ token, pageCount: this.state.pageCount })
                })
                .catch((err) => {
                    console.log('err', err)
                })
        } catch (e) {
            console.log('Error in store authToken');
        }
    }
    componentWillMount() {
        this.storeToken()
        this.setState({ searchResult: this.props.users });
    }
    onBackPress() {
        this.props.navigation.goBack();
    }
    handleLoadMore = () => {
        console.log('in handle more')
        this.setState(
            {
                pageCount: this.state.pageCount + 1
            },
            () => {
                this.props.societyMembersList({ token: authToken, pageCount: this.state.pageCount, list: this.props.users })
            }
        );
    };
    onSearchText(text) {
        let temp = this.props.users.filter(
            item =>
                item.name.toLowerCase().includes(text.toLowerCase()) ||
                item.mobile_no.toLowerCase().includes(text.toLowerCase()) ||
                item.block.toLowerCase().includes(text.toLowerCase())
        );

        this.setState({
            searchResult: temp,
            searchText: text
        });
    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{ flex: 0, backgroundColor: DARK_GRADIENT }} />
                <SafeAreaView style={{ flex: 1 }}>
                    <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content" />
                    <View style={{ height: 50 }}>
                        <HeaderSearch
                            headerText={"Society Members"}
                            leftImage={require("../img/left-arrow.png")}
                            rightImage={require("../img/search.png")}
                            onBackPress={() => this.onBackPress()}
                            onChangeText={text => this.onSearchText(text)}
                            onPressClose={() => {
                                console.warn("onpress close", this.state.societyMembers);
                                this.setState({
                                    searchResult: this.state.societyMembers,
                                    searchText: ""
                                });
                            }}
                        />
                    </View>

                    <LinearGradient
                        start={{ x: 0.2, y: 1.5 }}
                        end={{ x: 1.3, y: 0 }}
                        colors={[DARK_GRADIENT, LIGHT_GRADIENT]}
                        style={styles.container}>
                        {(this.props.users.length > 0 && !this.props.isLoading) ?
                            <FlatList
                                contentContainerStyle={{ padding: 10 }}
                                data={this.props.users}
                                showsVerticalScrollIndicator={false}
                                onEndReached={this.handleLoadMore}
                                onEndReachedThreshold={1}
                                renderItem={({ item, index }) => {
                                    return (
                                        // <Swipeable
                                        //     ref={this.updateRef}
                                        //     friction={2}
                                        //     leftThreshold={30}
                                        //     rightThreshold={40}
                                        //     renderRightActions={progress =>
                                        //         this.renderRightActions(progress, item)
                                        //     }
                                        // >
                                        <SocietyMemberItemLayout item={item} index={index} />
                                        // </Swipeable>
                                    );
                                }}
                                ListFooterComponent={
                                    (this.props.isData) ?
                                        <ActivityIndicator size="small" color={'white'} />
                                        :
                                        null
                                }
                            />
                            :
                            <View style={{ flex: 1, height: '100%', width: '100%', alignItems: 'center', justifyContent: 'center' }}>
                                {(this.props.isLoading) ?
                                    <ActivityIndicator
                                        size={70}
                                        color={LIGHT_GRADIENT}
                                    // animating={this.props.isLoading}
                                    />
                                    :
                                    <CustomTextView
                                        numberOfLines={1}
                                        text={'No members found.'}
                                        style={{ color: 'white', fontSize: 15 }}
                                    />
                                }

                            </View>
                        }

                    </LinearGradient>
                </SafeAreaView>
            </Fragment>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    }
})

const mapStateToProps = state => {
    return {
        users: state.Listing.users,
        isLoading: state.Listing.isLoading,
        isData: state.Listing.isData,
    };
};

export default connect(
    mapStateToProps,
    {
        societyMembersList
    }
)(SocietyMembers);
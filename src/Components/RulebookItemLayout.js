import React, { Component } from "react";
import { View, TouchableOpacity, StyleSheet, Animated } from "react-native";
import CustomTextView from "../Custom/CustomTextView";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";

export default class RulebookItemLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      scaleValue: new Animated.Value(0)
    };
  }
  componentDidMount() {
    Animated.timing(this.state.scaleValue, {
      toValue: 1,
      duration: 600,
      delay: this.props.index * 400
    }).start();
  }

  render() {
    return (
      <Animated.View
        style={[styles.container, { opacity: this.state.scaleValue }]}
      >
        <View style={styles.dotView} />

        <CustomTextView
          style={styles.descriptionStyle}
          text={this.props.item.rule}
        />
      </Animated.View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: "100%",
    backgroundColor: "white",
    padding: 5,
    flexDirection: "row",
    borderRadius: 8,
    marginBottom: 10,
    shadowColor: DARK_GRADIENT,
    shadowOffset: { height: 5, width: 0 },
    shadowOpacity: 0.5,
    shadowRadius: 5,
    elevation: 5
  },
  descriptionStyle: {
    fontSize: 16,
    color: DARK_GRADIENT,
    marginLeft: 8
  },
  dotView: {
    height: 10,
    width: 10,
    borderRadius: 10 / 2,
    backgroundColor: DARK_GRADIENT,
    marginTop: 4
  }
});

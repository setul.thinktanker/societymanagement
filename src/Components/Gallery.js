import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  StatusBar,
  Dimensions,
  TouchableOpacity,
  Image,
  FlatList,
  Platform
} from 'react-native';
import GalleryItemLayout from './GalleryItemLayout';
import Swiper from 'react-native-swiper';
import { connect } from 'react-redux';
// import { ProgressDialog } from 'react-native-simple-dialogs';
// import { changePageGallery, getGalleryData, getCouplePhotos } from '../Actions';
import { getUser } from '../Database/allSchema';
// import { BLESSING_IMAGE } from '../Actions/type';
// let BASE_URL = 'https://scube.net.in/app/wedding_app/uploads/galleryfiles/';
// import ImageLoad from './ImageLoad';

const window = Dimensions.get('window');

const formatData = (data, numColumns) => {
  const numberOfFullRows = Math.floor(data.length / numColumns);
  let numberOfElementsLastRow = data.length - numberOfFullRows * numColumns;
  while (
    numberOfElementsLastRow != numColumns &&
    numberOfElementsLastRow != 0
  ) {
    data.push({ key: 'blank-${numberOfElementsLastRow}', empty: true });
    numberOfElementsLastRow++;
  }
  return data;
};
const numColumns = 2;
let token = '';
let guestId = '';
let clientId = '';
let loginType = '';


class Gallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Gallery: [
        { title: 'Wedding Photoshoot' },
        { title: 'Engagement' },
        { title: 'Haldi' }
      ],
      pageIndex: 0
    };
  }

  componentWillMount() {
    getUser().then((user) => {
      if (user.length > 0) {
        token = user[0].accesstoken;
        guestId = user[0].guestId;
        clientId = user[0].client_id;
        loginType = user[0].login_type;
        this.props.getGalleryData(clientId, guestId, loginType, token);
        this.props.getCouplePhotos(token, guestId, clientId, loginType);
      }
    }).catch((error) => {
      console.log(error);
    });
  }

  onClickButton() {
    if (this.state.pageIndex === 0) {
      this.swiper.scrollBy(1, true);
    }
    if (this.state.pageIndex === 1) {
      this.swiper.scrollBy(-1, true);
    }
    this.setState({ pageIndex: (this.state.pageIndex === 0) ? 1 : 0 });
  }

  onPressRefresh() {
    this.props.getGalleryData(clientId, guestId, loginType, token);
  }

  render() {
    // if (!this.props.isLoading) {
    return (
      <View style={{ flex: 1 }}>
        <StatusBar hidden />
        <TouchableOpacity
          activeOpacity={0.4}
          onPress={() => this.props.navigation.goBack(null)}
          style={styles.RoundStyle}
        >
          <View style={styles.InsideRound}>
            <Image
              style={{ tintColor: '#ffff', zIndex: 11111 }}
              source={require('../img/Arrow.png')}
            />
          </View>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => this.onClickButton()}
          activeOpacity={0.8}
          style={styles.containerStyleBottom1}
        >
          <View style={styles.ButtonStyleContainer1} />
          <View
            style={{
              position: 'absolute',
              alignSelf: 'center',
              zIndex: 112,
              top: 22
            }}
          >
            <Text
              allowFontScaling={false}
              style={{ color: '#fff', fontSize: 15, fontWeight: 'bold' }}
            >
              {(this.state.pageIndex === 1) ? 'Gallery' : 'Guests'}
            </Text>
          </View>
        </TouchableOpacity>

        <Swiper ref={ref => (this.swiper = ref)} style={{
          flex: 1,
          overflow: 'visible'
        }}
          loop={false}
          onIndexChanged={(index) => this.setState({ pageIndex: index })}
          // index={this.state.pageIndex}
          showsPagination={false}
        >
          <View ref='page0' style={{ flex: 1 }}>
            <ImageLoad
              style={{ top: 0, height: '40%', width: '100%' }}
              source={{ uri: BLESSING_IMAGE + this.props.gallery_picture.picUrlGallery1 }}
            />

            <View style={styles.containerStyleBottom}>
              <View style={styles.ButtonStyleContainer} />
            </View>

            <View
              style={{
                position: 'absolute',
                zIndex: 111,
                top: (Platform.Version <= 23) ? 230 : 260,
                width: '100%',

              }}
            >
              <View
                style={{ left: 10, position: 'absolute', top: 10, elevation: 5 }}
              >
                <TouchableOpacity
                  activeOpacity={0.9}
                  // style={{ zIndex: 111111, backgroundColor: 'red', flex: 1, left: 10, position: 'absolute' }}
                  style={styles.Refresh}
                  onPress={() => this.onPressRefresh()}
                >
                  <Image
                    style={{
                      alignSelf: 'center',
                      tintColor: '#fff',
                      height: 16,
                      width: 16
                    }}
                    source={require('../img/refresh-button.png')}
                  />
                </TouchableOpacity>
              </View>

              <Text
                allowFontScaling={false}
                style={{ fontSize: 30, color: '#FF5E4D', alignSelf: 'center', textAlignVertical: 'center', marginTop: 5 }}
              >
                Gallery
                        </Text>


              <View
                style={{ right: 10, position: 'absolute', top: 10, elevation: 5 }}
              >
                <TouchableOpacity
                  activeOpacity={0.9}
                  // style={{ zIndex: 111111, backgroundColor: 'red', flex: 1, left: 10, position: 'absolute' }}
                  style={styles.Refresh}
                  onPress={() => this.onClickButton()}
                >
                  <Image
                    style={{
                      alignSelf: 'center',
                      tintColor: '#fff',
                      height: 16,
                      width: 16
                    }}
                    source={require('../img/right-arrow-forward.png')}
                  />
                </TouchableOpacity>
              </View>

            </View>
            <View
              style={{
                position: 'absolute',
                zIndex: 11111,
                top: 290,
                marginLeft: 5,
                marginRight: 5,
                marginTop: (Platform.Version <= 23) ? 25 : 35,
                height: '50%',
              }}>

              {
                (this.props.galleryList != null && this.props.galleryList.length > 0) ?
                  <FlatList
                    style={{}}
                    data={formatData(this.props.galleryList, numColumns)}
                    showsVerticalScrollIndicator={false}
                    numColumns={numColumns}
                    renderItem={({ item, index }) => {
                      if (item.empty === true) {
                        return <View style={[styles.item, styles.itemInvisible]} />;
                      }
                      let showImage = null;
                      if (item.image_name !== '') {
                        let tempArray = [];
                        for (let i of item.image_name) {
                          if (i.isAdmin === '1') {
                            tempArray.push(i);
                          }
                        }
                        if (tempArray.length > 0) {
                          showImage = { uri: BASE_URL + tempArray[tempArray.length - 1].file_name }
                        } else {
                          showImage = require("../img/img1.jpg");
                        }
                      } else {
                        showImage = require("../img/img1.jpg");
                      }
                      console.log(showImage);
                      return (
                        <GalleryItemLayout
                          title={item.ceremony_name}
                          index={index}
                          imageName={showImage}
                          listSize={formatData(this.props.galleryList, numColumns).length}
                          onClickItem={() =>
                            this.props.navigation.navigate('SubGallery', { imageList: item.image_name, ceremony_name: item.ceremony_name })
                          }
                        />
                      );
                    }}
                  />
                  :
                  <View style={{ height: 500, justifyContent: 'center', alignItems: 'center' }}>
                    <Text style={{ fontSize: 16 }}>Data not found</Text>
                  </View>
              }
            </View>
          </View>

          {/* { Second View   } */}

          <View ref='page1' style={{ flex: 1 }}>
            <ImageLoad
              style={{ top: 0, height: '40%', width: '100%' }}
              source={{ uri: BLESSING_IMAGE + this.props.gallery_picture.picUrlGallery2 }}
            />

            <View style={styles.containerStyleBottom}>
              <View style={styles.ButtonStyleContainer} />
            </View>

            <View
              style={{
                position: 'absolute',
                zIndex: 111,
                top: (Platform.Version <= 23) ? 230 : 260,
                width: '100%',
              }}
            >
              <View
                style={{ left: 10, position: 'absolute', top: 10, elevation: 5 }}
              >
                <TouchableOpacity
                  activeOpacity={0.9}
                  onPress={() => this.onPressRefresh()}
                  style={styles.Refresh}>
                  <Image
                    style={{
                      alignSelf: 'center',
                      tintColor: '#fff',
                      height: 16,
                      width: 16
                    }}
                    source={require('../img/refresh-button.png')}
                  />
                </TouchableOpacity>
              </View>

              <Text
                allowFontScaling={false}
                style={{ fontSize: 30, color: '#FF5E4D', alignSelf: 'center', textAlignVertical: 'center', marginTop: 5 }}
              >
                Guests
                    </Text>

              <View
                style={{
                  height: 55, elevation: 5,
                  width: 55, position: 'absolute', right: 10, top: 10
                }}
              >
                <TouchableOpacity
                  activeOpacity={0.8}
                  style={{
                    height: 55,
                    width: 55,
                    borderRadius: 55 / 2,
                    backgroundColor: '#FF5E4D',
                    alignItems: 'center',
                    justifyContent: 'center',
                  }}
                  onPress={() => {
                    console.warn('click');
                    this.props.navigation.navigate('UploadPhotos')
                  }
                  }
                >
                  <Image
                    style={{
                      alignSelf: 'center',
                      tintColor: '#fff',
                      height: 22,
                      width: 22
                    }}
                    source={require('../img/photo-camera.png')}
                  />
                </TouchableOpacity>
              </View>
            </View>

            <View
              style={{
                position: 'absolute',
                zIndex: 11111,
                top: 290,
                marginLeft: 5,
                marginRight: 5,
                marginTop: (Platform.Version <= 23) ? 25 : 35,
                height: '50%',
              }}>

              <FlatList
                data={formatData(this.props.guestList, numColumns)}
                showsHorizontalScrollIndicator={false}
                numColumns={numColumns}
                renderItem={({ item, index }) => {
                  if (item.empty === true) {
                    return <View style={[styles.item, styles.itemInvisible]} />;
                  }
                  if (item.image_name !== '') {
                    let tempArray = [];
                    for (let i of item.image_name) {
                      if (i.isAdmin === '0') {
                        tempArray.push(i);
                      }
                    }
                    if (tempArray.length > 0) {
                      showImage = { uri: BASE_URL + tempArray[tempArray.length - 1].file_name }
                    } else {
                      showImage = require("../img/img1.jpg");
                    }
                  } else {
                    showImage = require("../img/img1.jpg");
                  }
                  return (
                    <GalleryItemLayout
                      title={item.ceremony_name}
                      index={index}
                      image_name={item.image_name.length - 1}
                      imageName={showImage}
                      listSize={formatData(this.props.guestList, numColumns).length}
                      onClickItem={() => {
                        console.warn('click');
                        this.props.navigation.navigate('SubGalleryGuest', { imageList: item.image_name, ceremony_name: item.ceremony_name })
                      }
                      }
                    />
                  );
                }}
              />
            </View>

            {/* <TouchableOpacity
            style={styles.containerStyleBottom1}>
              <View style={styles.ButtonStyleContainer1}>

              </View>
                <View style={{position: 'absolute' , alignSelf: 'center' ,zIndex:112 , top: 22 }}>
                    <Text
                    allowFontScaling={false}
                    style={{color:'#fff' , fontSize: 15 , fontWeight: 'bold'}}>Gallery</Text>
                </View>
            </TouchableOpacity> */}
          </View>

          {/* <TouchableOpacity
            style={styles.containerStyleBottom1}>
              <View style={styles.ButtonStyleContainer1}>

              </View>
                <View style={{position: 'absolute' , alignSelf: 'center', zIndex:112 , top: 22 }}>
                    <Text
                    allowFontScaling={false}
                    style={{color:'#fff' , fontSize: 15 , fontWeight: 'bold'}}>Guests</Text>
                </View>
            </TouchableOpacity> */}
        </Swiper>
        <ProgressDialog
          visible={this.props.isLoading}
          title="Loading..."
          message='Please wait.'
        />
      </View>
    );
  }
  //   return null;
  // }
}
const styles = StyleSheet.create({
  containerStyleBottom: {
    alignSelf: 'center',
    backgroundColor: 'transparent',
    position: 'absolute',
    zIndex: 111,
    bottom: 0,
    height: '67%',
    width: window.width,
    overflow: 'hidden'
  },
  ButtonStyleContainer: {
    borderTopLeftRadius: window.width,
    borderTopRightRadius: window.width,
    width: window.width,
    height: '100%',
    backgroundColor: '#F8F8F8',
    transform: [{ scaleX: 2 }],
    zIndex: 1
  },

  containerStyleBottom1: {
    alignSelf: 'center',
    backgroundColor: 'transparent',
    position: 'absolute',
    zIndex: 111,
    bottom: 0,
    height: '10%',
    overflow: 'hidden',
    width: '100%'
  },
  ButtonStyleContainer1: {
    borderTopLeftRadius: window.width,
    borderTopRightRadius: window.width,
    width: window.width,
    height: window.width - 110,

    backgroundColor: '#FF5E4D',
    transform: [{ scaleX: 2 }],
    zIndex: 1
  },
  marker: {
    height: 100,
    width: '100%',
    borderRadius: 6,
    position: 'absolute',
    top: 0,
    left: 0,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgba(50, 50, 50, 0.6)'
  },
  imageTile: {
    height: 100,
    width: '100%',
    borderRadius: 6
  },
  RoundStyle: {
    position: 'absolute',
    zIndex: 1111,
    height: 50,
    width: 50,
    borderRadius: 50 / 2,
    margin: 20,
    backgroundColor: 'rgba(51, 51, 51,0.7)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  InsideRound: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 40,
    width: 40,
    borderRadius: 40 / 2,
    backgroundColor: 'rgba(51, 51,51,0.5)'
  },
  TextStyle: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold'
  },
  Refresh: {
    height: 55,
    width: 55,
    borderRadius: 55 / 2,
    backgroundColor: '#FF5E4D',
    alignItems: 'center',
    justifyContent: 'center'
  },
  item: {
    // alignItems: 'center',
    // justifyContent: 'center',
    // flex: 1
  },
  itemInvisible: {
    backgroundColor: 'transparent'
  }
});

const mapStateToProps = state => {
  return {
    isFirstPage: state.galleryReducer.isFirstPage,
    isLoading: state.galleryReducer.isLoading,
    galleryList: state.galleryReducer.galleryList,
    guestList: state.galleryReducer.guestList,
    authResult: state.galleryReducer.authResult,
    gallery_picture: state.couplephotoreducer.gallery_picture,
  };
};

export default connect(
  mapStateToProps,
  {
    changePageGallery, getGalleryData,
    getCouplePhotos
  }
)(Gallery);

import React, { Component, Fragment } from "react";
import {
  View,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  SafeAreaView
} from "react-native";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";
import Header from "./Header";
import ComplaintsItemLayout from "./ComplaintsItemLayout";

export default class Complaints extends Component {
  constructor(props) {
    super(props);
    this.state = {
      complaints: [
        {
          Subject: "Parking",
          description:
            "Some Members are parking 2 cars in 1 Parking lot because of extended space (the cars protude out of the parking areas). Can a Stilt Car Park Owner park more than One Car in the Parking Lot?",
          images: {
            uri: [
              {
                filename: "IMG_4065.JPG",
                path:
                  "/private/var/mobile/Containers/Data/Application/386A4495-9A57-4DCE-B423-7A670CCC8906//react-native-image-crop-picker/F3E30F7B-4895-4903-A0AE-5D7B4A3D676D.jpg"
              }
            ]
          },
          video:
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerBlazes.mp4"
        },
        {
          Subject: "Uncleaned garbage",
          description:
            "The garbage man does not clean them regularly. As a result they remain stuffed and stinking for days.it creates unnecessary litter in the colony at all times.",
          images: {
            uri: [
              {
                filename: "IMG_4065.JPG",
                path:
                  "/private/var/mobile/Containers/Data/Application/386A4495-9A57-4DCE-B423-7A670CCC8906/tmp/react-native-image-crop-picker/F3E30F7B-4895-4903-A0AE-5D7B4A3D676D.jpg"
              },
              {
                filename: "IMG_4075.JPG",
                path:
                  "/private/var/mobile/Containers/Data/Application/386A4495-9A57-4DCE-B423-7A670CCC8906/tmp/react-native-image-crop-picker/D2E8CE49-FDAF-4A23-8DD1-8EE5D0EFDA66.jpg"
              }
            ]
          },
          video:
            "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerMeltdowns.mp4"
        }
      ]
    };
  }
  onBackPress() {
    this.props.navigation.goBack();
  }
  render() {
    return (
      <Fragment>
        <SafeAreaView style={{ flex: 0, backgroundColor: DARK_GRADIENT }} />
        <SafeAreaView style={{ flex: 1 }}>
          <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content" />
          <View style={{ height: 50 }}>
            <Header
              headerText={"My Complaints"}
              leftImage={require("../img/left-arrow.png")}
              onBackPress={() => this.onBackPress()}
            />
          </View>
          <FlatList
            contentContainerStyle={{ padding: 10, paddingBottom: 80 }}
            data={this.state.complaints}
            showsVerticalScrollIndicator={false}
            renderItem={({ item, index }) => {
              return (
                <ComplaintsItemLayout
                  item={item}
                  onPressVideo={() =>
                    this.props.navigation.navigate("PlayVideo", {
                      uri: item.video
                    })
                  }
                  index={index}
                />
              );
            }}
          />
          <LinearGradient
            start={{ x: 0, y: 0.5 }}
            end={{ x: 1.5, y: 0 }}
            colors={[DARK_GRADIENT, DARK_GRADIENT]}
            style={styles.bottomBtn}
          >
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate("AddComplaint")}
              style={styles.btn}
            >
              <Image
                source={require("../img/add.png")}
                style={{
                  height: 30,
                  width: 30,
                  tintColor: "white",
                  alignSelf: "center"
                }}
              />
            </TouchableOpacity>
          </LinearGradient>
        </SafeAreaView>
      </Fragment>
    );
  }
}
const styles = StyleSheet.create({
  bottomBtn: {
    flex: 1,
    height: 60,
    width: 60,
    borderRadius: 60 / 2,
    position: "absolute",
    bottom: "2%",
    right: "4%",
    alignItems: "center",
    justifyContent: "center"
  },
  btn: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: "center"
  }
});

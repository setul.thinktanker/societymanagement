import React, { Component, Fragment } from "react";
import {
  View,
  StatusBar,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  Animated
} from "react-native";
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import LinearGradient from "react-native-linear-gradient";
import Header from "./Header";
import CustomTextView from "../Custom/CustomTextView";
import AddVehicleItemLayout from "./AddVehicleItemLayout";
import { RectButton } from "react-native-gesture-handler";
import Swipeable from "react-native-gesture-handler/Swipeable";
import Toast, { DURATION } from "react-native-easy-toast";
import { connect } from "react-redux";
import { vehicleDetailsList, addVehicle, deleteVehicle, initialVehicleData } from "../Actions";
import { getUser } from "../Database/allSchema";
import AsyncStorage from '@react-native-community/async-storage';
let authToken = '';
let userId = '';
let societyId = '';

class AddVehicle extends Component {
  constructor (props) {
    super(props);
    this.state = {
      pageCount: 1,
      myVehiclesData: [
        {
          id: 1,
          name: "User Name",
          mobileNo: "9999999999",
          houseNo: "A-101",
          vehicleType: "Car",
          VNo: "GJ272113",
          VImg: require("../img/motorcycle.png")
        },
        {
          id: 2,
          name: "Setul soni",
          mobileNo: "1234567890",
          houseNo: "A-101",
          vehicleType: "Cycle",
          VNo: "GJ272113",
          VImg: require("../img/car-trip.png")
        }
      ],
      vehicalForm: []
    };
  }

  storeToken = () => {
    try {
      AsyncStorage.getItem('@authToken_Key')
        .then((token) => {
          console.log('token', token)
          authToken = token
          getUser().then((user) => {
            userId = user[0].id;
            societyId = user[0].societyId
            this.props.vehicleDetailsList({
              userId,
              societyId,
              token: authToken,
              pageCount: this.state.pageCount,
            })
          }).catch((err) => {
            console.log('err', err)
          });
        })
        .catch((err) => {
          console.log('err', err)
        })
    } catch (e) {
      console.log('Error in store authToken');
    }
  }

  componentWillMount() {
    this.props.initialVehicleData()
    this.storeToken()
    console.log('vehicles', this.props.myVehiclesData)
    this.setState({ myVehiclesData: this.props.myVehiclesData })
  }
  componentWillReceiveProps(nextProps) {
    console.log('nextProps', nextProps)

    this.setState({ myVehiclesData: nextProps.myVehiclesData })

    if (nextProps.authResult == 'Delete Success') {
      this.refs.toast.show(nextProps.msg, 2500);
    }
    if (nextProps.authResult == 'Delete Failed') {
      this.refs.toast.show(nextProps.msg, 2500);
    }
    if (nextProps.authResult == 'Delete Error') {
      this.refs.toast.show(nextProps.msg, 2500);
    }
  }



  renderRightActionToDelete = (color, x, progress, index, id) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0]
    });
    return (
      <Animated.View
        style={ [
          styles.animateViewStyle,
          { transform: [{ translateX: trans }] }
        ] }
      >
        <RectButton
          style={ [styles.rightAction, { backgroundColor: color }] }
          onPress={ () => this.deleteVehicle(index, id) }
        >
          <Image
            style={ [styles.actionImg, { tintColor: "white" }] }
            source={ require("../img/delete.png") }
          />
        </RectButton>
      </Animated.View>
    );
  };
  renderRightActions(progress, index, id) {
    // console.log('id', id)
    return (
      <View style={ { width: 100, flexDirection: "row" } }>
        { this.renderRightActionToDelete("#C13D2D", 100, progress, index, id) }
      </View>
    );
  }
  updateRef = ref => {
    this._swipeableRow = ref;
  };
  close = () => {
    this._swipeableRow.close();
  };
  onBackPress() {
    this.props.navigation.goBack();
  }
  addNewVehicle() {
    // console.warn("addnew", this.state.myVehiclesData[0].name);
    let temp = {
      // name: this.state.myVehiclesData[0].name,
      // mobile_no: this.state.myVehiclesData[0].mobile_no,
      // block: this.state.myVehiclesData[0].block,
      vehicle_type: "",
      // society_id: this.state.myVehiclesData[0].society_id,
      // user_id: this.state.myVehiclesData[0].user_id,
      vehicle_number: "",
    };
    let tempArray = this.state.myVehiclesData;

    tempArray.push(temp);
    this.setState({ myVehiclesData: tempArray }, () => {
      console.log('tempArray', tempArray);
      // console.log('temp', temp);
    });
  }

  deleteVehicle(index, id) {
    console.log("index", index);
    console.log("id", id);
    console.log('authToken', authToken);
    //api call for remove vehicle
    if (id) {
      this.props.deleteVehicle({ vehicleId: id, token: authToken, userId, societyId, pageCount: this.state.pageCount, list: this.state.myVehiclesData })
    }

    let tempArray = this.state.myVehiclesData;
    tempArray.splice(index, 1);
    console.warn("tempArray", tempArray);
    this.setState({ myVehiclesData: tempArray });

  }

  onPressSave() {
    let array = this.state.myVehiclesData;
    console.log('array', array)

    let isValid = true;
    array.map(data => {
      Object.entries(data).map(([key, value]) => {
        if (value == null || value == "") {
          isValid = false;
          this.refs.toast.show("Please enter all fields", 1500);
        }
      });
    });
    if (isValid) {
      console.log('array', array)

      //api call
      this.props.addVehicle({
        vehicleData: array,
        societyId,
        userId,
        token: authToken
      })
      // this.props.navigation.navigate("ProfileDetails");
    }
  }

  render() {
    return (
      <Fragment>
        <SafeAreaView style={ { flex: 0, backgroundColor: DARK_GRADIENT } } />
        <SafeAreaView style={ { flex: 1 } }>
          <View style={ { height: 50 } }>
            <Header
              headerText={ "Add Vehicle" }
              leftImage={ require("../img/left-arrow.png") }
              onBackPress={ () => this.onBackPress() }
            // rightImage = {require('../img/add.png')}
            // onRightPress = {()=> this.addNewVehicle()}
            />
          </View>
          <Toast
            ref="toast"
            style={ { backgroundColor: "#1A2980" } }
            opacity={ 0.9 }
          />
          <ScrollView
            style={ { flex: 1 } }
            contentContainerStyle={ { paddingBottom: 70 } }
            showsVerticalScrollIndicator={ false }
          >
            <FlatList
              style={ { flex: 1 } }
              contentContainerStyle={ { paddingTop: 10, paddingHorizontal: 5 } }
              data={ this.state.myVehiclesData }
              extraData={ this.state }
              showsVerticalScrollIndicator={ false }
              renderItem={ ({ item, index }) => {
                return (
                  <Swipeable
                    ref={ this.updateRef }
                    friction={ 2 }
                    leftThreshold={ 30 }
                    rightThreshold={ 40 }
                    renderRightActions={ progress =>
                      this.renderRightActions(progress, index, item.id)
                    }
                  >
                    <AddVehicleItemLayout
                      item={ item }
                      index={ index }
                      onChangeUserName={ text => {
                        let tempList = this.state.myVehiclesData;
                        tempList[index] = { ...tempList[index], name: text };
                        this.setState({ myVehiclesData: tempList });
                      } }
                      onChangeVehicleData={ text => {
                        let tempList = this.state.myVehiclesData;
                        tempList[index] = { ...tempList[index], vehicle_number: text };
                        this.setState({ myVehiclesData: tempList });
                      } }
                      onChangeMobileNo={ text => {
                        let tempList = this.state.myVehiclesData;
                        tempList[index] = {
                          ...tempList[index],
                          mobile_no: text
                        };
                        this.setState({ myVehiclesData: tempList });
                      } }
                      onChangeVehicleType={ (
                        itemValue,
                        itemIndex,
                        vehicleType
                      ) => {
                        console.warn(vehicleType);
                        if (itemValue != 0) {
                          let tempList = this.state.myVehiclesData;
                          tempList[index] = {
                            ...tempList[index],
                            vehicle_type: vehicleType[itemIndex - 1].name
                          };
                          this.setState({ myVehiclesData: tempList });
                        }
                      } }
                      onSelectVehicleType={ item.vehicle_type }
                      text={ item.vehicle_type }
                    />
                  </Swipeable>
                );
              } }
            />

            <View style={ styles.addBtnView }>
              <TouchableOpacity
                onPress={ () => this.addNewVehicle() }
                style={ styles.addBtn }
              >
                <Image
                  source={ require("../img/add.png") }
                  style={ {
                    height: 30,
                    width: 30,
                    tintColor: "white",
                    alignSelf: "center"
                  } }
                />
              </TouchableOpacity>
            </View>
          </ScrollView>

          <TouchableOpacity
            style={ {
              width: "100%",
              height: 60,
              backgroundColor: DARK_GRADIENT
            } }
          >
            <TouchableOpacity
              onPress={ () => {
                this.onPressSave();
              } }
              style={ {
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              } }
            >
              <CustomTextView
                text="Save"
                style={ { color: "white", fontSize: 18, textAlign: "center" } }
              />
            </TouchableOpacity>
          </TouchableOpacity>

          {/* <LinearGradient
          start={{x: 0, y: 0.5}}
          end={{x: 1.5, y: 0}}
          colors={[DARK_GRADIENT, DARK_GRADIENT]}
          style = {styles.bottomBtn}>
          <TouchableOpacity 
            onPress = {()=> this.addNewVehicle()}
            style = {styles.btn}
          > 
            <Image 
              source = {require('../img/add.png')}
              style = {{height:30,width:30,tintColor:'white',alignSelf:'center'}}
            />
          </TouchableOpacity>
      </LinearGradient> */}
        </SafeAreaView>
      </Fragment>
    );
  }
}
const styles = StyleSheet.create({
  bottomBtn: {
    flex: 1,
    height: 60,
    width: 60,
    borderRadius: 60 / 2,
    position: "absolute",
    bottom: "2%",
    right: "4%",
    alignItems: "center",
    justifyContent: "center"
  },
  btn: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: "center"
  },
  rightAction: {
    alignItems: "center",
    flex: 1,
    borderRadius: 8,
    marginHorizontal: 2,
    justifyContent: "center"
  },
  actionImg: {
    height: 40,
    width: 40
  },
  bottomBtn: {
    flex: 1,
    height: 60,
    width: 60,
    borderRadius: 60 / 2,
    position: "absolute",
    bottom: "2%",
    right: "4%",
    alignItems: "center",
    justifyContent: "center"
  },
  btn: {
    flex: 1,
    width: "100%",
    height: "100%",
    justifyContent: "center"
  },
  animateViewStyle: {
    flex: 1,
    marginBottom: 10
  },
  ightAction: {
    alignItems: "center",
    flex: 1,
    borderRadius: 8,
    marginHorizontal: 2,
    justifyContent: "center"
  },
  actionImg: {
    height: 40,
    width: 40
  },
  addBtnView: {
    width: "100%",
    paddingHorizontal: 10,
    marginVertical: 5,
    height: 40
  },
  addBtn: {
    flex: 1,
    backgroundColor: "rgba(30, 118, 161, 1)",
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center"
  }
});

const mapStateToProps = state => {
  return {
    myVehiclesData: state.Vehicles.vehicles,
    isLoading: state.Vehicles.isLoading,
    isLoadingDelete: state.Vehicles.isLoading,
    authResult: state.Vehicles.authResult,
    msg: state.Vehicles.msg,
  };
};

export default connect(
  mapStateToProps,
  {
    vehicleDetailsList,
    addVehicle,
    deleteVehicle,
    initialVehicleData
  }
)(AddVehicle);

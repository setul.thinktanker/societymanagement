import React, {Component, Fragment} from "react";
import {
    View,
    StatusBar,
    FlatList,
    Animated,
    StyleSheet,
    Image,
    Linking,
    TouchableOpacity,
    SafeAreaView, ActivityIndicator
} from "react-native";

import {DARK_GRADIENT, LIGHT_GRADIENT} from "../Commons/Colors";
import VendorDetailsItemLayout from "./VendorDetailsItemLayout";
import LinearGradient from "react-native-linear-gradient";
import Header from "./Header";
import Swipeable from "react-native-gesture-handler/Swipeable";
import CustomTextView from "../Custom/CustomTextView";
import {RectButton} from "react-native-gesture-handler";
import AsyncStorage from "@react-native-community/async-storage";
import {getUser} from "../Database/allSchema";
import {connect} from "react-redux";
import {getVendors} from "../Actions";

let authToken = '';
let userId = '';
let societyId = '';

//all vendors display screen
class VenderDetails extends Component {
    constructor(props) {
        super(props);
        this.state = {
            // venderData: [
            //     {
            //         name: "Mr. Name",
            //         inSociety: false,
            //         profession: "Carpenter",
            //         mobile_no: "7645992344",
            //         address: "a-101,apartment name,Near hsdsadsx asxaxax bmnlaksl wsxamdjdsdsvsdcfkls",
            //         services: "Carpenters maintain and repair interior and exterior wood components such as window frames, doors and frames, walls, floors, decorative molding, etc. Carpentry also repairs and replaces drop-ceiling systems."
            //     },
            //     {
            //         name: "Mr. Name",
            //         inSociety: true,
            //         profession: "Plumber",
            //         mobile_no: "8767266233",
            //         address: "a-102,apartment name,Near hsdsadsx asxaxax bmnlaksl wsxamdjdsdsvsdcfkls",
            //         services: "Leak Repair. One of the primary services offered by any plumber is repairing plumbing leaks."
            //     }
            // ]
            venderData: []
        };
    }

    getToken() {
        try {
            AsyncStorage.getItem('@authToken_Key')
                .then((token) => {
                    console.log('token', token);
                    authToken = token;
                    getUser().then((user) => {
                        console.log("User data:" + JSON.stringify(user));
                        societyId = user[0].societyId;
                        userId = user[0].id;

                        // console.log('AsyncStorage societyId:', user[0].societyId);
                        // console.log('AsyncStorage userId:', user[0].id);

                        this.props.getVendors(societyId, "list", userId, authToken, false)

                    }).catch((err) => {
                        console.log('err', err)
                    });
                })
                .catch((err) => {
                    console.log('err', err)
                })
        } catch (e) {
            console.log('Error in store authToken');
        }
    }

    componentWillReceiveProps(nextProps, nextContext) {
        console.log("componentWillReceiveProps");
        console.log("", nextProps.isVendorLoading);
        // console.log("componentWillReceiveProps:", nextProps.venderList.data);
        // console.log("response :" + JSON.stringify(nextProps));
        // console.log("success :" + nextProps.success);

        if (nextProps.success) {
            this.state.venderData = [];
            this.state.venderData = nextProps.venderList.data;
            console.log("success");
            // this.state.venderData.concat(nextProps.response.data);
            // this.state.venderData = nextProps.response.data;
            // this.state.venderData = [];
            // console.log("Response data response data :" + nextProps.response.data);
            // console.log("Response data vendor:" + this.state.venderData)
        } else {
            console.log("failed")
        }
    }

    componentWillMount() {
        console.log("componentWillMount");
        this.getToken();
    }

    onBackPress() {
        this.props.navigation.goBack();
    }

    deleteVender(index) {
        //  let tempArray = this.state.venderData;
        //   tempArray.splice(index,1);
        //   console.warn('tempArray',tempArray);
        //   this.setState({ venderData:tempArray })
    }

    renderRightActionCall = (text, color, x, progress, item) => {
        const trans = progress.interpolate({
            inputRange: [0, 1],
            outputRange: [x, 0]
        });
        return (
            <Animated.View
                style={[
                    styles.animateViewStyle,
                    {transform: [{translateX: trans}]}
                ]}
            >
                <RectButton
                    style={[styles.rightAction, {backgroundColor: color}]}
                    onPress={() => {
                        Linking.openURL(`tel:${item.mobileNo}`);
                    }}
                >
                    <Image
                        style={[styles.actionImg, {tintColor: "white"}]}
                        source={require("../img/call.png")}
                    />
                </RectButton>
            </Animated.View>
        );
    };

    renderRightActions(progress, item, index) {
        return (
            <View style={{width: 100, flexDirection: "row"}}>
                {this.renderRightActionCall("Call", "#2DC170", 100, progress, item)}
            </View>
        );
    }

    render() {
        return (
            <Fragment>
                <SafeAreaView style={{flex: 0, backgroundColor: DARK_GRADIENT}}/>
                <SafeAreaView style={{flex: 1}}>
                    <StatusBar backgroundColor={DARK_GRADIENT} barStyle="light-content"/>
                    <View style={{height: 50}}>
                        <Header
                            headerText={"Vender Details"}
                            leftImage={require("../img/left-arrow.png")}
                            onBackPress={() => this.onBackPress()}
                            rightImage={require('../img/add.png')}
                            onRightPress={() => this.props.navigation.navigate("AddVender")}
                        />
                    </View>
                    {
                        this.state.venderData.length > 0 ? <FlatList
                            // style = {{ padding:10 }}
                            contentContainerStyle={{padding: 10, paddingBottom: 80}}
                            data={this.state.venderData}
                            showsVerticalScrollIndicator={false}
                            renderItem={({item, index}) => {
                                return (
                                    <Swipeable
                                        ref={this.updateRef}
                                        friction={2}
                                        leftThreshold={30}
                                        rightThreshold={40}
                                        renderRightActions={progress =>
                                            this.renderRightActions(progress, item, index)
                                        }>
                                        <VendorDetailsItemLayout item={item} index={index}/>
                                    </Swipeable>
                                );
                            }}
                        /> : <View style={{
                            flex: 1,
                            height: '100%',
                            width: '100%',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                            {
                                (this.props.isLoading) ?
                                    <ActivityIndicator
                                        size={70}
                                        color={LIGHT_GRADIENT}
                                        // animating={this.props.isLoading}
                                    />
                                    :
                                    <CustomTextView
                                        numberOfLines={1}
                                        text={'No venders found.'}
                                        style={{color: 'white', fontSize: 15}}
                                    />
                            }

                        </View>
                    }

                    {/*<LinearGradient*/}
                    {/*    start={{x: 0, y: 0.5}}*/}
                    {/*    end={{x: 1.5, y: 0}}*/}
                    {/*    colors={[DARK_GRADIENT, DARK_GRADIENT]}*/}
                    {/*    style={styles.bottomBtn}>*/}
                    {/*    <TouchableOpacity*/}
                    {/*        onPress={() => this.props.navigation.navigate("AddVender")}*/}
                    {/*        style={styles.btn}>*/}
                    {/*        <Image*/}
                    {/*            source={require("../img/add.png")}*/}
                    {/*            style={{*/}
                    {/*                height: 30,*/}
                    {/*                width: 30,*/}
                    {/*                tintColor: "white",*/}
                    {/*                alignSelf: "center"*/}
                    {/*            }}*/}
                    {/*        />*/}
                    {/*    </TouchableOpacity>*/}
                    {/*</LinearGradient>*/}
                </SafeAreaView>
            </Fragment>
        );
    }

    onItemClicked(item, index) {
        console.warn("Clicked: " + JSON.stringify(item));
        console.log("Clicked");
    }
}


const mapStateToProps = state => {
    console.log("mapStateToProps" + JSON.stringify(state));

    return {
        isVendorLoading: state.Vendor.vendorListLoading,
        success: state.Vendor.success,
        msg: state.Vendor.msg,
        response: state.Vendor.response,
        venderList: state.Vendor.venderList,
    }
};

const styles = StyleSheet.create({
    animateViewStyle: {
        flex: 1,
        marginBottom: 5,
        marginTop: 10
    },
    actionText: {
        color: "white",
        fontSize: 20,
        backgroundColor: "transparent",
        padding: 10
    },
    rightAction: {
        alignItems: "center",
        flex: 1,
        borderRadius: 8,
        marginLeft: 5,
        justifyContent: "center"
    },
    actionImg: {
        height: 40,
        width: 40
    },
    bottomBtn: {
        flex: 1,
        height: 60,
        width: 60,
        borderRadius: 60 / 2,
        position: "absolute",
        bottom: "2%",
        right: "4%",
        alignItems: "center",
        justifyContent: "center"
    },
    btn: {
        flex: 1,
        width: "100%",
        height: "100%",
        justifyContent: "center"
    }
});

export default connect(mapStateToProps, {getVendors})(VenderDetails);

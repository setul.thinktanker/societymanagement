import React, { Component } from "react";
import {
  View,
  FlatList,
  Text,
  Animated,
  StyleSheet,
  Image
} from "react-native";
import VehiclesDetailsItemLayout from "./VehiclesDetailsItemLayout";
import Swipeable from "react-native-gesture-handler/Swipeable";
import { RectButton } from "react-native-gesture-handler";

export default class SocietyVehicles extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vehiclesData: [
        {
          name: "Setul soni",
          mobileNo: "1234567890",
          houseNo: "A-101",
          VType: "Car",
          VNo: "GJ898989",
          VImg: require("../img/car-trip.png")
        },
        {
          name: "User Name",
          mobileNo: "9999999999",
          houseNo: "A-101",
          VType: "Car",
          VNo: "GJ272113",
          VImg: require("../img/motorcycle.png")
        },
        {
          name: "User Name",
          mobileNo: "9999999999",
          houseNo: "A-101",
          VType: "Car",
          VNo: "GJ272113",
          VImg: require("../img/bike.png")
        },
        {
          name: "User Name",
          mobileNo: "9999999999",
          houseNo: "A-101",
          VType: "Car",
          VNo: "GJ272113",
          VImg: require("../img/motorcycle.png")
        },
        {
          name: "Setul soni",
          mobileNo: "1234567890",
          houseNo: "A-101",
          VType: "Car",
          VNo: "GJ272113",
          VImg: require("../img/car-trip.png")
        }
      ]
    };
  }

  renderRightActionCall = (text, color, x, progress, item) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0]
    });
    return (
      <Animated.View
        style={[
          styles.animateViewStyle,
          { transform: [{ translateX: trans }] }
        ]}
      >
        <RectButton
          style={[styles.rightAction, { backgroundColor: color }]}
          onPress={() => {
            Linking.openURL(`tel:${item.mobileNo}`);
          }}
        >
          <Image
            style={[styles.actionImg, { tintColor: "white" }]}
            source={require("../img/call.png")}
          />
        </RectButton>
      </Animated.View>
    );
  };

  renderRightActionSms = (text, color, x, progress, item) => {
    const trans = progress.interpolate({
      inputRange: [0, 1],
      outputRange: [x, 0]
    });
    return (
      <Animated.View
        style={[
          styles.animateViewStyle,
          { transform: [{ translateX: trans }] }
        ]}
      >
        <RectButton
          style={[styles.rightAction, { backgroundColor: color }]}
          onPress={() => {
            Linking.openURL(`sms:${item.mobileNo}`);
          }}
        >
          <Image
            style={[styles.actionImg, { tintColor: "white" }]}
            source={require("../img/sms.png")}
          />
        </RectButton>
      </Animated.View>
    );
  };

  renderRightActions(progress, item) {
    return (
      <View style={{ width: 192, flexDirection: "row" }}>
        {this.renderRightActionCall("Call", "#2DC170", 192, progress, item)}
        {this.renderRightActionSms("Message", "#ffab00", 128, progress, item)}
      </View>
    );
  }
  updateRef = ref => {
    this._swipeableRow = ref;
  };
  close = () => {
    this._swipeableRow.close();
  };
  onBackPress() {
    this.props.navigation.goBack();
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <FlatList
          // style = {{ padding:10 }}
          contentContainerStyle={{ padding: 10 }}
          data={this.state.vehiclesData}
          showsVerticalScrollIndicator={false}
          renderItem={({ item, index }) => {
            return (
              <Swipeable
                ref={this.updateRef}
                friction={2}
                leftThreshold={30}
                rightThreshold={40}
                renderRightActions={progress =>
                  this.renderRightActions(progress, item)
                }
              >
                <VehiclesDetailsItemLayout item={item} index={index} />
              </Swipeable>
            );
          }}
        />
      </View>
    );
  }
}
const styles = StyleSheet.create({
  animateViewStyle: {
    flex: 1,
    marginBottom: 10
  },
  actionText: {
    color: "white",
    fontSize: 20,
    backgroundColor: "transparent",
    padding: 10
  },
  rightAction: {
    alignItems: "center",
    flex: 1,
    borderRadius: 8,
    marginHorizontal: 2,
    justifyContent: "center"
  },
  actionImg: {
    height: 40,
    width: 40
  }
});

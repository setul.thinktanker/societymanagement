import React, { Component, Fragment } from "react";
import {
  View,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
  Image,
  ImageBackground,
  SafeAreaView,
  TextInput,
  ScrollView
} from "react-native";
import { connect } from "react-redux";
import { editUserProfile } from "../Actions";
import AsyncStorage from '@react-native-community/async-storage';
import { DARK_GRADIENT, LIGHT_GRADIENT } from "../Commons/Colors";
import CustomTextView from "../Custom/CustomTextView";
import CustomDropDown from "../Custom/CustomDropDown";
import Header from "./Header";
import Toast, { DURATION } from "react-native-easy-toast";
import ImagePicker from "react-native-image-crop-picker";
import { getUser } from "../Database/allSchema";
let userId = '';

class Profile extends Component {
  constructor (props) {
    super(props);
    this.state = {
      gender: [{ name: "Female" }, { name: "Male" }],
      isRented: 0,
      radioBtnsData: ["Yes", "No"],
      houseType: [
        {
          "name": "flate",
          "blocklist": [
            {
              "name": "A",
              "house_no": [
                101,
                102,
                103
              ]
            },
            {
              "name": "B",
              "house_no": [
                101,
                102,
                103,
                104,
                105,
                106,
                107,
                108,
                109,
                110,
                111,
                112,
                113,
                114,
                115
              ]
            }
          ]
        },
        {
          "name": "rowhouse",
          "blocklist": [
            {
              "house_no": [
                101,
                102,
                103
              ]
            }
          ]
        },
        {
          "name": "duplex",
          "blocklist": [
            {
              "house_no": [
                101,
                102
              ]
            }
          ]
        },
        {
          "name": "tenament",
          "blocklist": [
            {
              "house_no": [
                101,
                102,
                103,
                104,
                105
              ]
            }
          ]
        },
        {
          "name": "bunglows",
          "blocklist": [
            {
              "house_no": [
                101,
                102,
                103,
                104,
                105
              ]
            }
          ]
        }
      ],
      blockType: [],
      blockNoType: [],
      profileImg: null
    };
    this.getData();
  }

  getData() {
    //get userData
    getUser().then((user) => {
      console.log('user', user[0].id)
      userId = user[0].id;
      if (user && user.length > 0) {
        let userName = user[0].name
        let name = userName.split(" ")
        this.setState({
          fname: (user[0].name == null) ? '' : name[0],
          lname: (user[0].name == null) ? '' : name[1],
          mobileNo: (user[0].mobileNo == null) ? '' : user[0].mobileNo,
          email: (user[0].email == null) ? '' : user[0].email,
          selectedGender: (user[0].gender == null) ? '' : user[0].gender,
          selectedHouseType: (user[0].houseType == null) ? '' : user[0].houseType,
          selectedBlock: (user[0].block == null) ? '' : user[0].block,
          selectedBlockNo: (user[0].blockNo == null) ? '' : user[0].blockNo,
          isOwnerStayInSociety: (user[0].isOwnerStayInSociety == null) ? '' : user[0].isOwnerStayInSociety,
          currentAddress: (user[0].currentAddress == null) ? '' : user[0].currentAddress,
          isRented: (user[0].isRented == null) ? '' : (user[0].isRented == 'Yes') ? 0 : 1,
          image: (user[0].image == null) ? '' : user[0].image,
        }, () => {
          this.setArrValues()
        })
      }
    }).catch((err) => {
      console.log('err', err)
    });
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.authResult == 'Edit_profile successfully') {
      console.log('nextProps.msg', nextProps.msg)
      this.refs.toast.show(nextProps.msg, 2000);
    }
    if (nextProps.authResult == 'Edit_profile Failed') {
      console.log('nextProps.msg', nextProps.msg)
      this.refs.toast.show('Somthing wents wrong please try again later.', 2000);
    }
    if (nextProps.authResult == 'Edit_profile Error') {
      Alert.alert('Oops', 'Somthing wents wrong please try again later.')
    }
  }

  componentWillMount() { }
  setArrValues = () => {
    let { selectedHouseType, selectedBlock, selectedBlockNo } = this.state;
    let tempHouse = this.props.houseType;
    // let list = tempHouse.map((item) => ({ name: item.name, blocklist: item.blocklist }));

    tempHouse.forEach(element => {
      //when element match the selected 
      if (element.name == selectedHouseType) {
        let data = element.blocklist[0]
        //if block type exist
        if (Object.keys(data)[0] == 'name') {
          //set house type & block no
          this.setState({
            selectedHouseType: element.name,
            blockType: element.blocklist,
            selectedBlock: selectedBlock,
            selectedBlockNo: selectedBlockNo
          }, () => {
            this.state.blockType.forEach(element => {
              console.log('element', element);
              if (element.name == selectedBlock) {
                let tempData = element.house_no;
                let temp = [];
                for (let index = 0; index < tempData.length; index++) {
                  let obj = {};
                  obj.name = tempData[index].toString();
                  temp.push(obj);
                }
                this.setState({ blockNoType: temp }, () => {
                  console.log('blockNoType', this.state.blockNoType)
                })
              }
            });

          });
        } else {
          //set house no & house type
          console.log('element', element)
          console.log('element', element.house_no)
          let tempData = data[0].house_no;
          let temp = [];
          for (let index = 0; index < tempData.length; index++) {
            let obj = {};
            obj.name = tempData[index].toString();
            temp.push(obj);
          }
          this.setState({
            blockNoType: temp,
            selectedBlockNo: selectedBlockNo,
            selectedHouseType: element.name
          }, () => {
            console.log('temp', this.state.blockNoType);
            console.log('temp', this.state.selectedBlockNo);
            console.log('selectedHouseType', this.state.selectedHouseType);
          })
        }

      }
    });

  }

  onBackPress() {
    this.props.navigation.goBack();
  }
  imagePickerHandler() {
    console.warn("onpress image picker");
    ImagePicker.openPicker({
      width: 300,
      height: 400,
      includeBase64: true,
    })
      .then(image => {
        this.setState({
          profileImg: image.path,
          profileImgBase: image.data
        });
        console.log(this.state.profileImgBase);
      })
      .catch(err => {
        console.warn("err in image pick", err);
      });
  }
  onPressSubmit = () => {
    const name = /^[a-zA-Z '.]{3,30}$/;
    const number = /^[0-9\b]{10,10}$/;
    const emailReg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    let isValid = true;
    // const {
    //   fname,
    //   lname,
    //   mobileNo,
    //   email,
    //   selectedGender,
    //   selectedHouseType,
    //   selectedBlock,
    //   selectedBlockNo,
    //   password,
    //   cPassword
    // } = this.state;
    const {
      profileImageBase,
      fname,
      lname,
      mobileNo,
      email,
      selectedGender,
      selectedHouseType,
      selectedBlock,
      selectedBlockNo,
      isOwnerStayInSociety,
      currentAddress,
      isRented,
      password
    } = this.state;

    //check validaiton - first name
    if (!fname) {
      isValid = false;
      this.refs.toast.show("Please enter first name", 1500);
    } else if (name.test(fname) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid first name", 1500);
    }
    //check validation - last name

    if (!lname && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter last name", 1500);
    } else if (name.test(lname) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid last name", 1500);
    }
    //check validation - mobile number
    if (!mobileNo && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter mobile number", 2000);
    } else if (number.test(mobileNo) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid mobile number", 2000);
    }

    //email
    if (!email && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter e-mail", 2000);
    } else if (emailReg.test(email) === false && isValid) {
      isValid = false;
      this.refs.toast.show("Please enter valid e-mail", 2000);
    }

    //check validation - gender
    if (!selectedGender && isValid) {
      isValid = false;
      this.refs.toast.show("Please select gender", 2000);
    }
    //check validation - house type
    if (!selectedHouseType && isValid) {
      isValid = false;
      this.refs.toast.show("Please select house type", 2000);
    }
    //check validation - block (abcd)
    // if (!selectedBlock && isValid) {
    //   isValid = false;
    //   this.refs.toast.show("Please select block", 2000);
    // }
    //check validation - house no
    if (!selectedBlockNo && isValid) {
      isValid = false;
      this.refs.toast.show("Please select house no", 2000);
    }
    if (isValid) {
      console.warn("is Valid");

      try {
        AsyncStorage.getItem('@authToken_Key')
          .then((token) => {
            console.log('token_reset', token)

            this.props.editUserProfile({
              userId,
              profileImageBase,
              fname,
              lname,
              mobileNo,
              email,
              selectedGender,
              selectedHouseType,
              selectedBlock,
              selectedBlockNo,
              isOwnerStayInSociety,
              currentAddress,
              isRented,
              password,
              token
            })
          })
          .catch((err) => {
            console.log('err', err)
          })
      } catch (e) {
        console.log('Error in store authToken');
      }

    } else {
      console.warn("is not valid");
    }
  };
  render() {
    return (
      <Fragment>
        <SafeAreaView style={ { flex: 0, backgroundColor: DARK_GRADIENT } } />
        <SafeAreaView style={ { flex: 1 } }>
          <StatusBar backgroundColor={ DARK_GRADIENT } barStyle="light-content" />
          <View style={ { height: 50 } }>
            <Header
              headerText={ "Edit Profile" }
              leftImage={ require("../img/left-arrow.png") }
              onBackPress={ () => this.onBackPress() }
            />
          </View>
          <ScrollView
            style={ { flex: 1 } }
            contentContainerStyle={ { paddingBottom: 300 } }
            showsVerticalScrollIndicator={ false }
          >
            <ImageBackground
              key={ new Date() }
              style={ { height: "60%", width: "100%" } }
              source={
                this.state.profileImg
                  ? { uri: this.state.profileImg + '?' + new Date() }
                  : require("../img/man.png")
              }
            >
              <View style={ styles.header } />

              <View style={ styles.mainRoundView }>
                <View style={ styles.roundView }>
                  <TouchableOpacity
                    onPress={ () => this.imagePickerHandler() }
                    style={ styles.profileImg }
                  >
                    <Image
                      key={ new Date() }
                      style={ {
                        height: 95,
                        width: 95,
                        borderRadius: 95 / 2,
                        alignSelf: "center"
                      } }
                      source={
                        this.state.profileImg
                          ? { uri: this.state.profileImg + '?' + new Date() }
                          : require("../img/man.png")
                      }
                    />
                  </TouchableOpacity>
                </View>
                <TouchableOpacity
                  onPress={ () => this.imagePickerHandler() }
                  style={ styles.plusView }
                >
                  <Image
                    style={ styles.imgPlus }
                    source={ require("../img/add.png") }
                  />
                </TouchableOpacity>
              </View>
              <View style={ styles.infoView }>
                <View style={ styles.inputView }>
                  <Image
                    style={ styles.imageIconStyle }
                    source={ require("../img/user.png") }
                  />
                  <TextInput
                    allowFontScaling={ false }
                    style={ styles.textInput }
                    placeholder="Enter First Name"
                    placeholderTextColor="white"
                    placeholderTextSize="15"
                    keyboardType="default"
                    maxLength={ 30 }
                    onChangeText={ text => this.setState({ fname: text }) }
                    value={ this.state.fname }
                  // onChangeText={this.props.onChangeText}
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                  />
                </View>
                <View style={ styles.inputView }>
                  <Image
                    style={ styles.imageIconStyle }
                    source={ require("../img/user.png") }
                  />
                  <TextInput
                    allowFontScaling={ false }
                    style={ styles.textInput }
                    placeholder="Enter Last Name"
                    placeholderTextColor="white"
                    placeholderTextSize="15"
                    keyboardType="default"
                    maxLength={ 30 }
                    onChangeText={ text => this.setState({ lname: text }) }
                    value={ this.state.lname }
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                  />
                </View>
                <View style={ styles.inputView }>
                  <Image
                    style={ styles.imageIconStyle }
                    source={ require("../img/smartphone.png") }
                  />
                  <TextInput
                    allowFontScaling={ false }
                    style={ styles.textInput }
                    placeholder="Enter Mobile Number"
                    placeholderTextColor="white"
                    placeholderTextSize="15"
                    keyboardType="number-pad"
                    maxLength={ 10 }
                    onChangeText={ text => this.setState({ mobileNo: text }) }
                    value={ this.state.mobileNo }
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                  />
                </View>
                <View style={ styles.inputView }>
                  <Image
                    style={ styles.imageIconStyle }
                    source={ require("../img/envelope.png") }
                  />
                  <TextInput
                    allowFontScaling={ false }
                    style={ styles.textInput }
                    placeholder="Enter E-mail"
                    placeholderTextColor="white"
                    placeholderTextSize="15"
                    keyboardType="email-address"
                    maxLength={ 30 }
                    onChangeText={ text => this.setState({ email: text }) }
                    value={ this.state.email }
                  // returnKeyType="search"
                  // onSubmitEditing={this.props.onSubmitEditing}
                  />
                </View>
                { (!this.state.isOwnerStayInSociety == 'No') ?
                  <View>
                    <View style={ styles.inputView }>
                      <Image
                        style={ styles.imageIconStyle }
                        source={ require("../img/address.png") }
                      />
                      <TextInput
                        allowFontScaling={ false }
                        style={ styles.textInput }
                        placeholder="Enter Current Address"
                        placeholderTextColor="white"
                        placeholderTextSize="15"
                        keyboardType="default"
                        maxHeight={ 60 }
                        multiline={ true }
                        onChangeText={ text =>
                          this.setState({ currentAddress: text })
                        }
                        value={ this.state.currentAddress }
                      />
                    </View>
                    <View style={ [styles.inputView, { justifyContent: 'center', alignItems: 'center', paddingVertical: 14 }] } >
                      <Image
                        style={ styles.imageIconStyle }
                        source={ require("../img/Rented.png") }
                      />
                      <CustomTextView
                        style={ [styles.modalText] }
                        text="It is rented ?"
                      />
                      { this.state.radioBtnsData.map((data, key) => {
                        return (
                          <View key={ key } style={ { flex: 1, marginLeft: 20 } }>
                            { this.state.isRented == key ? (
                              <TouchableOpacity
                                onPress={ () => {
                                  console.warn('key', key)
                                  this.setState({ isRented: key });
                                } }
                                style={ styles.btnRadio }
                              >
                                <Image
                                  style={ styles.img }
                                  source={ require("../img/selectedRadioBtn.png") }
                                />
                                <CustomTextView
                                  text={ data }
                                  style={ [styles.radioText] }
                                />
                              </TouchableOpacity>
                            ) : (
                                <TouchableOpacity
                                  onPress={ () => {
                                    console.warn('key', key)
                                    this.setState({ isRented: key });
                                  } }
                                  style={ styles.btnRadio }
                                >
                                  <Image
                                    style={ [styles.img, {}] }
                                    source={ require("../img/unSelectedRadioBtn.png") }
                                  />

                                  <CustomTextView
                                    text={ data }
                                    style={ [styles.radioText] }
                                  />
                                </TouchableOpacity>
                              ) }
                          </View>
                        );
                      }) }
                    </View>
                  </View>
                  :
                  null
                }
                <CustomDropDown
                  style={ { backgroundColor: DARK_GRADIENT, marginTop: 15 } }
                  image={ require("../img/gender.png") }
                  item={ this.state.gender }
                  prompt="Select Gender"
                  onValueChange={ (itemValue, itemIndex) => {
                    if (itemValue != 0) {
                      this.setState({
                        selectedGender: this.state.gender[itemIndex - 1].name
                      });
                    } z
                  } }
                  selectedValue={ this.state.selectedGender || "Select Gender" }
                  text={ this.state.selectedGender }
                />

                <CustomDropDown
                  style={ { backgroundColor: DARK_GRADIENT, marginTop: 15 } }
                  enabled={ !this.props.isLoading }
                  image={ require("../img/house-outline.png") }
                  item={ this.props.houseType }
                  prompt="Select House Type"
                  onValueChange={ (itemValue, itemIndex) => {
                    if (itemValue != 0) {
                      let arr = [];
                      //blank block type and house no

                      let data = this.props.houseType[itemIndex - 1].blocklist;
                      data.forEach(element => {
                        //check block type exist
                        if (Object.keys(element)[0] == 'name') {
                          //set house type & block no
                          this.setState({
                            selectedHouseType: this.state.houseType[itemIndex - 1].name,
                            blockType: this.props.houseType[itemIndex - 1].blocklist
                          });
                        } else {
                          this.setState({ blockType: arr, blockNoType: arr })
                          //set house no & house type
                          let tempData = data[0].house_no;
                          let temp = [];
                          for (let index = 0; index < tempData.length; index++) {
                            let obj = {};
                            obj.name = tempData[index].toString();
                            temp.push(obj);
                          }
                          this.setState({
                            blockNoType: temp,
                            selectedHouseType: this.props.houseType[itemIndex - 1].name
                          }, () => {
                            console.log('temp', this.state.blockNoType);
                          })
                        }
                      })

                    }
                  } }
                  selectedValue={ this.state.selectedHouseType }
                  text={ this.state.selectedHouseType }
                />
                <View
                  style={ {
                    flexDirection: "row",
                    width: "100%",
                    justifyContent: "space-between"
                  } }
                >
                  { (this.state.blockType.length > 0) ?
                    <View style={ { width: "42%" } }>

                      <CustomDropDown
                        style={ { backgroundColor: DARK_GRADIENT, marginTop: 15 } }
                        enabled={ !this.props.isLoading }
                        item={ this.state.blockType }
                        prompt="Select Block"
                        onValueChange={ (itemValue, itemIndex) => {
                          if (itemValue != 0) {
                            this.setState({
                              selectedBlock: this.state.blockType[itemIndex - 1].name
                            }, () => {
                              let data = this.state.blockType[itemIndex - 1].house_no;
                              let temp = [];
                              for (let index = 0; index < data.length; index++) {
                                let obj = {};
                                obj.name = data[index].toString();
                                temp.push(obj);
                              }
                              this.setState({ blockNoType: temp }, () => {
                                console.log('temp', this.state.blockNoType);
                              })
                            });
                          }
                        } }
                        selectedValue={
                          this.state.selectedBlock || "Select Block"
                        }
                        text={ this.state.selectedBlock }
                      />
                    </View>
                    :
                    null }

                  <View style={ { width: (this.state.blockType.length > 0) ? "52%" : "100%" } }>
                    <CustomDropDown
                      style={ { backgroundColor: DARK_GRADIENT, marginTop: 15 } }
                      enabled={ !this.props.isLoading }
                      item={ this.state.blockNoType }
                      prompt="Select House No"
                      onValueChange={ (itemValue, itemIndex) => {
                        if (itemValue != 0) {
                          this.setState({
                            selectedBlockNo: this.state.blockNoType[itemIndex - 1].name
                          });
                        }
                      } }
                      selectedValue={ this.state.selectedBlockNo }
                      text={ this.state.selectedBlockNo }
                    />
                  </View>
                </View>


                <TouchableOpacity
                  activeOpacity={ 0.9 }
                  onPress={ this.onPressSubmit }
                  style={ styles.btn }
                >
                  { this.props.isLoading ? (
                    <Image
                      style={ {
                        height: 40,
                        width: 40,
                        backgroundColor: "transparent"
                      } }
                      source={ require("../img/loader2.gif") }
                    />
                  ) : (
                      <CustomTextView style={ styles.textStyle } text="Submit" />
                    ) }
                </TouchableOpacity>

                { (this.state.isRented == 'Yes') ?
                  <TouchableOpacity
                    onPress={ () =>
                      this.props.navigation.navigate("RenterDetails")
                    }
                    activeOpacity={ 0.9 }
                    style={ styles.linkStyle }
                  >
                    <CustomTextView
                      style={ styles.textStyle2 }
                      text="Are you want to change renter's details ? "
                    />
                    <View
                      style={ { borderBottomWidth: 1, borderColor: "#1A2980" } }
                    >
                      <CustomTextView
                        style={ styles.textStyle2 }
                        text="Click here"
                      />
                    </View>
                  </TouchableOpacity>
                  :
                  null }
              </View>
              <Toast
                ref="toast"
                style={ { backgroundColor: "#1E76A1" } }
                opacity={ 1 }
              />
            </ImageBackground>
          </ScrollView>
        </SafeAreaView>
      </Fragment>
    );
  }
}
const styles = StyleSheet.create({
  containerHeader: {
    position: "absolute",
    width: "100%",
    height: 50,
    flexDirection: "row",
    paddingHorizontal: 10,
    zIndex: 100
  },
  imageStyle: {
    height: 25,
    width: 25,
    tintColor: "white",
    alignSelf: "center"
  },
  imgView: {
    height: "100%",
    justifyContent: "center"
  },
  headerText: {
    fontSize: 20,
    color: "white",
    textAlign: "center",
    marginRight: "5%"
  },
  header: {
    height: "60%",
    width: "100%",
    top: 0,
    backgroundColor: "rgba(30, 118, 161, 0.5)",
    flexDirection: "row"
  },
  roundView: {
    height: 100,
    width: 100,
    borderRadius: 100 / 2,
    borderWidth: 3,
    backgroundColor: "white",
    borderColor: DARK_GRADIENT,
    alignSelf: "center",
    bottom: "6%"
  },
  profileImg: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 100 / 2,
    backgroundColor: "white"
  },
  plusView: {
    height: 30,
    width: 30,
    borderRadius: 30 / 2,
    position: "absolute",
    right: "36%",
    backgroundColor: DARK_GRADIENT,
    alignItems: "center",
    justifyContent: "center"
  },
  imgPlus: {
    height: 20,
    width: 20,
    tintColor: "white"
  },
  mainRoundView: {
    height: "10%",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  inputView: {
    width: "100%",
    borderRadius: 30,
    backgroundColor: DARK_GRADIENT,
    marginTop: 15,
    paddingHorizontal: 5,
    flexDirection: "row",
    paddingVertical: Platform.OS === "ios" ? 10 : 0
  },
  imageIconStyle: {
    height: 20,
    width: 20,
    tintColor: "white",
    alignSelf: "center",
    marginLeft: 8
  },
  textInput: {
    fontSize: 14,
    color: "white",
    width: "100%",
    marginLeft: 8
  },
  textStyle: {
    fontSize: 20,
    alignSelf: "center",
    color: "white"
  },
  infoView: {
    flex: 1,
    paddingHorizontal: 10,
    marginTop: 20
  },
  btn: {
    height: 40,
    width: 100,
    borderRadius: 30,
    marginTop: 20,
    alignSelf: "flex-end",
    alignItems: "center",
    backgroundColor: "#1E76A1",
    justifyContent: "center"
  },
  textStyle2: {
    fontSize: 15,
    color: "#1A2980",
    alignSelf: "center"
  },
  linkStyle: {
    flex: 1,
    paddingHorizontal: 10,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 20
  },
  modalText: {
    color: 'white',
    fontSize: 15,
    marginLeft: 15
  },
  img: {
    height: 20,
    width: 20,
    marginLeft: 8,
    marginRight: 5,
    tintColor: 'white'
  },
  btnRadio: {
    flexDirection: "row"
  },
  radioText: {
    color: "white",
    fontSize: 15
  },
});

const mapStateToProps = state => {
  return {
    houseType: state.Login.houseType,
    isLoading: state.Profile.isLoading,
    authResult: state.Profile.authResult,
  };
};

export default connect(
  mapStateToProps,
  {
    editUserProfile
    // userSignUp
  }
)(Profile);

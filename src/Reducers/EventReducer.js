
import {
    IINITIAL_EVENT_DATA,
    ADD_EVENT_LOADING,
    ADD_EVENT_SUCCESS,
    ADD_EVENT_FAILED,
    ADD_EVENT_ERROR,
    EVENT_LIST_LODING,
    EVENT_LIST_SUCCESS,
    EVENT_LIST_FAILED,
    EVENT_LIST_ERROR,
    DELETE_EVENT_LOADING,
    DELETE_EVENT_SUCCESS,
    DELETE_EVENT_FAILED,
    DELETE_EVENT_ERROR,
    EDIT_EVENT_LOADING,
    EDIT_EVENT_SUCCESS,
    EDIT_EVENT_FAILED,
    EDIT_EVENT_ERROR,
} from '../Actions/type';

const INTIAL_STATE = {
    authResult: '',
    event: [],
    msg: '',
    isLoading: false,
    isListLoading: false,
    isLoadingDelete: false,
};

export default (state = INTIAL_STATE, action) => {
    switch (action.type) {

        case ADD_EVENT_LOADING:
            return {
                ...state,
                isLoading: true,
            };
        case ADD_EVENT_SUCCESS:
            // console.log('success action.payload', action.payload.list)
            return {
                ...state,
                // users: action.payload.list,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoading: false,
            };
        case ADD_EVENT_FAILED:
            // console.log('failed action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoading: false
            };
        case ADD_EVENT_ERROR:
            // console.log('error action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload,
                isLoading: false
            };
        //Edit Event
        case EDIT_EVENT_LOADING:
            return {
                ...state,
                isLoading: true,
            };
        case EDIT_EVENT_SUCCESS:
            // console.log('success action.payload', action.payload.list)
            return {
                ...state,
                // users: action.payload.list,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoading: false,
            };
        case EDIT_EVENT_FAILED:
            // console.log('failed action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoading: false
            };
        case EDIT_EVENT_ERROR:
            // console.log('error action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload,
                isLoading: false
            };
        //event List
        case EVENT_LIST_LODING:
            return {
                ...state,
                isListLoading: true,
            };
        case EVENT_LIST_SUCCESS:
            console.log('success action.payload', action.payload.list)
            return {
                ...state,
                event: action.payload.list,
                isListLoading: false,
                isDataEvent: action.payload.isData
            };
        case EVENT_LIST_FAILED:
            console.log('failed action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload,
                isListLoading: false
            };
        case EVENT_LIST_ERROR:
            console.log('error action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload,
                isListLoading: false
            };

        //delete event
        case DELETE_EVENT_LOADING:
            return {
                ...state,
                isLoadingDelete: true,
            };
        case DELETE_EVENT_SUCCESS:
            console.log('success action.payload', action.payload.list)
            return {
                ...state,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoadingDelete: false,
            };
        case DELETE_EVENT_FAILED:
            console.log('failed action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoadingDelete: false
            };
        case DELETE_EVENT_ERROR:
            console.log('error action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoadingDelete: false
            };

        case IINITIAL_EVENT_DATA:
            return INTIAL_STATE;
        default:
            return state;
    }
};

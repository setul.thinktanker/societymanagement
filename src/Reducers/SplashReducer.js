import {
    GET_TOKEN_LODING,
    GET_TOKEN_SUCCESS,
    GET_TOKEN_FAILED,
} from '../Actions/type';

const INTIAL_STATE = {
    splashAuthToken: '',
    authResult: '',
    isLoading: ''
};

export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
        case GET_TOKEN_LODING:
            return {
                ...state,
                isLoading: true,
            };
        case GET_TOKEN_SUCCESS:
            return {
                ...state,
                authResult: action.payload.text,
                splashAuthToken: action.payload.splashAuthToken,
                isLoading: false
            };
        case GET_TOKEN_FAILED:
            return {
                ...state,
                authResult: action.payload,
                isLoading: false,
            };
        default:
            return state;
    }
};

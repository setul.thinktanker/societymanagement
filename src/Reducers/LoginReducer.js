import {
  LOGIN_LOADING,
  LOGIN_INITIAL_STATE,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  SOCIETYCODE_SUCCESS,
  SOCIETYCODE_FAILED,
  SOCIETYCODE_ERROR
} from '../Actions/type';

const INTIAL_STATE = {
  mobileNo: '',
  mobileNoErr: '',
  password: '',
  passwordError: '',
  isLoading: false,
  authToken: '',
  authResult: '',
  error: '',
  houseType: []
};

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {

    case LOGIN_LOADING:
      // console.log('loading somthing...');
      return {
        ...state,
        isLoading: true,
        authResult: '',
      };
    case LOGIN_FAILED:
      // console.log('action.payload.Login', action.payload)
      return {
        ...state,
        authResult: action.payload.text,
        error: action.payload.error,
        isLoading: false,
      };
    case LOGIN_SUCCESS:
      // console.log('action.payload.Login', action.payload)
      return {
        ...state,
        isLoading: false,
        authResult: action.payload.text,
        authToken: action.payload.authToken
      };
    case LOGIN_ERROR:
      console.log('action.payload.Login', action.payload)
      return {
        ...state,
        isLoading: false,
        authResult: action.payload,
      };
    case LOGIN_INITIAL_STATE:
      return INTIAL_STATE;

    case SOCIETYCODE_SUCCESS:
      // console.log('action.payload.houseType', action.payload.houseType);
      return {
        ...state,
        authResult: action.payload,
        houseType: action.payload.houseType
      }
    case SOCIETYCODE_FAILED:
      return {
        ...state,
        authResult: action.payload
      }
    case SOCIETYCODE_ERROR:
      return {
        ...state,
        authResult: action.payload
      }
    default:
      return state;
  }
};

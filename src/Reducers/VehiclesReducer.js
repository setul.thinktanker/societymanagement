
import {
    INITIAL_VEHICLES_DATA,
    VEHICLE_LIST_LODING,
    VEHICLE_LIST_SUCCESS,
    VEHICLE_LIST_FAILED,
    VEHICLE_LIST_ERROR,
    ADD_VEHICLE_LOADING,
    ADD_VEHICLE_SUCCESS,
    ADD_VEHICLE_FAILED,
    ADD_VEHICLE_ERROR,
    DELETE_VEHICLE_LOADING,
    DELETE_VEHICLE_SUCCESS,
    DELETE_VEHICLE_FAILED,
    DELETE_VEHICLE_ERROR,
} from '../Actions/type';

const INTIAL_STATE = {
    myVehicles: [],
    authResult: '',
    vehicles: [],
    msg: '',
    isLoading: false,
    isLoadingDelete: false,
};

export default (state = INTIAL_STATE, action) => {
    switch (action.type) {

        case ADD_VEHICLE_LOADING:
            return {
                ...state,
                isLoading: true,
            };
        case ADD_VEHICLE_SUCCESS:
            // console.log('success action.payload', action.payload.list)
            return {
                ...state,
                // users: action.payload.list,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoading: false,
            };
        case ADD_VEHICLE_FAILED:
            // console.log('failed action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoading: false
            };
        case ADD_VEHICLE_ERROR:
            // console.log('error action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload,
                isLoading: false
            };
        //delete vehicle
        case DELETE_VEHICLE_LOADING:
            return {
                ...state,
                isLoadingDelete: true,
            };
        case DELETE_VEHICLE_SUCCESS:
            console.log('success action.payload', action.payload.list)
            return {
                ...state,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoadingDelete: false,
            };
        case DELETE_VEHICLE_FAILED:
            console.log('failed action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoadingDelete: false
            };
        case DELETE_VEHICLE_ERROR:
            console.log('error action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                msg: action.payload.msg,
                isLoadingDelete: false
            };
        //get vehicles details
        case VEHICLE_LIST_LODING:
            return {
                ...state,
                isLoadingVehicles: true,
            };
        case VEHICLE_LIST_SUCCESS:
            console.log('success action.payload', action.payload.list)
            return {
                ...state,
                vehicles: action.payload.list,
                isLoadingVehicles: false,
                isDataVehicles: action.payload.isData
            };
        case VEHICLE_LIST_FAILED:
            console.log('failed action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload,
                isLoadingVehicles: false
            };
        case VEHICLE_LIST_ERROR:
            console.log('error action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload,
                isLoadingVehicles: false
            };
        case INITIAL_VEHICLES_DATA:
            return INTIAL_STATE;
        default:
            return state;
    }
};

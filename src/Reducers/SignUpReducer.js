import {
  SIGNUP_INITIAL_STATE,
  SIGNUP_ON_TEXT_CHANGE,
  SIGNUP_LOADING,
  SIGNUP_SUCCESS,
  SIGNUP_FAILED,
  SIGNUP_ERROR,
} from '../Actions/type';
const INTIAL_STATE = {

  isLoading: false,
  authResult: '',
  accessToken: '',
  signUpErr: '',
};

export default (state = INTIAL_STATE, action) => {
  switch (action.type) {

    case SIGNUP_ON_TEXT_CHANGE:
      return {
        ...state,
        isLoading: false,
        [action.payload.props]: action.payload.value,
        authResult: ''
      };

    case SIGNUP_INITIAL_STATE:
      return INTIAL_STATE;

    case SIGNUP_LOADING:
      return {
        ...state,
        isLoading: true,
        authResult: '',
      };
    case SIGNUP_FAILED:
      console.log('reducer', action.payload.signUpErr);
      return {
        ...state,
        isLoading: false,
        authResult: action.payload.text,
        signUpErr: action.payload.signUpErr
      };
    case SIGNUP_SUCCESS:
      console.log('action.payload.accessToken', action.payload.authToken);
      return {
        ...state,
        isLoading: false,
        authResult: action.payload.text,
        authToken: action.payload.authToken
      };
    case SIGNUP_ERROR:
      return {
        ...state,
        isLoading: false,
        authResult: action.payload.text,
      };
    default:
      return state;
  }
};
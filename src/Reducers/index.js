import { combineReducers } from 'redux';
import SplashReducer from './SplashReducer';
import LoginReducer from './LoginReducer';
import SignUpReducer from './SignUpReducer';
import ListingReducer from './ListingReducer';
import VehiclesReducer from './VehiclesReducer';
import RenterDetailsReducer from './RenterDetailsReducer';
import ProfileReducer from './ProfileReducer';
import EventReducer from './EventReducer';
import VendorReducer from './VenderReducer';
import NoticeReducer from './NoticeReducer';

export default combineReducers({
  Splash: SplashReducer,
  Login: LoginReducer,
  SignUp: SignUpReducer,
  Listing: ListingReducer,
  Vehicles: VehiclesReducer,
  RenterDetails: RenterDetailsReducer,
  Profile: ProfileReducer,
  Events: EventReducer,
  Vendor: VendorReducer,
  Notice: NoticeReducer

})
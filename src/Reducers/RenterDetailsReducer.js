import {RENTER_DETAILS_INITIAL_STATE,
    RENTER_DETAILS_LOADING,
    RENTER_DETAILS_SUCCESS,
    RENTER_DETAILS_FAILED,
    RENTER_DETAILS_ERROR} from '../Actions/type';

const INTIAL_STATE = {

isLoading: false,
authResult: '',

};

export default (state = INTIAL_STATE, action) => {
switch (action.type) {


    
  case RENTER_DETAILS_INITIAL_STATE:
    return INTIAL_STATE;

  case RENTER_DETAILS_LOADING:
    return{
       ...state,
       isLoading: true,
       authResult: '',
    };
  case RENTER_DETAILS_FAILED:
    return {
       ...state,
       authResult: action.payload,
       isLoading: false,
      //  fnameErr: action.payload.fnameErr,
      //  lnameErr: action.payload.lnameErr,
      //  mobileNoErr: action.payload.mobileNoErr,
      //  passwordErr: action.payload.passwordErr,
      //  genderErr: action.payload.genderErr,
      //  houseTypeErr: action.payload.houseTypeErr,
      //  houseBlockTypeErr: action.payload.houseBlockTypeErr,
      //  blockNoTypeErr: action.payload.blockNoTypeErr,
     };
  case RENTER_DETAILS_SUCCESS:
    return {
        ...state,
        isLoading: false,
        authResult: action.payload
     };
    case RENTER_DETAILS_ERROR:
      return{
              ...state,
              isLoading: false,
              authResult: action.payload,
            };
  default:
    return state;
}
};
import {
    DELETE_NOTICE_FAILED, DELETE_NOTICE_LOADING,
    DELETE_NOTICE_SUCCESS,
    GET_NOTICE_FAILED,
    GET_NOTICE_LOADING,
    GET_NOTICE_SUCCESS
} from "../Actions/type";

const INITIAL_STATE = {
    isLoading: false,
    noticeList: '',
    isNoticeDeleted:false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_NOTICE_LOADING:
            return {
                ...state,
                isLoading: true
            };
        case GET_NOTICE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                success: action.payload.success,
                noticeList: action.payload.data,
            };
        case GET_NOTICE_FAILED:
            return {
                ...state,
                isLoading: false,
                success: false,
            };

        case DELETE_NOTICE_SUCCESS:
            return {
                ...state,
                isLoading: false,
                success: response,
            };
        case DELETE_NOTICE_FAILED:
            return {
                ...state,
                isLoading: false,
                success: false,
            };
        case DELETE_NOTICE_LOADING:
            return {
                ...state,
                isLoading: true,
                success: false,
            };
        default:
            return {
                ...state
            }
    }
}



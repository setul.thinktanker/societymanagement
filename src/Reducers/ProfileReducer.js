import {
    EDIT_PROFILE_LODING,
    EDIT_PROFILE_SUCCESS,
    EDIT_PROFILE_FAILED,
    EDIT_PROFILE_ERROR,
    RESET_PWD_LODING,
    RESET_PWD_SUCCESS,
    RESET_PWD_FAILED,
    RESET_PWD_ERROR,
} from '../Actions/type';

const INTIAL_STATE = {
    authResult: '',
    msg: '',
    isLoading:false,
    isLoadingResetPwd: false
};

export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
        case EDIT_PROFILE_LODING:
            // console.log('loading somthing...');
            return {
                ...state,
                isLoading: true,
                authResult: '',
            };
        case EDIT_PROFILE_SUCCESS:
            console.log('action.payload.Profile', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                isLoading: false,
            };
        case EDIT_PROFILE_FAILED:
            console.log('action.payload.Profile', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                isLoading: false,
            };
        case EDIT_PROFILE_ERROR:
            console.log('action.payload.Profile', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                isLoading: false,
            };
        case RESET_PWD_LODING:
            // console.log('loading somthing...');
            return {
                ...state,
                isLoadingResetPwd: true,
                authResult: '',
            };
        case RESET_PWD_SUCCESS:
            console.log('action.payload.Profile', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                isLoadingResetPwd: false,
                msg: action.payload.msg
            };
        case RESET_PWD_FAILED:
            console.log('action.payload.Profile', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                isLoadingResetPwd: false,
                msg: action.payload.msg
            };
        case RESET_PWD_ERROR:
            console.log('action.payload.Profile', action.payload)
            return {
                ...state,
                authResult: action.payload.text,
                isLoadingResetPwd: false,
            };
        default:
            return state;
    }
};
import {
    ADD_VENDOR_ERROR,
    ADD_VENDOR_LOADING,
    ADD_VENDOR_SUCCESS, DELETE_EVENT_LOADING, DELETE_VENDOR_ERROR, DELETE_VENDOR_FAILED, DELETE_VENDOR_SUCCESS,
    GET_VENDOR_FAILED,
    GET_VENDOR_LOADING,
    GET_VENDOR_SUCCESS
} from "../Actions/type";

const INITIAL_STATE = {
    isLoading: '',
    response: '',
    msg: '',
    success: '',
    vendorListLoading: '',
    venderList: '',
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case ADD_VENDOR_LOADING:
            // console.log("ADD_VENDOR_LOADING");
            return {
                ...state,
                isLoading: true,
            };
        case ADD_VENDOR_SUCCESS:
            // console.log("ADD_VENDOR_SUCCESS ", action.payload);
            return {
                ...state,
                isLoading: false,
                success: action.payload.success,
                msg: action.payload.text,
                response: action.payload.data
            };
        case ADD_VENDOR_ERROR:
            // console.log(ADD_VENDOR_ERROR);
            return {
                ...state,
                isLoading: false,
                success: false,
                msg: action.payload.text,
            };
        case GET_VENDOR_LOADING:
            return {
                ...state,
                vendorListLoading: true
            };
        case GET_VENDOR_SUCCESS:
            console.log("GET_VENDOR_SUCCESS:", action.payload);
            return {
                ...state,
                vendorListLoading: false,
                success: action.payload.success,
                msg: action.payload.msg,
                response: action.payload.data,
                venderList: action.payload.data,
            };
        case GET_VENDOR_FAILED:
            return {
                ...state,
                vendorListLoading: false,
                response: "Something went wrong!",
                success: false,
            };
        case DELETE_EVENT_LOADING:
            return {
                ...state,
                isLoading: true,
            };
        case DELETE_VENDOR_SUCCESS:
            return {
                ...state,
                isLoading: false,
                success: action.payload.response.success,
                msg: action.payload.response.msg
            };
        case DELETE_VENDOR_ERROR:
            return {
                ...state,
                isLoading: false,
                success: action.payload.response.success,
                msg: action.payload.response.msg
            };
        case DELETE_VENDOR_FAILED:
            return {
                ...state,
                isLoading: false,
                success: false,
                msg: "Something went wrong!!"
            };
        default:
            return state;
    }
};

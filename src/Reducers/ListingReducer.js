import {
    SOCIETY_MEMBERS_LIST_LODING,
    SOCIETY_MEMBERS_LIST_SUCCESS,
    SOCIETY_MEMBERS_LIST_FAILED,
    SOCIETY_MEMBERS_LIST_ERROR,
    VEHICLE_LIST_LODING,
    VEHICLE_LIST_SUCCESS,
    VEHICLE_LIST_FAILED,
    VEHICLE_LIST_ERROR,
} from '../Actions/type';

const INTIAL_STATE = {
    users: [],
    vehicles: [],
    authResult: '',
    isLoading: false,
    isLoadingVehicles: false,
    isData: false,
    isDataVehicles: false
};

export default (state = INTIAL_STATE, action) => {
    switch (action.type) {
        case SOCIETY_MEMBERS_LIST_LODING:
            return {
                ...state,
                isLoading: true,
            };
        case SOCIETY_MEMBERS_LIST_SUCCESS:
            // console.log('success action.payload', action.payload.list)
            return {
                ...state,
                users: action.payload.list,
                isLoading: false,
                isData: action.payload.isData
            };
        case SOCIETY_MEMBERS_LIST_FAILED:
            // console.log('failed action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload,
                isLoading: false
            };
        case SOCIETY_MEMBERS_LIST_ERROR:
            // console.log('error action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload,
                isLoading: false
            };
        //get vehicles details
        case VEHICLE_LIST_LODING:
            return {
                ...state,
                isLoadingVehicles: true,
            };
        case VEHICLE_LIST_SUCCESS:
            console.log('success action.payload', action.payload.list)
            return {
                ...state,
                vehicles: action.payload.list,
                isLoadingVehicles: false,
                isDataVehicles: action.payload.isData
            };
        case VEHICLE_LIST_FAILED:
            console.log('failed action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload,
                isLoadingVehicles: false
            };
        case VEHICLE_LIST_ERROR:
            console.log('error action.payload', action.payload)
            return {
                ...state,
                authResult: action.payload,
                isLoadingVehicles: false
            };

        default:
            return state;
    }
};

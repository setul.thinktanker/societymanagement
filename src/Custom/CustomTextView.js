import React, { PureComponent } from 'react';
import { Text } from 'react-native';

export default class CustomTextView extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    return (
        <Text allowFontScaling = {false} 
        style = {this.props.style} 
        numberOfLines ={this.props.numberOfLines}
        onPress = {this.props.onPress} >{this.props.text}</Text>
    );
  }
}

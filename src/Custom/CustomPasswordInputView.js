import React, { PureComponent } from "react";
import {
  View,
  Image,
  TextInput,
  Dimensions,
  TouchableOpacity,
  StyleSheet
} from "react-native";

export default class CustomPasswordInputView extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isHidePwd: true
    };
  }

  render() {
    return (
      <View style={{ flexDirection: "row", flex: 1 }}>
        <TextInput
          allowFontScaling={false}
          style={styles.textInput}
          placeholder={this.props.placeholder}
          value={this.props.value}
          secureTextEntry={this.state.isHidePwd ? true : false}
          placeholderTextColor="white"
          placeholderTextSize="15"
          keyboardType="default"
          maxLength={30}
          editable={this.props.editable}
          onChangeText={this.props.onChangeText}
        />
        <TouchableOpacity
          style={{ width: "20%" }}
          onPress={() => {
            this.setState({ isHidePwd: !this.state.isHidePwd });
          }}
        >
          <Image
            style={[styles.imageIconStyle, { alignSelf: "flex-end" }]}
            source={
              this.state.isHidePwd
                ? require("../img/hide.png")
                : require("../img/view.png")
            }
          />
        </TouchableOpacity>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  textInput: {
    fontSize: 14,
    color: "white",
    width: "75%",
    marginLeft: 8
  },
  imageIconStyle: {
    height: 22,
    width: 22,
    tintColor: "white",

    marginTop: Platform.OS === "ios" ? 0 : 12
  }
});

import React, { PureComponent } from 'react';
import { View, Image, Picker, TouchableOpacity, Platform, Modal, StyleSheet } from 'react-native';
import CustomTextView from '../Custom/CustomTextView';

export default class CustomDropDown extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      showPicker: false
    };
  }
  componentWillMount() {

    this.setState({ OS: Platform.OS })
  }
  pickerItem(itemArray) {
    // console.log(itemArray);
    return itemArray.map((item) => {
      return (
        <Picker.Item
          label={item.name}
          value={item.name}
        />
      )
    }
    );
  }


  pickerIos() {
    return (
      <TouchableOpacity
        onPress={() => this.setState({ showPicker: true })}
        enabled={this.props.enabled}
        activeOpacity={0.9}
        style={[styles.inputViewDropDown, this.props.style]}>
        {(this.props.image) ?
          <Image
            style={styles.imageIconStyle}
            source={this.props.image}
          />
          : null}
        <CustomTextView
          style={styles.textInput}
          text={(this.props.text) ? this.props.text : this.props.prompt} />
        <Image
          style={[styles.arrowIconStyle, { position: 'absolute', right: 10, bottom: 10 }]}
          source={require('../img/downArrow.png')}
        />
        <Modal
          visible={this.state.showPicker}
          transparent
          animationType={'slide'}
          supportedOrientations={['portrait', 'landscape']}
        >
          <TouchableOpacity
            style={{ flex: 1 }}
            onPress={() => {
              this.setState({ showPicker: false })
            }}
          />
          <TouchableOpacity
            onPress={() => {
              this.setState({
                showPicker: false,
              });
            }}
            hitSlop={styles.hitSlopStyle}
          >
            <View>
              <CustomTextView
                text="Done"
                style={styles.pickerText} />
            </View>
          </TouchableOpacity>
          <View
            style={styles.pickerViewStyle}>
            <Picker
              style={styles.pickerStyle}
              testID={'Picker'}
              enabled={true}
              onValueChange={this.props.onValueChange}
              selectedValue={this.props.selectedValue}>

              <Picker.Item label={this.props.prompt} value='0' />
              {this.pickerItem(this.props.item)}

            </Picker>
          </View>
        </Modal>
      </TouchableOpacity>
    );
  }

  pickerAndroid() {

    return (
      <TouchableOpacity
        activeOpacity={0.9}
        style={[styles.inputViewDropDown, { paddingVertical: 0 }, this.props.style]}>
        {(this.props.image) ?
          <Image
            style={styles.imageIconStyle}
            source={this.props.image}
          />
          : null}
        <Image
          style={[styles.arrowIconStyle, { position: 'absolute', right: 10, bottom: Platform.OS === 'ios' ? 10 : '30%' }]}
          source={require('../img/downArrow.png')}
        />
        <Picker
          style={styles.pickerAndroidStyle}
          testID={'Picker'}
          enabled={this.props.enabled}
          onValueChange={this.props.onValueChange}
          selectedValue={this.props.selectedValue}>

          <Picker.Item label={this.props.prompt} value='0' />
          {this.pickerItem(this.props.item)}
        </Picker>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View>
        {(this.state.OS === 'ios') ?
          this.pickerIos()
          :
          this.pickerAndroid()
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({

  pickerAndroidStyle: {
    flex: 1,
    backgroundColor: 'transparent',
    color: 'white',
    fontSize: 14,
    marginLeft: 10,
    justifyContent: 'center'
  },
  text: {
    color: 'white',
    fontSize: 10
  },

  inputViewDropDown: {
    width: '100%',
    borderRadius: 30,
    backgroundColor: 'rgba(255,255,255,0.3)',
    marginTop: 15,
    paddingHorizontal: 5,
    flexDirection: 'row',
    paddingVertical: Platform.OS === 'ios' ? 10 : 14,
    alignItems: 'center'
  },
  imageIconStyle: {
    height: 20,
    width: 20,
    tintColor: 'white',
    alignSelf: 'center',
    marginLeft: 8,
  },
  textInput: {
    fontSize: 14,
    color: 'white',
    width: '100%',
    marginLeft: 8,
    // marginTop:1
  },
  btn: {
    height: 40,
    width: 100,
    borderRadius: 30,
    marginTop: 20,
    alignSelf: 'flex-end',
    alignItems: 'center',
    backgroundColor: '#1A2980',
    justifyContent: 'center'
  },
  arrowIconStyle: {
    height: 18,
    width: 18,
    tintColor: 'white',
    alignSelf: 'center',
    marginLeft: 8
  },
  lineView: {
    borderRightWidth: 1.5,
    borderColor: 'white',
    borderRadius: 20,
    height: '80%',
    alignSelf: 'center',
    marginLeft: 6
  },
  pickerText: {
    color: '#007AFE',
    fontWeight: 'bold',
    fontSize: 16,
    padding: 5,
    textAlign: 'right',
    backgroundColor: 'white'
  },
  hitSlopStyle: {
    top: 4,
    right: 4,
    bottom: 4,
    left: 4
  },
  pickerViewStyle: {
    height: 215,
    justifyContent: 'center',
    backgroundColor: '#D0D4DB',
  },
  pickerStyle: {
    backgroundColor: 'white'
  }
});
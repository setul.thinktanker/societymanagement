import axios from 'axios';
import { RENTER_DETAILS_INITIAL_STATE,
        RENTER_DETAILS_LOADING,
        RENTER_DETAILS_SUCCESS,
        RENTER_DETAILS_FAILED,
        RENTER_DETAILS_ERROR
  } from './type';

  export const reanterDetailsInitialState = () => {
    return {
      type: RENTER_DETAILS_INITIAL_STATE,
      payload: ''
    };
};

export const reanterDetails = ({ fname, lname, mobileNo, email, SelectGender, SelectedDate, password }) =>{
  return(dispatch)=>{
      dispatch({
          type: RENTER_DETAILS_LOADING
      });

      setTimeout(() => {
          dispatch({
              type: RENTER_DETAILS_SUCCESS,
              payload: 'RenterDetails submit Success'
          });
      }, 2000);
  }
}
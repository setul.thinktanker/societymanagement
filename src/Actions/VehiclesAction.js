import axios from 'axios';
import {
    BASE_URL,
    INITIAL_VEHICLES_DATA,
    VEHICLE_LIST_LODING,
    VEHICLE_LIST_SUCCESS,
    VEHICLE_LIST_FAILED,
    VEHICLE_LIST_ERROR,
    ADD_VEHICLE_LOADING,
    ADD_VEHICLE_SUCCESS,
    ADD_VEHICLE_FAILED,
    ADD_VEHICLE_ERROR,
    DELETE_VEHICLE_LOADING,
    DELETE_VEHICLE_SUCCESS,
    DELETE_VEHICLE_FAILED,
    DELETE_VEHICLE_ERROR,
} from './type';


//initial vehicle data
export const initialVehicleData = () => {
    return (dispatch) => {
        dispatch({
            type: INITIAL_VEHICLES_DATA
        })
    };
}
//add vehicle 
export const addVehicle = ({ vehicleData, societyId, userId, token }) => {

    console.log('vehicleData', vehicleData)
    console.log('SocietyId', societyId)
    console.log('userId', userId)
    console.log('token', token)

    return (dispatch) => {
        dispatch({
            type: ADD_VEHICLE_LOADING
        })

        axios.post(`${BASE_URL}addvehicledetail`, {
            vehicle: vehicleData,
            user_id: userId,
            society_id: societyId,
        }, {
                headers: {
                    Accept: 'application/json',
                    Authorization: token
                }
            })
            .then(response => {
                if (response.data.success) {

                    dispatch(vehicleDetailsList({ userId, societyId, token, pageCount: 1 }))

                    dispatch({
                        type: ADD_VEHICLE_SUCCESS,
                        payload: { text: 'Add Success', msg: response.data.message }
                    });

                } else {
                    dispatch({
                        type: ADD_VEHICLE_FAILED,
                        payload: { text: 'Add Failed', msg: response.data.message }
                    });
                }
            })
            .catch((err) => {
                console.log('fetch fail GetVehicle', err);
                dispatch({
                    type: ADD_VEHICLE_ERROR,
                    payload: 'Add Error'
                });
            });
    };
}
//delete vehicle
export const deleteVehicle = ({ vehicleId, token, userId, societyId, pageCount, list }) => {

    console.log('vehicleId', vehicleId)
    console.log('token', token)
    console.log('userId', userId)
    console.log('societyId', societyId)
    console.log('pageCount', pageCount)
    console.log('list', list)

    return (dispatch) => {
        dispatch({
            type: DELETE_VEHICLE_LOADING
        })

        axios.post(`${BASE_URL}deletevehicle`, {
            vehicle_id: vehicleId,
        }, {
                headers: {
                    Accept: 'application/json',
                    Authorization: token
                }
            })
            .then(response => {
                if (response.data.success) {
                    dispatch(vehicleDetailsList({ userId, societyId, token, pageCount }))
                    dispatch({
                        type: DELETE_VEHICLE_SUCCESS,
                        payload: { text: 'Delete Success', msg: response.data.message }
                    });

                } else {
                    dispatch({
                        type: ADD_VEHICLE_FAILED,
                        payload: { text: 'Delete Failed', msg: response.data.error }
                    });
                }
            })
            .catch((err) => {
                console.log('fetch fail GetVehicle', err);
                dispatch({
                    type: ADD_VEHICLE_ERROR,
                    payload: { text: 'Delete Error', msg: 'something went wrong please try again later.' }
                });
            });
    };
}
//get vehicle details
export const vehicleDetailsList = ({ userId, societyId, token, pageCount, list }) => {

    return (dispatch) => {

        if (pageCount == 1) {
            dispatch({
                type: VEHICLE_LIST_LODING
            })
        }

        console.log('vehicleDetailsList vehicleDetailsList')
        console.log('societyId', societyId)
        console.log('token', token)
        console.log('userId', userId)
        console.log('pageCount', pageCount)
        console.log('list', list)

        axios.post(`${BASE_URL}vehicledetail`, {
            society_id: societyId,
            user_id: userId,
            page_no: pageCount,
            page_count: 10
        }, {
                headers: {
                    Accept: 'application/json',
                    Authorization: token
                }
            })
            .then(response => {
                console.log('response vehicle', response);
                if (response.data.success) {
                    let vehicles = response.data.data;
                    let isData = (vehicles.length == 0) ? false : true;
                    if (pageCount > 1) {
                        dispatch({
                            type: VEHICLE_LIST_SUCCESS,
                            payload: { list: list.concat(vehicles), isData: isData }
                        });
                    } else {
                        dispatch({
                            type: VEHICLE_LIST_SUCCESS,
                            payload: { list: vehicles, isData: isData }
                        });
                    }
                } else {
                    dispatch({
                        type: VEHICLE_LIST_FAILED,
                        payload: 'GetVehicle Failed'
                    });
                }
            })
            .catch((err) => {
                console.log('fetch fail GetVehicle', err);
                dispatch({
                    type: VEHICLE_LIST_ERROR,
                    payload: 'GetVehicle Error'
                });
            });
    };
}
import axios from 'axios';
import {
    BASE_URL,
    EDIT_PROFILE_LODING,
    EDIT_PROFILE_SUCCESS,
    EDIT_PROFILE_FAILED,
    EDIT_PROFILE_ERROR,
    RESET_PWD_LODING,
    RESET_PWD_SUCCESS,
    RESET_PWD_FAILED,
    RESET_PWD_ERROR,
} from './type';
import { updateUser, deleteUser } from "../Database/allSchema";

//For edit profile
export const editUserProfile = ({
    userId,
    profileImageBase,
    fname,
    lname,
    mobileNo,
    email,
    selectedGender,
    selectedHouseType,
    selectedBlock,
    selectedBlockNo,
    isOwnerStayInSociety,
    currentAddress,
    isRented,
    password,
    token
}) => {
    return (dispatch) => {
        dispatch({
            type: EDIT_PROFILE_LODING
        });
    }
    let data = {
        user_id: userId,
        name: fname + ' ' + lname,
        mobileNo: mobileNo,
        email: email,
        gender: selectedGender,
        houseType: selectedHouseType,
        block: selectedBlock,
        houseNo: selectedBlockNo,
        currentAddress: currentAddress,
        isRented: Yes,
        password: password,
        isOwnerStayInSociety: isOwnerStayInSociety,
        image: 'data:image/png;base64,' + profileImageBase
    }
    axios.post(`${BASE_URL}editprofile`,
        data,
        {
            headers: {
                Accept: 'application/json',
                Authorization: token
            }
        })
        .then((response) => {
            console.log('edit response')
            if (response.data.success) {
                let userData = response.data.user
                let updatedData = {
                    id: userData._id,
                    societyId: userData.society_id,
                    name: userData.name,
                    mobileNo: userData.mobileNo,
                    email: userData.email,
                    gender: userData.gender,
                    houseType: userData.houseType,
                    block: userData.block,
                    blockNo: userData.houseNo,
                    isOwnerStayInSociety: userData.isOwnerStayInSociety,
                    currentAddress: userData.currentAddress,
                    isRented: userData.isRented,
                    image: userData.image,
                    password: userData.password
                }
                updateUser(updatedData).then(() => {
                    console.log('Insert Suceess');
                    console.log(updateUser);
                    dispatch({
                        type: EDIT_PROFILE_SUCCESS,
                        payload: { text: 'Edit_profile successfully' }
                    });
                }).catch((error) => {
                    console.log(`User Insert error${error}`);
                    dispatch({
                        type: EDIT_PROFILE_FAILED,
                        payload: { text: 'Edit_profile Failed' }
                    });
                });
            }
        })
        .catch((err) => {
            dispatch({
                type: EDIT_PROFILE_ERROR,
                payload: { text: 'Edit_profile Error' }
            });
        })
}

//For reset password

export const resetPassword = ({ token, userId, oldPassword, newPassword }) => {

    return (dispatch) => {
        dispatch({
            type: RESET_PWD_LODING
        });
        axios.post(`${BASE_URL}resetpassword`, {
            user_id: userId,
            old_password: oldPassword,
            password: newPassword
        }, {
            headers: {
                Accept: 'application/json',
                Authorization: token
            }
        })
            .then((response) => {
                console.log(response)
                if (response.data.success) {
                    dispatch({
                        type: RESET_PWD_SUCCESS,
                        payload: { text: 'Reset Successfully', msg: response.data.message }
                    });
                } else {
                    dispatch({
                        type: RESET_PWD_FAILED,
                        payload: { text: 'Reset Failed', msg: response.data.message }
                    });
                }
            })
            .catch((err) => {
                console.log('Reset password', err)
                dispatch({
                    type: RESET_PWD_ERROR,
                    payload: { text: 'Reset Error' }
                });
            })
    }
}
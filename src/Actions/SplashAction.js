import axios from 'axios';
import {
    AUTH_URL,
    GET_TOKEN_LODING,
    GET_TOKEN_SUCCESS,
    GET_TOKEN_FAILED,
} from "./type";

export const getToken = () => {
    return (dispatch) => {
        dispatch({
            type: GET_TOKEN_LODING
        })
        axios.post(AUTH_URL, {
            client_id: "5de8f5a5ab7e6f12a86e8822",
            grant_type: "client_credentials",
            client_secret: "NTtinxiOYo68dbuer5UOGgEbuFBpamq8FoSvaVOl"
        }).then(response => {
            dispatch({
                type: GET_TOKEN_SUCCESS,
                payload: { text: 'Get_Token_Success', splashAuthToken: response.data.token_type + ' ' + response.data.access_token }
            });
        }).catch(error => {
            console.log('err', error);
            dispatch({
                type: GET_TOKEN_FAILED,
                payload: 'Get_Token_Err'
            })
        })
    }
} 
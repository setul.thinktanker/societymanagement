import axios from 'axios';
import {
    SIGNUP_INITIAL_STATE,
    SIGNUP_LOADING,
    SIGNUP_SUCCESS,
    SIGNUP_FAILED,
    SIGNUP_ERROR,
    BASE_URL
} from './type';
import { insertUser, deleteUser } from "../Database/allSchema";

export const signUpInitialState = () => {
    return {
        type: SIGNUP_INITIAL_STATE,
        payload: ''
    };
};

export const userSignUp = ({
    fname,
    lname,
    mobileNo,
    email,
    selectedGender,
    selectedHouseType,
    selectedBlock,
    selectedBlockNo,
    isOwnerStayInSociety,
    currentAddress,
    isRented,
    password,
    token
}) => {
    return (dispatch) => {
        dispatch({
            type: SIGNUP_LOADING
        });

        axios.post(`${BASE_URL}registeruser`, {
            name: fname + ' ' + lname,
            mobileNo,
            email,
            gender: selectedGender,
            houseType: selectedHouseType,
            block: selectedBlock,
            houseNo: selectedBlockNo,
            currentAddress: currentAddress,
            isRented: (isRented == 0) ? 'Yes' : 'No',
            image: null,
            password: password,
            isOwnerStayInSociety: (isOwnerStayInSociety) ? "Yes" : "No",
            society_id: '5cec09e54e2f1f126c78eb07'
        }, {
            headers: {
                Accept: 'application/json',
                Authorization: token
            }
        })
            .then((response) => {
                console.log('response', response);
                console.log('response', response.data.user);
                if (response.data.success) {
                    //store user in data base
                    let user = response.data.user
                    deleteUser().then(() => {
                        const newUser = {
                            id: (user._id == null) ? '' : user._id,
                            societyId: (user.society == null) ? '' : user.society,
                            societyName: (user.society_name == null) ? '' : user.society_name,
                            name: (user.name == null) ? '' : user.name,
                            mobileNo: (user.mobile_no == null) ? '' : user.mobile_no,
                            email: (user.email == null) ? '' : user.email,
                            gender: (user.gender == null) ? '' : user.gender,
                            houseType: (user.houseType == null) ? '' : user.houseType,
                            block: (user.block == null) ? '' : user.block,
                            blockNo: (user.houseNo == null) ? '' : user.houseNo,
                            isOwnerStayInSociety: (user.isOwnerStayInSociety == null) ? '' : user.isOwnerStayInSociety,
                            currentAddress: (user.currentAddress == null) ? '' : user.currentAddress,
                            isRented: (user.isRented == null) ? '' : user.isRented,
                            isChairman: (user.is_chairman == null) ? '' : user.is_chairman,
                            image: (user.image == null) ? '' : user.image,
                            password: (user.password == null) ? '' : user.password
                        };
                        insertUser(newUser).then((res) => {
                            console.log('newUser insert successfully', res)
                            // console.log('response.data.access_token', response.data.access_token)
                            dispatch({
                                type: SIGNUP_SUCCESS,
                                payload: {
                                    text: 'SignUp Success',
                                    authToken: `Bearer ${response.data.token}`,
                                }
                            });
                        }).catch((err) => {
                            dispatch({
                                type: SIGNUP_ERROR,
                                payload: { text: 'SignUp Error' }
                            });
                            console.log('newUser not inserted', err)
                        });
                    })

                } else {
                    console.log('response.data.errors', response.data.errors);
                    dispatch({
                        type: SIGNUP_FAILED,
                        payload: { text: 'SignUp Failed', signUpErr: response.data.errors }
                    });
                }
            })
            .catch((err) => {
                console.log('signUp err', err);
                dispatch({
                    type: SIGNUP_ERROR,
                    payload: { text: 'SignUp Error' }
                });
            })

        // userDB.limitToLast(1).once("value")
        //     .then((snap) => {
        //         snap.forEach(DataSnapshot => {
        //             console.log('child', Number(DataSnapshot.key) + 1);
        //             userId = Number(DataSnapshot.key) + 1
        //         })
        //         if (userId) {
        //             firebase.database().ref('users/').child(userId).set({
        //                 name: fname + ' ' + lname,
        //                 mobileNo,
        //                 email,
        //                 gender: selectedGender,
        //                 houseType: selectedHouseType,
        //                 block: selectedBlock,
        //                 blockNo: selectedBlockNo,
        //                 isOwnerStayInSociety: (isOwnerStayInSociety) ? "Yes" : "No",
        //                 currentAddress,
        //                 isRented: (isRented == 0) ? 'Yes' : 'No',
        //                 image: null,
        //                 password
        //             })
        //                 .then(() => {
        //                     dispatch({
        //                         type: SIGNUP_SUCCESS,
        //                         payload: 'SignUp Success'
        //                     });
        //                 })
        //                 .catch(() => {
        //                     dispatch({
        //                         type: SIGNUP_FAILED,
        //                         payload: 'SignUp Failed'
        //                     });
        //                 })
        //         }
        //         else {
        //             dispatch({
        //                 type: SIGNUP_ERROR,
        //                 payload: 'SignUp Error'
        //             });
        //         }
        //     })
        //     .catch((err) => {
        //         dispatch({
        //             type: SIGNUP_ERROR,
        //             payload: 'SignUp Error'
        //         });
        //     });


    }
}

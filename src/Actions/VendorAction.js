import {
    ADD_VEHICLE_ERROR,
    ADD_VENDOR_ERROR, ADD_VENDOR_FAILED,
    ADD_VENDOR_LOADING,
    ADD_VENDOR_SUCCESS,
    BASE_URL, DELETE_VENDOR_FAILED, DELETE_VENDOR_SUCCESS,
    GET_VENDOR_FAILED,
    GET_VENDOR_LOADING,
    GET_VENDOR_SUCCESS,
} from "./type";
import axios from 'axios';

export const addVendor = (
    vendorName, vendorMobile, vendorProfession, venderAddress, venderServices, venderLeaving,
    societyId, blockNo, status, authToken, venderId
) => {
    return (dispatch) => {
        dispatch({type: ADD_VENDOR_LOADING});

        const data = {
            "name": vendorName,
            "profession": vendorProfession,
            "services": venderServices,
            "address": venderAddress,
            "block_no": blockNo,
            "living": venderLeaving === 0 ? "Yes" : "No",
            "mobile_no": vendorMobile,
            "status": status,
            "societyId": societyId,
            "vendor_id": venderId
        };

        console.log("Data passed : " + JSON.stringify(data));
        // console.log("URL : " + `${BASE_URL}vendor`);

        axios.post(`${BASE_URL}vendor`, data, {
            headers: {
                Accept: 'application/json',
                Authorization: authToken
            }
        }).then((response) => {
            console.log('Add vendor response', response.data);

            if (response.data.success === true) {
                dispatch({
                    type: ADD_VENDOR_SUCCESS,
                    payload: {
                        text: response.data.msg,
                        success: response.data.success,
                        data: response.data
                    }
                });
            } else {
                dispatch({
                    type: ADD_VENDOR_FAILED,
                    payload: {
                        text: 'Failed to add vendor',
                        success: false
                    }
                });
            }
        }).catch((error) => {
            console.log('Failed to add vendor', error);
            dispatch({
                type: ADD_VENDOR_ERROR,
                payload: {
                    text: 'Failed to add vendor',
                    success: false
                }
            })
        });
    };
};

export const getVendors = (societyId, status, userID, authToken, passCusId) => {

    console.log("getVendors");
    console.log("societyId:" + societyId);
    console.log("status:" + status);
    console.log("userID" + userID);
    console.log("authToken" + authToken);

    return (dispatch) => {
        let data = {};

        if (passCusId === false) {
            data = {
                "societyId": societyId,
                "status": status,
            };
        } else {
            data = {
                "societyId": societyId,
                "status": status,
                "userID": userID
            };
        }

        dispatch({type: GET_VENDOR_LOADING});

        console.log("Data passed:" + JSON.stringify(data));
        console.log("URL :", `${BASE_URL}vendor`);

        axios.post(`${BASE_URL}vendor`, data, {
            headers: {
                Accept: 'application/json',
                Authorization: authToken
            }
        }).then((response) => {
            console.log('Get vendor listing:', response.data);
            if (response.data.success === true) {
                console.log('response.data.success');
                dispatch({
                    type: GET_VENDOR_SUCCESS,
                    payload: {
                        success: response.data.success,
                        msg: "Success",
                        data: response.data,
                    }
                })
            } else {
                console.log('response.data.failed');
                dispatch({
                    type: GET_VENDOR_FAILED,
                    payload: {
                        text: 'Failed to get vendor list!',
                        success: false
                    }
                });
            }
        }).catch((error) => {
            console.log('Getting vendor data error', error);
            dispatch({
                type: GET_VENDOR_FAILED,
                payload: {
                    text: 'Failed to get vendor list!',
                    success: false
                }
            })
        });
    }
};

export const deleteVenderDB = (vendor_id, status) => {
    return (dispatch) => {
        dispatch({type: DELETE_VENDOR_LOADING});

        const data = {
            "vendor_id": vendor_id,
            "status": status,
        };

        console.log("Data passed : " + JSON.stringify(data));

        axios.post(`${BASE_URL}vendor`, data, {
            headers: {
                Accept: 'application/json',
                Authorization: authToken
            }
        }).then((response) => {
            console.log('DELETE VENDOR RESPONSE', response.data);

            if (response.data.success === true) {
                dispatch({
                    type: DELETE_VENDOR_SUCCESS,
                    payload: {
                        response: response.data,
                    }
                });
            } else {
                dispatch({
                    type: DELETE_VENDOR_FAILED,
                    payload: {
                        response: response.data,
                    }
                });
            }
        }).catch((error) => {
            console.log('DELETE VENDOR ERROR', error);
            dispatch({
                type: DELETE_VENDOR_FAILED,
                payload: {
                    // response: response.data,
                    success: false
                }
            })
        });
    };
};

import axios from 'axios';
import {
    BASE_URL,
    INITIAL_EVENT_DATA,
    ADD_EVENT_LOADING,
    ADD_EVENT_SUCCESS,
    ADD_EVENT_FAILED,
    ADD_EVENT_ERROR,
    EDIT_EVENT_LOADING,
    EDIT_EVENT_SUCCESS,
    EDIT_EVENT_FAILED,
    EDIT_EVENT_ERROR,
    EVENT_LIST_LODING,
    EVENT_LIST_SUCCESS,
    EVENT_LIST_FAILED,
    EVENT_LIST_ERROR,
    DELETE_EVENT_LOADING,
    DELETE_EVENT_SUCCESS,
    DELETE_EVENT_FAILED,
    DELETE_EVENT_ERROR

} from './type';

//initial vehicle data
export const initialEventData = () => {
    return (dispatch) => {
        dispatch({
            type: INITIAL_EVENT_DATA
        })
    };
}

//add event

export const addEvent = ({ eventData, eventName, description, societyId, userId, token }) => {

    // console.log('vehicleData', eventData)
    // console.log('eventName', eventName)
    // console.log('SocietyId', societyId)
    // console.log('userId', userId)
    // console.log('token', token)

    return (dispatch) => {
        dispatch({
            type: ADD_EVENT_LOADING
        })

        axios.post(`${BASE_URL}event`, {
            userId: userId,
            societyId: societyId,
            title: eventName,
            description: description,
            date: eventData,
            status: 'add'
        }, {
                headers: {
                    Accept: 'application/json',
                    Authorization: token
                }
            })
            .then(response => {
                console.warn('respp', response)
                if (response.data.success) {

                    // dispatch(vehicleDetailsList({ userId, societyId, token, pageCount: 1 }))

                    dispatch({
                        type: ADD_EVENT_SUCCESS,
                        payload: { text: 'Add Success', msg: response.data.msg }
                    });

                } else {
                    dispatch({
                        type: ADD_EVENT_FAILED,
                        payload: { text: 'Add Failed', msg: response.data.msg }
                    });
                }
            })
            .catch((err) => {
                console.log('fetch fail GetVehicle', err);
                dispatch({
                    type: ADD_EVENT_ERROR,
                    payload: 'Add Error'
                });
            });
    };
}

export const eventList = ({ userId, societyId, pageNo, token, list }) => {

    console.log('event listing');
    console.log('userId', userId);
    console.log('societyId', societyId);
    console.log('pageCount', pageNo);
    console.log('token', token);
    console.log('list', list);

    return (dispatch) => {

        if (pageNo == 1) {
            dispatch({
                type: EVENT_LIST_LODING
            })
        }

        axios.post(`${BASE_URL}event`, {
            userId: userId,
            societyId: societyId,
            page_no: pageNo,
            page_count: 10,
            status: 'list'
        }, {
                headers: {
                    Accept: 'application/json',
                    Authorization: token
                }
            })
            .then(response => {
                console.log('response event', response);
                if (response.data.success) {
                    let events = response.data.eventlist;
                    let isData = (events.length == 0) ? false : true;
                    if (pageNo > 1) {
                        dispatch({
                            type: EVENT_LIST_SUCCESS,
                            payload: { list: list.concat(events), isData: isData }
                        });
                    } else {
                        dispatch({
                            type: EVENT_LIST_SUCCESS,
                            payload: { list: events, isData: isData }
                        });
                    }
                } else {
                    dispatch({
                        type: EVENT_LIST_FAILED,
                        payload: 'GetEvent Failed'
                    });
                }
            })
            .catch((err) => {
                console.log('fetch fail GetEvent', err);
                dispatch({
                    type: EVENT_LIST_ERROR,
                    payload: 'GetEvent Error'
                });
            });
    };
}

export const deleteEvent = ({ eventId, token, userId, societyId, pageNo, list }) => {

    console.log('eventId', eventId)
    console.log('token', token)
    console.log('userId', userId)
    console.log('societyId', societyId)
    console.log('pageNo', pageNo)
    console.log('list', list)

    return (dispatch) => {
        dispatch({
            type: DELETE_EVENT_LOADING
        })

        axios.post(`${BASE_URL}event`, {
            event_id: eventId,
            status: "delete"
        }, {
                headers: {
                    Accept: 'application/json',
                    Authorization: token
                }
            })
            .then(response => {
                console.log('delete event response', response)
                if (response.data.success) {
                    dispatch(eventList({ userId, societyId, token, pageNo, list }))
                    dispatch({
                        type: DELETE_EVENT_SUCCESS,
                        payload: { text: 'Delete Success', msg: response.data.msg }
                    });

                } else {
                    dispatch({
                        type: DELETE_EVENT_FAILED,
                        payload: { text: 'Delete Failed', msg: response.data.msg }
                    });
                }
            })
            .catch((err) => {
                console.log('fetch fail GetVehicle', err);
                dispatch({
                    type: DELETE_EVENT_ERROR,
                    payload: { text: 'Delete Error', msg: 'something went wrong please try again later.' }
                });
            });
    };
}

export const editEvent = ({ eventId, eventData, eventName, description, societyId, userId, token }) => {

    console.log('eventData', eventData)
    console.log('eventId', eventId)
    console.log('eventName', eventName)
    console.log('SocietyId', societyId)
    console.log('userId', userId)
    console.log('token', token)

    return (dispatch) => {
        dispatch({
            type: EDIT_EVENT_LOADING
        })

        axios.post(`${BASE_URL}event`, {
            userId: userId,
            societyId: societyId,
            title: eventName,
            description: description,
            date: eventData,
            event_id: eventId,
            status: 'update'
        }, {
                headers: {
                    Accept: 'application/json',
                    Authorization: token
                }
            })
            .then(response => {
                console.warn('edit event response', response)
                if (response.data.success) {

                    // dispatch(vehicleDetailsList({ userId, societyId, token, pageCount: 1 }))

                    dispatch({
                        type: EDIT_EVENT_SUCCESS,
                        payload: { text: 'edit Success', msg: response.data.msg }
                    });

                } else {
                    dispatch({
                        type: EDIT_EVENT_FAILED,
                        payload: { text: 'edit Failed', msg: response.data.error }
                    });
                }
            })
            .catch((err) => {
                console.log('fetch fail GetVehicle', err);
                dispatch({
                    type: EDIT_EVENT_ERROR,
                    payload: 'edit Error'
                });
            });
    };
}
import {
    BASE_URL, DELETE_NOTICE_FAILED, DELETE_NOTICE_LOADING, DELETE_NOTICE_SUCCESS,
    GET_NOTICE_FAILED,
    GET_NOTICE_LOADING,
    GET_NOTICE_SUCCESS,
} from "./type";
import axios from "axios";

export const getNotice = (societyId, userId, authToken) => {
    return (dispatch) => {
        dispatch({ type: GET_NOTICE_LOADING });

        //change url
        // axios.post(`${BASE_URL}vendor`, data, {
        // axios.post("http://192.168.0.133:8000/api/show-noticeboard", data, {
        const URL = "http://192.168.0.133:8000/api/show-noticeboard";
        axios.post(URL, null, {
            headers: {
                Accept: 'application/json',
                Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMWJkZmFiNjlkMjk1ZjY4NmQzZWU4N2Q1MjAyODczNmU0ZjdhMzI2YmIwODg4NGU3ZWI4MTgyODgxZjlmMmVkNjQ2ZGNlZDM3ODYyNDQ1OTMiLCJpYXQiOjE1ODQ2OTI5MjYsIm5iZiI6MTU4NDY5MjkyNiwiZXhwIjoxNjE2MjI4OTI2LCJzdWIiOiIxMyIsInNjb3BlcyI6W119.tTwxjda3n8Ck8oaVV5Bc3QkA_dUMYZ4S59T1FbE1O-TRnoKXrMyeQ4rCImeZj6rYmiRrME1k_xSZYD0-f-Kfcd_Lbv2WNGwlhFdViYeZ-Jh7EdIt2DDwoPORIbTU8N_fCaM7ZTANteaJrvJy5KFGTQ2KTOtKBa3VZ2RqDB2A1d8nsbKrdlCbWcJtLFtYvTPgeXYcFGwNDGFRRDCs0aYWmcx6mdHzIXoMCbAeI3Qw41tApBHPHEz9ID-1xBpIZVJuN_PUxdPgTrffFCs276LbPOc-ksuBKFpZDiXHSimfWlAwvWUSUWUe-MtRF1O0pxBI-LNizjXRPD9yZ2T5QKB5HPkO3NMunDm9xgYqx9NbPdzCpPwP1-89SXAGhjAPm9nRKJHujwzpyB-zNhq-POlruxT8Qa9a2Qdm51JfCvD8FMXqJ5NGo1ie3vPJxvLl23M6wuT7RMbpTknizr9_78ylaqf4oC_4kuwmabIhpc_JaJuxnRHMlKBqYxB2CgrCvLwhpi2B-x0RJayQYV8Pgj-AQE0TKF6JLnwcMIOrG9fHSOgYLE_mTtO0q26op21_AAYjO9LcFTmPvylb35msbojn1zc0Mnf1GoWARVIegjj63iMNnDLGm1FcAnm_uu2mPSRJMbsFkDX1y7EMdpAVr0X1pw0xqNgidqFAJ3b5vyJixoI"
                // Authorization: authToken
            }
        }).then((response) => {
            console.log('Notice listing:', response.data);
            dispatch({
                type: GET_NOTICE_SUCCESS,
                payload: {
                    success: response.data.success,
                    msg: response.data.msg,
                    data: response.data,
                }
            })
        }).catch((error) => {
            console.log('Getting notice data error:', error);
            dispatch({
                type: GET_NOTICE_FAILED,
                payload: {
                    success: response.data.success,
                    msg: response.data.msg,
                }
            })
        });
    }
};

export const delNotice = (noticeId) => {
    return (dispatch) => {
        dispatch({ type: DELETE_NOTICE_LOADING });

        const data = {
            id: noticeId,
        };
        // axios.post(`${BASE_URL}vendor`, data, {
        // axios.post("http://192.168.0.133:8000/api/show-noticeboard", data, {
        const URL = "http://192.168.0.133:8000/api/delete-noticeboard";
        axios.post(URL, data, {
            headers: {
                Accept: 'application/json',
                Authorization: "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiMWJkZmFiNjlkMjk1ZjY4NmQzZWU4N2Q1MjAyODczNmU0ZjdhMzI2YmIwODg4NGU3ZWI4MTgyODgxZjlmMmVkNjQ2ZGNlZDM3ODYyNDQ1OTMiLCJpYXQiOjE1ODQ2OTI5MjYsIm5iZiI6MTU4NDY5MjkyNiwiZXhwIjoxNjE2MjI4OTI2LCJzdWIiOiIxMyIsInNjb3BlcyI6W119.tTwxjda3n8Ck8oaVV5Bc3QkA_dUMYZ4S59T1FbE1O-TRnoKXrMyeQ4rCImeZj6rYmiRrME1k_xSZYD0-f-Kfcd_Lbv2WNGwlhFdViYeZ-Jh7EdIt2DDwoPORIbTU8N_fCaM7ZTANteaJrvJy5KFGTQ2KTOtKBa3VZ2RqDB2A1d8nsbKrdlCbWcJtLFtYvTPgeXYcFGwNDGFRRDCs0aYWmcx6mdHzIXoMCbAeI3Qw41tApBHPHEz9ID-1xBpIZVJuN_PUxdPgTrffFCs276LbPOc-ksuBKFpZDiXHSimfWlAwvWUSUWUe-MtRF1O0pxBI-LNizjXRPD9yZ2T5QKB5HPkO3NMunDm9xgYqx9NbPdzCpPwP1-89SXAGhjAPm9nRKJHujwzpyB-zNhq-POlruxT8Qa9a2Qdm51JfCvD8FMXqJ5NGo1ie3vPJxvLl23M6wuT7RMbpTknizr9_78ylaqf4oC_4kuwmabIhpc_JaJuxnRHMlKBqYxB2CgrCvLwhpi2B-x0RJayQYV8Pgj-AQE0TKF6JLnwcMIOrG9fHSOgYLE_mTtO0q26op21_AAYjO9LcFTmPvylb35msbojn1zc0Mnf1GoWARVIegjj63iMNnDLGm1FcAnm_uu2mPSRJMbsFkDX1y7EMdpAVr0X1pw0xqNgidqFAJ3b5vyJixoI"
                // Authorization: authToken
            }
        }).then((response) => {
            console.log('Notice listing:', response.data);
            dispatch({
                type: DELETE_NOTICE_SUCCESS,
                payload: {
                    success: response.data.success,
                    msg: response.data.msg,
                    data: response.data,
                }
            })
        }).catch((error) => {
            console.log('Getting notice data error:', error);
            dispatch({
                type: DELETE_NOTICE_FAILED,
                payload: {
                    success: response.data.success,
                    msg: response.data.msg,
                }
            })
        });
    }
};

import axios from 'axios';
import {
    BASE_URL,
    SOCIETY_MEMBERS_LIST_LODING,
    SOCIETY_MEMBERS_LIST_SUCCESS,
    SOCIETY_MEMBERS_LIST_FAILED,
    SOCIETY_MEMBERS_LIST_ERROR,
    VEHICLE_LIST_LODING,
    VEHICLE_LIST_SUCCESS,
    VEHICLE_LIST_FAILED,
    VEHICLE_LIST_ERROR,
} from './type';



//get society members 
export const societyMembersList = ({ token, pageCount, list }) => {
    // console.log('societymember_count', pageCount)
    // console.log('societymember_list', list)
    return (dispatch) => {

        if (pageCount == 1) {
            dispatch({
                type: SOCIETY_MEMBERS_LIST_LODING
            })
        }

        axios.post(`${BASE_URL}societymembers`, {
            page_no: pageCount,
            page_count: 10
        }, {
                headers: {
                    Accept: 'application/json',
                    Authorization: token
                }
            })
            .then(response => {
                if (response.data.success) {
                    let users = response.data.user;
                    let isData = (users.length == 0) ? false : true;
                    if (pageCount > 1) {
                        dispatch({
                            type: SOCIETY_MEMBERS_LIST_SUCCESS,
                            payload: { list: list.concat(users), isData: isData }
                        });
                    } else {
                        dispatch({
                            type: SOCIETY_MEMBERS_LIST_SUCCESS,
                            payload: { list: users, isData: isData }
                        });
                    }
                } else {
                    dispatch({
                        type: SOCIETY_MEMBERS_LIST_FAILED,
                        payload: 'Get Society Members Failed'
                    });
                }
            })
            .catch((err) => {
                console.log('fetch fail society', err);
                dispatch({
                    type: SOCIETY_MEMBERS_LIST_ERROR,
                    payload: 'Get Society Members Error'
                });
            });
    };
}

// //get vehicle details
// export const vehicleDetailsList = ({ userId, societyId, token, pageCount, list }) => {
//     console.log('vehicleDetailsList vehicleDetailsList')
//     console.log('societyId', societyId)
//     console.log('token', token)
//     console.log('userId', userId)
//     console.log('pageCount', pageCount)
//     console.log('list', list)

//     return (dispatch) => {

//         if (pageCount == 1) {
//             dispatch({
//                 type: VEHICLE_LIST_LODING
//             })
//         }

//         axios.post(`${BASE_URL}vehicledetail`, {
//             society_id: societyId,
//             user_id: userId,
//             page_no: pageCount,
//             page_count: 10
//         }, {
//                 headers: {
//                     Accept: 'application/json',
//                     Authorization: token
//                 }
//             })
//             .then(response => {
//                 if (response.data.success) {
//                     let vehicles = response.data.data;
//                     let isData = (vehicles.length == 0) ? false : true;
//                     if (pageCount > 1) {
//                         dispatch({
//                             type: VEHICLE_LIST_SUCCESS,
//                             payload: { list: list.concat(vehicles), isData: isData }
//                         });
//                     } else {
//                         dispatch({
//                             type: VEHICLE_LIST_SUCCESS,
//                             payload: { list: vehicles, isData: isData }
//                         });
//                     }
//                 } else {
//                     dispatch({
//                         type: VEHICLE_LIST_FAILED,
//                         payload: 'GetVehicle Failed'
//                     });
//                 }
//             })
//             .catch((err) => {
//                 console.log('fetch fail GetVehicle', err);
//                 dispatch({
//                     type: VEHICLE_LIST_ERROR,
//                     payload: 'GetVehicle Error'
//                 });
//             });
//     };
// }

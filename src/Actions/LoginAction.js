import axios from 'axios';
import {
  BASE_URL,
  LOGIN_LOADING,
  LOGIN_FAILED,
  LOGIN_SUCCESS,
  LOGIN_ERROR,
  LOGIN_INITIAL_STATE,
  SOCIETYCODE_SUCCESS,
  SOCIETYCODE_FAILED,
  SOCIETYCODE_ERROR
} from './type';
import { insertUser, deleteUser } from "../Database/allSchema";
import { StackActions, NavigationActions } from 'react-navigation';
import firebase from 'react-native-firebase';
import { array } from 'prop-types';
let userDB = firebase.database().ref('users/');


export const loginInitialstate = () => {
  return {
    type: LOGIN_INITIAL_STATE,
    payload: ''
  };
};

// export const userLogin = ({ mobileNo, password }) => {

//   return (dispatch) => {

//     dispatch({
//       type: LOGIN_LOADING
//     });
//     // let isUser = false;
//     userDB.once("value").then((snap) => {
//       snap.forEach(child => {
//         console.log('child', child.val().mobileNo)
//         if (child.val().mobileNo == mobileNo && child.val().password == password) {
//           console.log(child.val().mobileNo + '==' + password)
//           console.log('childkey', child.key)
//           deleteUser().then(() => {
//             const newUser = {
//               id: (child.key == null) ? '' : Number(child.key),
//               name: (child.val().name == null) ? '' : child.val().name,
//               mobileNo: (child.val().mobileNo == null) ? '' : child.val().mobileNo,
//               email: (child.val().email == null) ? '' : child.val().email,
//               gender: (child.val().gender == null) ? '' : child.val().gender,
//               houseType: (child.val().houseType == null) ? '' : child.val().houseType,
//               block: (child.val().block == null) ? '' : child.val().block,
//               blockNo: (child.val().blockNo == null) ? '' : child.val().blockNo,
//               isOwnerStayInSociety: (child.val().isOwnerStayInSociety == null) ? '' : child.val().isOwnerStayInSociety,
//               currentAddress: (child.val().currentAddress == null) ? '' : child.val().currentAddress,
//               isRented: (child.val().isRented == null) ? '' : child.val().isRented,
//               image: (child.val().image == null) ? '' : child.val().image,
//               password: (child.val().password == null) ? '' : child.val().password
//             };
//             insertUser(newUser).then((res) => {
//               console.log('newUser insert successfully', res)
//               dispatch({
//                 type: LOGIN_SUCCESS,
//                 payload: 'Login Success'
//               });
//             }).catch((err) => {
//               dispatch({
//                 type: LOGIN_FAILED,
//                 payload: 'Login Failed'
//               });
//               console.log('newUser not inserted', err)
//             });
//           })

//           return true;
//         } else {
//           dispatch({
//             type: LOGIN_FAILED,
//             payload: 'Login Failed'
//           });
//           return false;
//         }

//       });
//     }).catch((err) => {
//       dispatch({
//         type: LOGIN_ERROR,
//         payload: 'Login Error'
//       });
//     })
//   };
// }

export const userLogin = ({ mobileNo, password, token }) => {

  return (dispatch) => {
    dispatch({
      type: LOGIN_LOADING
    });
    axios.post(`${BASE_URL}loginuser`, {
      mobile_no: mobileNo,
      password
    }, {
        headers: {
          Accept: 'application/json',
          // Authorization: token
        }
      })
      .then((response) => {
        console.log('response', response.data)
        if (response.data.success) {
          let user = response.data.user;
          let societyId = user.society
          console.log("ID: " + user.id)
          console.log("societyID: ", user.society_id)


          //first delete user from realm database
          deleteUser().then(() => {
            const newUser = {
              id: (user.id == null) ? '' : user.id.toString(),
              societyId: (user.society_id == null) ? '' : user.society_id.toString(),
              societyName: (user.society_name == null) ? '' : user.society_name,
              mobileNo: (user.mobile_no == null) ? '' : user.mobile_no,
              email: (user.email == null) ? '' : user.email,
              gender: (user.gender == null) ? '' : user.gender,
              houseType: (user.house_type == null) ? '' : user.house_type,
              block: (user.block == null) ? '' : user.block,
              blockNo: (user.house_no == null) ? '' : user.house_no.toString(),
              isOwnerStayInSociety: (user.isOwnerStayInSociety == null) ? '' : user.isOwnerStayInSociety,
              currentAddress: (user.currentAddress == null) ? '' : user.currentAddress,
              isRented: (user.isRented == null) ? '' : user.isRented,
              isChairman: (user.is_chairman == null) ? '' : user.is_chairman,
              image: (user.image == null) ? '' : user.image,
              name: (user.name == null) ? '' : user.name,
              password: (user.password == null) ? '' : user.password
            };

            //insert new user from realm database
            insertUser(newUser)
              .then((result) => {
                console.log('result', result);
                getSocietyDetails(societyId, token)

                dispatch({
                  type: LOGIN_SUCCESS,
                  payload: {
                    text: 'Login Success',
                    authToken: `Bearer ${response.data.token}`,
                  }
                });
              })
              .catch((err) => {
                dispatch({
                  type: LOGIN_FAILED,
                  payload: { text: 'Login Failed', error: 'something wents wrong please try again later.' }
                });
                console.log('User not inserted', err)
              });

          })
        } else {
          console.log('response.data.errors', response.data.errors)
          dispatch({
            type: LOGIN_FAILED,
            payload: { text: 'Login Failed', error: response.data.errors }
          })
        }
      })
      .catch((error) => {
        console.log('Login error', error)
        dispatch({
          type: LOGIN_ERROR,
          payload: 'Login Error'
        })
      })
  }
}


export const getSocietyDetails = (societyCode, token) => {
  return (dispatch) => {
    axios.post(`${BASE_URL}getsociety-data`, {
      // society_id: societyCode
      society_id: societyCode
    }, {
        headers: {
          Accept: 'application/json',
          // Authorization: token
          // Authorization: `Bearer ${accessToken}`
        }
      })
      .then((response) => {
        console.log('response society Data', response);
        if (response.data.success) {

          let society = []
          let list = []

          let tempHouseType = response.data.data.house_type
          society.push(tempHouseType)

          for (let index = 0; index < society.length; index++) {

            let element = society[index];
            let items = Object.keys(element)
            let array = items;

            for (let index = 0; index < array.length; index++) {
              // create object which have name key as society structure
              let name = array[index]
              //set object type
              obj = {}
              obj.name = name
              obj.blocklist = [element[name]]
              list.push(obj)
              //set blocklist
              let tempBlocklist = obj.blocklist
              let newObj = {}
              console.log('tempBlocklist', tempBlocklist)
              for (let index = 0; tempBlocklist < array.length; index++) {
                const blocklistIn = tempBlocklist[index];
                console.log('blocklistIn', blocklistIn)
              }
              // newObj.house_no = element[name].newObj

              // ({ name: item.type, blocklist: item.blocklist })
              console.log('newObj', newObj)

            }
          }


          dispatch({
            type: SOCIETYCODE_SUCCESS,
            payload: { text: 'Societycode Success', houseType: list }
          });

        } else {
          dispatch({
            type: SOCIETYCODE_FAILED,
            payload: { text: 'Societycode Failed' }
          });
        }
      })
      .catch((error) => {
        console.log('society_code err', error)
        dispatch({
          type: SOCIETYCODE_ERROR,
          payload: { text: 'Societycode Error' }
        });
      })
  }
}

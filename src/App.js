import React, { Component } from "react";
import { } from "react-native";
import { createStackNavigator, createAppContainer, createDrawerNavigator } from "react-navigation";
import { Provider } from "react-redux";
import Reducers from "./Reducers";
import ReduxThunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";

import Splash from "./Components/Splash";
import Login from "./Components/Login";
import SignUp from "./Components/SignUp";
import SocietyMembers from "./Components/SocietyMembers";
import ResetPassword from "./Components/ResetPassword";
import ForgotPassword from "./Components/ForgotPassword";
import EnterOtp from "./Components/EnterOtp";
import RenterDetails from "./Components/RenterDetails";
import Home from "./Components/Home";
import DrawerMenu from "./Components/DrawerMenu";
import Notification from "./Components/Notification";
import Profile from "./Components/Profile";
import ProfileDetails from "./Components/ProfileDetails";
import VehiclesDetails from "./Components/VehiclesDetails";
import AddVehicle from "./Components/AddVehicle";
import Venders from "./Components/Venders";
import AddVender from "./Components/AddVender";
import Complaints from "./Components/Complaints";
import ComplaintsItemLayout from "./Components/ComplaintsItemLayout";
import PlayVideo from "./Components/PlayVideo";
import AddComplaint from "./Components/AddComplaint";
import Maintenance from "./Components/Maintenance";
import CommonplotDetails from "./Components/CommonplotDetails";
import Rulebook from "./Components/Rulebook";
import Event from './Components/Event';
import AddEvent from './Components/AddEvent';
import EditEvent from './Components/EditEvent';
import PhotoGallery from "./Components/PhotoGallery";
import PhotoGalleryDetails from "./Components/PhotoGalleryDetails";
import Notice from "./Components/Notice"
import VenderDetails from "./Components/VenderDetails";

const RootStack = createStackNavigator(
  {
    //screens
    Splash: { screen: Splash },
    Login: { screen: Login },
    SignUp: { screen: SignUp },
    EnterOtp: { screen: EnterOtp },
    RenterDetails: { screen: RenterDetails },
    Home: { screen: Home },
    SocietyMembers: { screen: SocietyMembers },
    Notification: { screen: Notification },
    Profile: { screen: Profile },
    ProfileDetails: { screen: ProfileDetails },
    VehiclesDetails: { screen: VehiclesDetails },
    AddVehicle: { screen: AddVehicle },
    VenderDetails: { screen: VenderDetails },
    Venders: { screen: Venders },
    AddVender: { screen: AddVender },
    Complaints: { screen: Complaints },
    ComplaintsItemLayout: { screen: ComplaintsItemLayout },
    PlayVideo: { screen: PlayVideo },
    AddComplaint: { screen: AddComplaint },
    Maintenance: { screen: Maintenance },
    CommonplotDetails: { screen: CommonplotDetails },
    ResetPassword: { screen: ResetPassword },
    ForgotPassword: { screen: ForgotPassword },
    Rulebook: { screen: Rulebook },
    Event: { screen: Event },
    AddEvent: { screen: AddEvent },
    EditEvent: { screen: EditEvent },
    PhotoGallery: { screen: PhotoGallery },
    Notice: { screen: Notice },
    PhotoGalleryDetails: { screen: PhotoGalleryDetails }
  },
  {
    //Run First
    initialRouteName: "Splash",
    headerMode: "none"
  }
);

const DrawerNavigation = createDrawerNavigator({
  RootStack: { screen: RootStack },
}, {
    initialRouteName: 'RootStack',
    // drawerWidth: '20%',
    drawerBackgroundColor: 'transparent',
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
    drawerPosition: 'left',
    backBehavior: 'none',
    contentComponent: props => <DrawerMenu { ...props } />,
    // drawerLockMode: 'locked-closed'
  });

const MyApp = createAppContainer(DrawerNavigation);

export default class App extends React.Component {
  componentWillMount() {
    console.disableYellowBox = true;
  }
  render() {
    const store = createStore(Reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={ store }>
        <MyApp />
      </Provider>
    );
  }
}
